// From http://epsg.io/21781.js and http://epsg.io/2056.js
proj4.defs("EPSG:21781","+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=600000 +y_0=200000 +ellps=bessel +towgs84=674.4,15.1,405.3,0,0,0,0 +units=m +no_defs");
proj4.defs("EPSG:2056","+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs");
proj4.defs('urn:x-ogc:def:crs:EPSG:21781', proj4.defs('EPSG:21781'));
proj4.defs('urn:x-ogc:def:crs:EPSG:2056', proj4.defs('EPSG:2056'));

var proj_2056 = ol.proj.get('EPSG:2056');

var geojsonFormat = new ol.format.GeoJSON();

var centerStyle = new ol.style.Style({
    image: new ol.style.Circle({
        radius: 4,
        fill: new ol.style.Fill({color: 'white'}),
        stroke: new ol.style.Stroke({color: 'black'})
    })
});
var theoCenterStyle = new ol.style.Style({
    image: new ol.style.Circle({
        radius: 3,
        fill: new ol.style.Fill({color: 'red'}),
        stroke: new ol.style.Stroke({color: 'black'})
    })
});

var radiusStyle = new ol.style.Style({
    // Yellow border, filled with translucid background
    stroke: new ol.style.Stroke({
      color: 'yellow',
      width: 1
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 0, 0.1)'
    })
});

var gemeindeStyle = new ol.style.Style({
    // Similar to default ol style
    stroke: new ol.style.Stroke({
        color: "#32aaffa0",
        width: 2
    }),
    fill: new ol.style.Fill({
        color: "#ffffff60",
    })
});

var plotGrayStyle = new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'black',
      width: 3
    }),
    fill: new ol.style.Fill({
      color: 'rgba(205, 0, 0, 0.5)'
    })
});

var baseTextStyle = {
    font: '12px Arial,sans-serif',
    textAlign: 'center',
    fill: new ol.style.Fill({
        color: 'black'
    }),
    stroke: new ol.style.Stroke({
        color: 'white',
        width: 2
    })
};

var baselayer = new ol.layer.Tile({
    source: new ol.source.TileWMS({
      url: 'https://geowms.bl.ch',
      params: {
        'LAYERS': 'orthofotos_swissimage_2018_group',
        'FORMAT': 'image/png'
      },
    })
});

var kantonlayer = new ol.layer.Tile({
    source: new ol.source.TileWMS({
      url: 'https://wms.geo.admin.ch',
      params: {
        'LAYERS': 'ch.swisstopo.swissboundaries3d-kanton-flaeche.fill',
        'FORMAT': 'image/png'
      },
    })
});

class KSPMap extends ol.Map {

  addLayerFromGeoJSONUrl(srcUrl, layerName, style, callback) {
    var source = new ol.source.Vector(),
        self = this;
    $.ajax(srcUrl).then(function(response) {
        var features = geojsonFormat.readFeatures(response);
        source.addFeatures(features);
        if (callback) callback(self);
    });
    var newLayer = new ol.layer.Vector({source: source});
    newLayer.set('selectable', true);
    this.addLayer(newLayer);
    this[layerName] = newLayer;
    if (style) newLayer.setStyle(style);
    return newLayer;
  }
}

function plotobsLayer (centerCoords, centerExactCoords, featuresJSON, radius) {
    this.source = new ol.source.Vector();
    var features = geojsonFormat.readFeatures(featuresJSON);
    this.source.addFeatures(features);
    var circleFeat = new ol.Feature(
        new ol.geom.Circle(centerExactCoords || centerCoords, parseFloat(radius))
    );
    circleFeat.setStyle(radiusStyle);
    this.source.addFeature(circleFeat);
    if (centerExactCoords) {
        // Add point with the theoric coordinates
        var theoricCenter = new ol.Feature(new ol.geom.Point(centerCoords));
        theoricCenter.setStyle(theoCenterStyle);
        this.source.addFeature(theoricCenter);
    }
    this.layer = new ol.layer.Vector({
        source: this.source,
        style: function(feature, resolution) {
            if (feature.get('type') == 'center')
                return [centerStyle];
            baseTextStyle.text = feature.get('nr').toString();
            return [
                new ol.style.Style({
                    image: new ol.style.Circle({
                        radius: 9,
                        fill: new ol.style.Fill({
                            color: 'rgba(52, 101, 164, 0.5)',
                        }),
                        stroke: new ol.style.Stroke({
                            color: '#0000FF',
                            width: 1
                        })
                    }),
                    text: new ol.style.Text(baseTextStyle)
                })
            ];
        }
    });
}

function gemeindenLayer (gemeinden) {
    this.source = new ol.source.Vector({
        features: gemeinden
    });
    this.layer = new ol.layer.Vector({
        source: this.source,
        style: function(feature, resolution) {
            return [new ol.style.Style({
             fill: new ol.style.Fill({color: 'rgba(0,255,255,0.1)'}),
             stroke: new ol.style.Stroke({
               color: '#0ff',
               width: 1
             }),
             text: new ol.style.Text({
                font: '22px Arial sans-serif',
                text: feature.get('name'),
                fill: new ol.style.Fill({color: 'rgba(200,200,200,0.5)'}),
                stroke: new ol.style.Stroke({color: 'white', width: 1})
             })
           })]
        }
    });
}

function getSelectIaction() {
    return new ol.interaction.Select({
        layers: function(layer) { return layer.get('selectable') == true; }/*,
        style: new ol.style.Style({
            image: new ol.style.Circle({
              radius: 5,
              fill: new ol.style.Fill({
                color: '#FF0000'
              }),
              stroke: new ol.style.Stroke({
                color: '#000000'
              })
            })
        })*/
    });
}
