
function calcPlotNumbs(map) {
    if (map.plotObsLayer) $('span#plotobsCount').text(map.plotObsLayer.getSource().getFeatures().length);
    var plotExcluded = plotObsolete = plotCurrent = 0;
    map.plotLayer.getSource().getFeatures().forEach(function (item) {
        if (item.get('excluded')) plotExcluded += 1;
        else {
            if (item.get('in_previous')) plotCurrent += 1;
            else plotObsolete += 1;
        }
    });
    $('span#plotExcludedCount').text(plotExcluded);
    $('span#plotObsoleteCount').text(plotObsolete);
    $('span#plotCurrentCount').text(plotCurrent);
}

function pointFromLatLon(map) {
    latVal = $('#id_latitude').val();
    longVal = $('#id_longitude').val();
    if (latVal && latVal > 1000000 && longVal && longVal > 2000000) {
        map.newPlotLayer.getSource().clear();
        newPt = new ol.Feature({geometry: new ol.geom.Point([longVal, latVal])});
        map.newPlotLayer.getSource().addFeature(newPt);
    }
}

var selectIaction = getSelectIaction();
selectIaction.getFeatures().on('change:length', function(e) {
    if (e.target.getArray().length > 0) {
        let feature = e.target.item(0);
        $('div#plot').load(feature.getProperties().url);
    } else {
        $('div#plot').html('');
    }
});
var modify = new ol.interaction.Modify({
    features: selectIaction.getFeatures()
});

var inventoryStyle = new ol.style.Style({
    // Similar to default ol style
    stroke: new ol.style.Stroke({
        color: "#ffaaffa0",
        width: 2
    }),
    fill: new ol.style.Fill({
        color: "#ff333330",
    })
});

var lightGreyStyle = new ol.style.Style({
    image: new ol.style.Circle({
        radius: 4,
        fill: new ol.style.Fill({color: '#bbb'}),
        stroke: new ol.style.Stroke({color: '#222'})
    })
});

var excludedStyle = new ol.style.Style({
    image: new ol.style.Circle({
        radius: 4,
        fill: new ol.style.Fill({color: '#555'}),
        stroke: new ol.style.Stroke({color: '#222'})
    })
});

var newPlotStyle = new ol.style.Style({
    image: new ol.style.Circle({
        radius: 8,
        fill: new ol.style.Fill({color: '#F7FF00'}),
        stroke: new ol.style.Stroke({color: '#222'})
    })
});

// TODO: add box selection tool with Ctrl-Drag: https://openlayers.org/en/latest/examples/box-selection.html

$(document).ready(function() {
    $('div#plot').on('click', 'button#exclude, button#reinclude', function(ev) {
        ev.preventDefault();
        var featArray = selectIaction.getFeatures().getArray();
        var ptIds = [];
        for (var i = 0; i < featArray.length; i++) {
            ptIds.push(featArray[i].getProperties().pk);
        }
        var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
        var postParams = {ptIds: ptIds, csrfmiddlewaretoken: csrftoken, op: this.id};
        $.post(inventory.exclude_plots_url, postParams, function (data) {
            // Remove points from map and selection
            plotLayer.getSource().removeFeature(featArray[0]);
            features = selectIaction.getFeatures();
            for (var i = 0; i < features.getArray().length; i++) {
                features.remove(features.getArray()[i]);
            }
            // Readd points received from response data
            plotLayer.getSource().addFeatures(geojsonFormat.readFeatures(data.features));
            calcPlotNumbs(map);
        });
    });

    $('button#showform').click(function(ev) {
        ev.preventDefault();
        $('#newplot_form').show();
        // Pre-fill coordinates with map center.
        $('#id_latitude').val(parseInt(municipalityCenter[1] / 100) * 100);
        $('#id_longitude').val(parseInt(municipalityCenter[0] / 100) * 100);
        pointFromLatLon(map);
        $(this).hide();
    });
    $('button.cancel').click(function(ev) {
        ev.preventDefault();
        $(ev.target.closest('form')).hide();
        $('button#showform').show();
        $('button#shpBtn').show();
    });
    $('button#newplot_submit').click(function(ev) {
        ev.preventDefault();
        $.post($('#newplot_form').attr('action'), $('#newplot_form').serialize(), function (data) {
            map.newPlotLayer.getSource().clear();
            map.plotLayer.getSource().addFeatures(geojsonFormat.readFeatures(data.features));
            $('#newplot_form').hide();
            $('button#showform').show();
        });
    });
    $('#id_latitude, #id_longitude').on('change paste keyup', (ev) => pointFromLatLon(map));

    $('button#shpBtn').click(ev => {
        ev.preventDefault();
        $('#shapeForm').show();
        $(this).hide();
    });
    $('button#shapeForm_submit').click(ev => {
        ev.preventDefault();
        const form = ev.target.closest('form');
        const data = new FormData(form);
        $.ajax({url: form.action, type: 'POST', data: data, processData: false, contentType: false,
            success: function(data, textStatus, jqXHR){
                if (data.result == "Error") {
                    alert(data.error);
                    return;
                }
                if (data.new > 0) {
                    map.newPlotLayer.getSource().clear();
                    map.plotLayer.getSource().addFeatures(geojsonFormat.readFeatures(data.features));
                }
                $('#shapeForm').hide();
                $('button#shpBtn').show();
                alert(`${data.new} neue Objekte importiert, ${data.outside} außerhalb des Inventars, ${data.existing} bereits vorhanden.`);
            },
            error: function(jqXHR, textStatus, errorThrown){
                if (textStatus == 'error') alert("Leider erzeugt die Abfrage einen Fehler auf dem Server.");
            }
        });
    });

    $('button#savePolygon').click(function(ev) {
        ev.preventDefault();
        var poly = invPolygonLayer.getSource().getFeatures()[0];
        var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
        // featureProjection will force output to WGS84
        var geom = geojsonFormat.writeGeometry(poly.getGeometry(), {featureProjection: proj_2056});
        var postParams = {geom: geom, csrfmiddlewaretoken: csrftoken};
        $.post(ev.target.dataset.url, postParams);
    });

    var map = new KSPMap({
        target: 'map',
        layers: [
          baselayer,
          new ol.layer.Vector({
            source: new ol.source.Vector({
                features: [geojsonFormat.readFeature(gem_vector)]
            }),
            style: gemeindeStyle
          })
        ],
        view: new ol.View({
          projection: proj_2056,
          center: municipalityCenter,
          zoom: 13
        }),
        interactions: ol.interaction.defaults().extend([selectIaction, modify])
    });

    var invPolygonLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: [geojsonFormat.readFeature(gem_vector)]
        }),
        style: inventoryStyle,
        selectable: true
    });
    map.addLayer(invPolygonLayer);

    map.addLayerFromGeoJSONUrl(inventory.plotobs_url, 'plotObsLayer', null, calcPlotNumbs);

    if (!inventory.is_passed) {
        map.addLayerFromGeoJSONUrl(
            inventory.plots_noobs_url, 'plotLayer',
            function(feature, resolution) {
                return feature.get('excluded') ? [excludedStyle] : (feature.get('in_previous') ? [centerStyle] : [lightGreyStyle]);
            },
            calcPlotNumbs
        );
        map['newPlotLayer'] = new ol.layer.Vector({
            source: new ol.source.Vector(), style: newPlotStyle
        });
        map.addLayer(map.newPlotLayer);
    }
});
