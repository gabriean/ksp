from decimal import Decimal

from django import template
from django.contrib.gis.gdal import SpatialReference, CoordTransform
from django.utils import numberformat
from django.utils.html import format_html

from gemeinde.models import Gemeinde

register = template.Library()

ch1903 = SpatialReference(21781)
ch1903plus = SpatialReference(2056)
trans = CoordTransform(ch1903, ch1903plus)


@register.filter
def div(value, arg):
    if value is None:
        return ''
    return value / arg


@register.filter
def gem_name(gem_code):
    """
    Display a municipality plz/name from its code (A4355).
    """
    gem = Gemeinde.from_code(gem_code)
    if not gem:
        return gem_code
    return "%s %s" % (gem.plz, gem.name)

@register.filter
def gem_name_list(gem_qs):
    """
    Display a comma-separated list of municipality names from a queryset.
    """
    return ", ".join([gem.name for gem in gem_qs])


@register.filter(is_safe=False)
def display_value(value):
    """Handle display of values with None or decimals."""
    if isinstance(value, list) and len(value) == 2:
        tech_value, value = value
    if value is None:
        return ''
    elif isinstance(value, (float, Decimal)):
        return numberformat.format(value, ',', decimal_pos=1, grouping=3, thousand_sep=' ')
    return {'o': 'Öffentlich', 'p': 'Privat'}.get(value, value)


@register.filter
def ordered_heads(field_names, ordering):
    """Output query_line (tuple) with indices from ordering"""
    for idx, classes in ordering:
        yield format_html(
            '<th class="{}" {}>{}</th>',
            ' '.join(classes),
            'title="Standardfehler"' if 'sf' in classes else '',
            field_names[idx]
        )

@register.filter
def ordered_vals(query_line, ordering):
    """Output query_line (tuple) with indices from ordering"""
    for idx, classes in ordering:
        yield format_html(
            '<td class="{}">{}</td>', ' '.join(classes), display_value(query_line[idx])
        )

@register.filter
def coords_to_js(geom):
    if geom:
        return eval(geom.json)['coordinates']
    return ''

@register.filter
def swiss_rounded(coords):
    if coords:
        return '%s, %s' % (int(round(coords[0])), int(round(coords[1])))
    return '-'

@register.filter
def as_geojson(geom):
    if geom is None:
        return '{}'
    if geom.srid != 2056:
        geom.transform(trans)
    return ('{"type": "Feature", "geometry":%s,'
            ' "crs": { "type": "name", "properties": { "name": "urn:x-ogc:def:crs:EPSG:%s" } } }' % (geom.json, geom.srid))
