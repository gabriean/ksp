"""
**************
Database Views
**************
"""
import re
import subprocess

from django.conf import settings
from django.db import connection, models
from django.utils.functional import cached_property

from gemeinde.models import Gemeinde
from .models import (
    AdminRegion, CrownClosure, DevelStage, FoliageDensity, OwnerType, Plot,
    PlotObs, StandStructure, TreeGroup, TreeSpecies, Vita,
)


class DBView:
    """Introspection class for a database view."""
    def __init__(self, name=None, oid=None):
        if not name and not oid:
            raise ValueError("You must provide either name or oid")
        self._name = name
        self._oid = oid
        self._relkind = None

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def __lt__(self, other):
         return self.name.lower() < other.name.lower()

    @property
    def oid(self):
        if self._oid:
            return int(self._oid)
        with connection.cursor() as cursor:
            cursor.execute("SELECT oid, relkind FROM pg_class WHERE relname=%s", [self._name])
            try:
                self._oid, self._relkind = cursor.fetchone()
            except TypeError:
                pass
        return self._oid

    @property
    def name(self):
        if self._name:
            return self._name
        with connection.cursor() as cursor:
            cursor.execute("SELECT relname, relkind FROM pg_class WHERE oid=%s", [self._oid])
            try:
                self._name, self._relkind = cursor.fetchone()
            except TypeError:
                pass
        return self._name

    @property
    def is_view(self):
        if self._relkind is None:
            self.name, self.oid  # Force db query
        return self._relkind in ('v', 'm')

    @cached_property
    def definition(self):
        with connection.cursor() as cursor:
            if self.is_view:
                cursor.execute("SELECT pg_get_viewdef(%s, true)", [self.oid])
                try:
                    definition = cursor.fetchone()[0]
                except TypeError:
                    return None
            else:
                try:
                    dump = subprocess.check_output(
                        "pg_dump -U %s -t 'public.%s' --schema-only --no-acl ksp2" % (
                        settings.DATABASES['default']['USER'], self.name),
                        env={'PGPASSWORD': settings.DATABASES['default'].get('PASSWORD', '')},
                        shell=True
                    )
                except subprocess.CalledProcessError as err:
                    return "Error retrieving table structure (%s)." % err
                # Grab the CREATE TABLE part
                capture = False
                definition = ''
                comments = self.field_comments()
                for line in dump.decode('utf-8').splitlines():
                    if 'CREATE TABLE' in line:
                        capture = True
                    if capture:
                        first_word = line.strip().split()[0]
                        comment = ''
                        if first_word in comments and comments[first_word] is not None:
                            comment = '    -- %s\n' % comments[first_word]
                        definition += comment + line + "\n"
                        if line == ");":
                            break
        return definition

    @cached_property
    def comment(self):
        with connection.cursor() as cursor:
            cursor.execute("SELECT obj_description(%s, 'pg_class')", [self.oid])
            return cursor.fetchone()[0]

    def field_comments(self):
        query = """SELECT
            cols.column_name,
            (
                SELECT
                    pg_catalog.col_description(c.oid, cols.ordinal_position::int)
                FROM
                    pg_catalog.pg_class c
                WHERE
                    c.oid = (SELECT ('"' || cols.table_name || '"')::regclass::oid)
                    AND c.relname = cols.table_name
            ) AS column_comment
        FROM
            information_schema.columns cols
        WHERE
            cols.table_catalog = %s AND cols.table_name = %s AND cols.table_schema = 'public';
        """
        with connection.cursor() as cursor:
            cursor.execute(query, (settings.DATABASES['default']['NAME'], self.name))
            return {line[0]: line[1] for line in cursor.fetchall()}

    def sub_views(self, recurse=False, only_views=False):
        """Return a list of dependent views or tables."""
        if not self.is_view:
            return []
        matches = re.findall(r'(?:FROM|JOIN)\s(\w+)', self.definition)
        sub_views = [DBView(name=view_name) for view_name in set(matches)]
        if recurse:
            [sub_views.extend(dbv.sub_views(recurse=True)) for dbv in sub_views]
        if only_views:
            return [sb for sb in sub_views if sb.is_view]
        return sub_views


class HomepageView(models.Model):
    gemeinde = models.CharField(max_length=50, db_column="gemeindename")
    jahr = models.SmallIntegerField("Aufnahmejahr", db_column="year")
    probepunkte = models.IntegerField("Anzahl Probepunkte", db_column="Anzahl Probepunkte")
    waldflaeche = models.DecimalField("theoretische Waldfläche pro ha", max_digits=5, decimal_places=1,
        db_column="theoretische Waldfläche ha")
    probebaum_abs = models.IntegerField("Anzahl Probebäume", db_column="Anzahl Probebaeume")
    stammzahl_ha = models.IntegerField("Stammzahl pro ha", db_column="Stammzahl pro ha")
    stammzahl_stdf = models.FloatField("Sf [%]", db_column="%% Standardfehler")
    volumen_ha = models.DecimalField("Volumen [m3 pro ha]", max_digits=5, decimal_places=1,
        db_column="Volumen pro ha")
    volumen_stdf = models.FloatField("Sf [%]", db_column="%% Standardfehler2")
    grundflaeche_ha = models.DecimalField("Grundflaeche [m2 pro ha]", max_digits=5, decimal_places=1,
        db_column="Grundflaeche pro ha")
    grundflaeche_stdf = models.FloatField("Sf [%]", db_column="%% Standardfehler3")
    inv_period = models.SmallIntegerField()
    waldflaeche_bestand = models.IntegerField("Waldfläche gemäss Bestandeskarte")

    class Meta:
        db_table = 'bv_web_homepage_holzproduktion'
        managed = False


class LatestInventories(models.Model):
    municipality = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', on_delete=models.PROTECT)
    inv_from = models.DateField("Von")
    inv_to = models.DateField("Bis")

    class Meta:
        db_table = 'bv_latest_inventory'
        managed = False


class PlotInRegionView(models.Model):
    """A materialized view linking plots to region polygons."""
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING)
    adminregion = models.ForeignKey(AdminRegion, on_delete=models.DO_NOTHING)
    region_name = models.CharField(max_length=100)
    region_type = models.CharField(max_length=20)

    class Meta:
        db_table = 'plot_in_region'
        managed = False


class GemeindenWithFlaecheView(models.Model):
    """A materialized view storing the forest surface calculated from waldbestandkarte."""
    gid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, unique=True, db_column='gemeindename')
    # + all other gemeindegrenzen_bsbl fields
    waldflaeche = models.IntegerField()

    class Meta:
        db_table = 'gemeinden_with_flaeche'
        managed = False


class BWARTEN(models.Model):
    plot_obs = models.OneToOneField(PlotObs, primary_key=True, verbose_name="OBS ID", on_delete=models.DO_NOTHING)
    bwarten = models.IntegerField(db_column='BWARTEN', verbose_name="Gehölzartenvielfalt (BWARTEN)")
    num_spec = models.IntegerField(db_column='Anzahl Baumarten', verbose_name="Anzahl Baumarten")
    special_spec = models.IntegerField(db_column='Special Species', verbose_name="Special Species")

    class Meta:
        db_table = 'bv_bwarten_anz_baumarten_special_species'
        managed = False


class BWNATURN(models.Model):
    plot_obs = models.OneToOneField(PlotObs, primary_key=True, verbose_name="OBS ID", on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING)
    code = models.CharField(max_length=6)
    bwnaturn = models.IntegerField(db_column="BWNATURN", verbose_name="Naturnähe des Nadelholzanteils (BWNATURN)")
    fichte_perc = models.FloatField(db_column="%% Basalflächenanteil der Fichte",
        verbose_name="% Basalflächenanteil der Fichte")
    nadel_perc = models.FloatField(db_column="%% Basalflächenanteil Nadelholz",
        verbose_name="% Basalflächenanteil Nadelholz")
    nadel_wtanne_perc = models.FloatField(db_column="%% Basalflächenanteil Nadelholz ohne Tanne",
        verbose_name="% Basalflächenanteil Nadelholz ohne Tanne")

    class Meta:
        db_table = 'bv_bwnaturn_ndh_fi_ndh_ohne_ta'
        managed = False


class BWSTRU1M(models.Model):
    id = models.OneToOneField(PlotObs, primary_key=True, verbose_name="OBS ID", db_column='id',
        on_delete=models.DO_NOTHING)
    stand_devel_stage = models.ForeignKey(DevelStage, verbose_name="Entwicklungsstufe",
        on_delete=models.DO_NOTHING)
    stand_crown_closure = models.ForeignKey(CrownClosure, verbose_name="Schlussgrad des Bestandes",
        on_delete=models.DO_NOTHING)
    stand_structure = models.ForeignKey(StandStructure, verbose_name="Schichtung des Bestandes",
        on_delete=models.DO_NOTHING)
    bwstru1m = models.IntegerField(db_column="BWSTRU1M", verbose_name="Strukturvielfalt (BWSTRU1M)")

    class Meta:
        db_table = 'bv_bwstru1m'
        managed = False


class Biotopwert(models.Model):
    id = models.OneToOneField(PlotObs, primary_key=True, verbose_name="OBS ID", db_column='id',
        on_delete=models.DO_NOTHING)
    municipality = models.ForeignKey(Gemeinde, verbose_name="Gemeinde", on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField(verbose_name="Jahr")
    owner_type = models.ForeignKey(OwnerType, verbose_name="Besitzertyp", null=True, blank=True,
        on_delete=models.DO_NOTHING)
    biolfi1m = models.FloatField(verbose_name="Biotopwert (BIOLFI1M)", db_column="BIOLFI1M")
    bioklass = models.SmallIntegerField(verbose_name="Biotopwert Klasse", db_column="Biotopwert Klassen")

    class Meta:
        db_table = "bv_biotopwert_klassen_pro_plotobs_year_ownertype"
        managed = False


class BaseHolzProduktion(models.Model):
    # This is not a real primary key, but the flag does prevent Django from auto-creating a non-existent id key
    plotobs = models.ForeignKey(PlotObs, db_column='id', primary_key=True, on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, db_column='plot_id', on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField(verbose_name="Jahr")

    gemeinde = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', db_column="municipality_id", on_delete=models.DO_NOTHING)
    anzahl_baume_abs = models.IntegerField("Anzahl Probebäume", db_column="Anzahl Probebaeume")
    anzahl_baume_rel = models.FloatField("Stammzahl/ha", db_column="Stammzahl pro ha")
    volumen_abs = models.DecimalField("Volumen [m3]", max_digits=5, decimal_places=1, db_column="Volumen m3")
    volumen_rel = models.FloatField("Volumen [m3/ha]", db_column="Volumen pro ha")
    grundflaeche_abs = models.FloatField("Grundfläche [m2]", db_column="Grundflaeche m2")
    grundflaeche_rel = models.FloatField("Grundfläche [m2/ha]", db_column="Grundflaeche pro ha")

    class Meta:
        abstract = True


class HolzProduktion(BaseHolzProduktion):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs"
        managed = False


class ModelProSpec(models.Model):
    # This is not a real primary key, but the flag does prevent Django from auto-creating a non-existent id key
    id = models.CharField(max_length=20, primary_key=True)
    plotobs = models.ForeignKey(PlotObs, db_column='plot_obs_id', on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, db_column='plot_id', on_delete=models.DO_NOTHING)
    spec = models.ForeignKey(TreeSpecies, verbose_name="Baumart", db_column='tree_spec_id', on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField(verbose_name="Jahr")

    gemeinde = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', db_column="municipality_id", on_delete=models.DO_NOTHING)
    anzahl_baume_abs = models.IntegerField("Anzahl Probebäume", db_column="Anzahl Probebaeume")
    anzahl_baume_rel = models.FloatField("Stammzahl/ha", db_column="Stammzahl pro ha")
    volumen_abs = models.DecimalField("Volumen [m3]", max_digits=5, decimal_places=1, db_column="Volumen m3")
    volumen_rel = models.FloatField("Volumen [m3/ha]", db_column="Volumen pro ha")
    grundflaeche_abs = models.FloatField("Grundfläche [m2]", db_column="Grundflaeche m2")
    grundflaeche_rel = models.FloatField("Grundfläche [m2/ha]", db_column="Grundflaeche pro ha")

    class Meta:
        abstract = True


class ModelProSpecGroup(models.Model):
    # This is not a real primary key, but the flag does prevent Django from auto-creating a non-existent id key
    id = models.CharField(max_length=20, primary_key=True)
    plotobs = models.ForeignKey(PlotObs, db_column='plot_obs_id', on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, db_column='plot_id', on_delete=models.DO_NOTHING)
    group = models.ForeignKey(TreeGroup, verbose_name="Baumartgruppe", db_column='tree_group_id', on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField(verbose_name="Jahr")

    gemeinde = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', db_column="municipality_id", on_delete=models.DO_NOTHING)
    anzahl_baume_abs = models.IntegerField("Anzahl Probebäume", db_column="Anzahl Probebaeume")
    anzahl_baume_rel = models.FloatField("Stammzahl/ha", db_column="Stammzahl pro ha")
    volumen_abs = models.DecimalField("Volumen [m3]", max_digits=5, decimal_places=1, db_column="Volumen m3")
    volumen_rel = models.FloatField("Volumen [m3/ha]", db_column="Volumen pro ha")
    grundflaeche_abs = models.FloatField("Grundfläche [m2]", db_column="Grundflaeche m2")
    grundflaeche_rel = models.FloatField("Grundfläche [m2/ha]", db_column="Grundflaeche pro ha")

    class Meta:
        abstract = True


class HolzProduktionProSpec(ModelProSpec):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_allspec"
        managed = False


class HolzProduktionProSpecGroup(ModelProSpecGroup):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_specgroup"
        managed = False


class Totholz(BaseHolzProduktion):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_totholz"
        managed = False


class TotholzProSpec(ModelProSpec):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_allspec_totholz"
        managed = False


class TotholzProSpecGroup(ModelProSpecGroup):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_specgroup_totholz"
        managed = False


class Einwuchs(BaseHolzProduktion):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_einwuchs"
        managed = False


class EinwuchsProSpec(ModelProSpec):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_allspec_einwuchs"
        managed = False


class EinwuchsProSpecGroup(ModelProSpecGroup):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_specgroup_einwuchs"
        managed = False


class Nutzung(BaseHolzProduktion):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_nutzung"
        managed = False


class NutzungProSpec(ModelProSpec):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_allspec_nutzung"
        managed = False


class NutzungProSpecGroup(ModelProSpecGroup):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_und_specgroup_nutzung"
        managed = False


class Zuwachs(models.Model):
    id = models.ForeignKey(PlotObs, db_column='id', primary_key=True, on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING)
    leb_stem = models.IntegerField("Lebende Stammzahl")
    #leb_perc = models.FloatField()  # Unused in interface
    leb_growth_ha = models.FloatField("Lebender Zuwachs/ha/Jahr")
    ein_stem = models.IntegerField("Einwuchs Stammzahl")
    ein_growth_ha = models.FloatField("Einwuchs Zuwachs/ha/Jahr")
    mor_stem = models.IntegerField("Tote Stammzahl")
    mor_growth_ha = models.FloatField("Tote/genutzte Bäume Zuwachs/ha/Jahr")
    growth_ha_total = models.FloatField("Totaler Zuwachs/ha/Jahr")

    class Meta:
        db_table = "bv_zuwachs_total"
        managed = False


class BodenSchaden(models.Model):
    id = models.ForeignKey(PlotObs, db_column='id', primary_key=True, on_delete=models.DO_NOTHING)
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING)
    municipality = models.ForeignKey(Gemeinde, verbose_name="Gemeinde", on_delete=models.DO_NOTHING)
    year = models.SmallIntegerField("Jahr")
    #soil_compaction =
    code0 = models.SmallIntegerField("nicht, wenig verdichtet (0-10%)")
    code1 = models.SmallIntegerField("teilweise verdichtet (10-30%)")
    code2 = models.SmallIntegerField("flächig verdichtet >30%")

    class Meta:
        db_table = "bv_bodenschaden"
        managed = False


class Giganten(models.Model):
    #id = TreeObs.id
    species = models.CharField(max_length=100)
    dbh = models.SmallIntegerField("BHD")
    plot_obs = models.ForeignKey(PlotObs, on_delete=models.DO_NOTHING, db_column='plot_obs_id')
    year = models.SmallIntegerField("Jahr")
    plot = models.ForeignKey(Plot, on_delete=models.DO_NOTHING, verbose_name="Aufnahmepunkt")
    vita = models.ForeignKey(Vita, on_delete=models.DO_NOTHING, verbose_name="Lebenslauf")
    st_x = models.IntegerField("X Koord.")
    st_y = models.IntegerField("Y Koord.")
    sealevel= models.SmallIntegerField("Höhe ü. Meer", null=True)
    municipality = models.ForeignKey(Gemeinde, verbose_name='Gemeinde', on_delete=models.DO_NOTHING)

    class Meta:
        db_table = "bv_giganten"
        managed = False


class Eschentriebsterben(models.Model):
    #id = TreeObs.id
    plot_obs = models.ForeignKey(PlotObs, on_delete=models.DO_NOTHING, db_column='obs_id')
    volumen_m3 = models.DecimalField("Volumen [m3]", max_digits=10, decimal_places=4)
    ash_dieback = models.ForeignKey(FoliageDensity, verbose_name="Eschentriebsterben",
        on_delete=models.DO_NOTHING)

    class Meta:
        db_table = "bv_eschentriebsterben"
        managed = False
