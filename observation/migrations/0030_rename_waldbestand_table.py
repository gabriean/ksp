from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0029_add_density'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='waldbestandkarte',
            table='waldbestandkarte',
        ),
    ]
