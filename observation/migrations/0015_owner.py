from django.contrib.gis.db.models.fields import MultiPolygonField
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0014_zuwachs_lebend_pro_plotobs'),
    ]

    operations = [
        migrations.CreateModel(
            name='OwnerType2',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True)),
            ],
            options={
                'db_table': 'owner_type',
            },
        ),
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(max_length=10, unique=True)),
                ('name', models.CharField(max_length=100)),
                ('geom', MultiPolygonField(blank=True, null=True, srid=2056)),
                ('otype', models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, to='observation.OwnerType2', verbose_name='Besitzertyp')),
            ],
            options={
                'db_table': 'owner',
            },
        ),
        migrations.RunSQL("""
 CREATE MATERIALIZED VIEW plot_in_owner AS
 SELECT row_number() OVER ()::integer AS id,
    plot.id AS plot_id,
    owner.id AS owner_id,
    owner.name AS owner_name,
    owner.otype_id
   FROM plot, owner
  WHERE st_within(plot.the_geom, owner.geom)
WITH DATA;""",
            "DROP MATERIALIZED VIEW plot_in_owner"
        ),
        migrations.CreateModel(
            name='PlotInOwnerView',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('owner_name', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'plot_in_owner',
                'managed': False,
            },
        ),
    ]
