from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0020_plotobs_owner'),
    ]

    operations = [
        migrations.RunSQL("DROP MATERIALIZED VIEW plot_in_owner"),
        migrations.DeleteModel(
            name='PlotInOwnerView',
        ),
    ]
