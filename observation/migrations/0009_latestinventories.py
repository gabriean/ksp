from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0008_check_forest_edge_values'),
    ]

    operations = [
        migrations.CreateModel(
            name='LatestInventories',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('inv_from', models.DateField(verbose_name='Von')),
                ('inv_to', models.DateField(verbose_name='Bis')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_latest_inventory',
            },
        ),

        migrations.RunSQL("""
 CREATE OR REPLACE VIEW bv_latest_inventory AS 
 SELECT DISTINCT ON (inventory.municipality_id) inventory.id,
    inventory.municipality_id,
    inventory.inv_from,
    inventory.inv_to
   FROM inventory
  WHERE inventory.inv_to < now()
  ORDER BY inventory.municipality_id, inventory.inv_from DESC;""",
            "DROP VIEW bv_latest_inventory"
        ),
    ]
