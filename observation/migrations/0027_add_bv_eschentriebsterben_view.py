from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0026_add_bv_giganten_view'),
    ]

    operations = [
        migrations.RunSQL("""
CREATE OR REPLACE VIEW public.bv_eschentriebsterben AS
 SELECT
    tree_obs.id,
    obs_id,
    ksp_volume_et_bl(tree_obs.dbh) AS volumen_m3,
    ash_dieback_id
   FROM tree_obs
     LEFT JOIN plot_obs ON tree_obs.obs_id = plot_obs.id
     LEFT JOIN tree ON tree_obs.tree_id = tree.id
     LEFT JOIN tree_spec ON tree.spec_id = tree_spec.id
  WHERE species = 'Esche' AND ash_dieback_id IS NOT NULL;""",
            "DROP VIEW public.bv_eschentriebsterben"
        ),
    ]
