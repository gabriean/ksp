from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0002_initial_views'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='plotobs',
            name='inv_period',
        ),
    ]
