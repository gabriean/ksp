from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0028_add_tarif_klasses'),
    ]

    operations = [
        migrations.AddField(
            model_name='inventory',
            name='default_density',
            field=models.IntegerField(blank=True, choices=[(10000, '1/ha (10000m2)'), (20000, '0,5/ha (20000m2)')], null=True, verbose_name='StandardDichte'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='density',
            field=models.IntegerField(null=True, verbose_name='Dichte (m2)'),
        ),
    ]
