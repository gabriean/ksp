from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0003_remove_plotobs_inv_period'),
    ]

    operations = [
        migrations.AlterField(
            model_name='inventory',
            name='municipality',
            field=models.ForeignKey(on_delete=models.deletion.PROTECT, to='gemeinde.Gemeinde', verbose_name='Gemeinde'),
        ),
    ]
