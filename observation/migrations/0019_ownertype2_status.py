from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0018_specgroup_unmanaged_views'),
    ]

    operations = [
        migrations.AddField(
            model_name='ownertype2',
            name='status',
            field=models.CharField(choices=[('o', 'Öffentlich'), ('p', 'Privat')], default='o', max_length=1),
            preserve_default=False,
        ),
    ]
