from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0013_replace_period_zuwachs_views'),
    ]

    operations = [
        migrations.RunSQL("""
CREATE OR REPLACE VIEW public.bv_secondobs AS 
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.year,
    plot_obs.municipality_id,
    plot_obs.forest_edgef,
    plot_obs.inv_team_id,
    inventory.ordering as invent1,
    prev_obs.ordering as invent0,
    plot_obs.year - prev_obs.year AS year_diff
   FROM plot_obs
     LEFT JOIN inventory ON plot_obs.inv_team_id = inventory.id
     LEFT JOIN ( SELECT pobs2.id,
            pobs2.plot_id,
            pobs2.year,
            inventory_1.ordering
           FROM plot_obs pobs2
             LEFT JOIN inventory inventory_1 ON pobs2.inv_team_id = inventory_1.id
        ) prev_obs ON plot_obs.plot_id = prev_obs.plot_id AND inventory.ordering = (prev_obs.ordering + 1)
  WHERE prev_obs.id IS NOT NULL;"""
        ),
        migrations.RunSQL("""
CREATE OR REPLACE VIEW public.bv_zuwachs_lebend_pro_plotobs AS 
 SELECT po.id,
    max(po.plot_id) AS plot_id,
    COALESCE(sum(zuw.growth_year), 0::double precision) AS growth,
    COALESCE(sum(zuw.growth_year), 0::double precision) * ksp_lokale_dichte(max(po.forest_edgef))::double precision AS growth_ha,
    count(zuw.id) AS nb_stem,
    COALESCE(sum(zuw.growth_year) / NULLIF(count(zuw.id), 0)::double precision * ksp_lokale_dichte(max(po.forest_edgef))::double precision, 0::double precision) AS growth_stem_ha
   FROM bv_secondobs po
     LEFT JOIN bv_zuwachs_lebend zuw ON zuw.obs_id = po.id
  GROUP BY po.id;"""
        ),
        migrations.RunSQL("""
CREATE OR REPLACE VIEW public.bv_zuwachs_totgenutzt_pro_plotobs AS 
 SELECT leb_zuwachs.id,
    leb_zuwachs.plot_id,
    stem_vita.leb_perc,
    stem_vita.nutzung + stem_vita.tot AS mor_stem,
        CASE
            WHEN stem_vita.leb_perc < 0.5::double precision AND stem_vita.lebend < 7 THEN 0::double precision
            WHEN (stem_vita.nutzung + stem_vita.tot) = 0 THEN 0::double precision
            ELSE (stem_vita.nutzung + stem_vita.tot)::double precision * leb_zuwachs.growth_stem_ha / 2.0::double precision
        END AS mor_growth_ha
   FROM bv_zuwachs_lebend_pro_plotobs leb_zuwachs
     LEFT JOIN ( SELECT vi.id,
            vi.lebend,
            vi.nutzung,
            vi.tot,
                CASE
                    WHEN vi.lebend = 0 THEN 0.0::double precision
                    ELSE vi.lebend::double precision / (vi.lebend + vi.nutzung + vi.tot)::double precision
                END AS leb_perc
           FROM bv_stem_pro_plotobs_pro_vita vi) stem_vita ON leb_zuwachs.id = stem_vita.id;"""
        ),
        migrations.RunSQL("""
CREATE OR REPLACE VIEW public.bv_zuwachs_einwuchs AS 
 SELECT tobs.id,
    tobs.tree_id,
    tobs.obs_id,
    bv_secondobs.invent0,
    bv_secondobs.invent1,
    tobs.dbh,
    tobs.dbh - 11 AS dbh_diff,
    bv_secondobs.year_diff,
    GREATEST(0::double precision, (ksp_volume_et_bl(tobs.dbh) - ksp_volume_et_bl(11::smallint))::double precision / (bv_secondobs.year_diff::double precision / 2.0::double precision)) AS growth_year
   FROM tree_obs tobs
     LEFT JOIN plot_obs po ON tobs.obs_id = po.id
     LEFT JOIN bv_secondobs ON po.id=bv_secondobs.id
     LEFT JOIN lt_vita vita ON tobs.vita_id = vita.id
  WHERE vita.code::text = 'e'::text;"""
        ),
        migrations.RunSQL("""
CREATE OR REPLACE VIEW public.bv_zuwachs_einwuchs_pro_plotobs AS 
 SELECT po.id,
    max(po.plot_id) AS plot_id,
    COALESCE(sum(zuw.growth_year), 0::double precision) AS growth,
    COALESCE(sum(zuw.growth_year) * ksp_lokale_dichte(max(po.forest_edgef))::double precision, 0::double precision) AS growth_ha,
    count(zuw.obs_id) AS nb_stem,
    sum(zuw.growth_year) / count(*)::double precision * ksp_lokale_dichte(max(po.forest_edgef))::double precision AS growth_stem_ha
   FROM bv_secondobs po
     LEFT JOIN bv_zuwachs_einwuchs zuw ON zuw.obs_id = po.id
  GROUP BY po.id;"""
        ),
        migrations.RunSQL("""
CREATE OR REPLACE VIEW public.bv_zuwachs_total AS 
 SELECT leb_zuwachs.id,
    leb_zuwachs.plot_id,
    leb_zuwachs.nb_stem AS leb_stem,
    tot_zuwachs.leb_perc,
    leb_zuwachs.growth_ha AS leb_growth_ha,
    ein_zuwachs.nb_stem AS ein_stem,
    ein_zuwachs.growth_ha AS ein_growth_ha,
    tot_zuwachs.mor_stem,
    tot_zuwachs.mor_growth_ha,
    leb_zuwachs.growth_ha + ein_zuwachs.growth_ha + COALESCE(tot_zuwachs.mor_growth_ha, 0::double precision) AS growth_ha_total
   FROM bv_zuwachs_lebend_pro_plotobs leb_zuwachs
     LEFT JOIN bv_zuwachs_einwuchs_pro_plotobs ein_zuwachs ON leb_zuwachs.id = ein_zuwachs.id
     LEFT JOIN bv_zuwachs_totgenutzt_pro_plotobs tot_zuwachs ON leb_zuwachs.id = tot_zuwachs.id;"""
        ),
    ]
