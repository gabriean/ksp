from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0006_plot_sealevel_nullable'),
    ]

    operations = [
        migrations.AddField(
            model_name='treespecies',
            name='is_historic',
            field=models.BooleanField(default=False, help_text='True if the species should not be used for new observations'),
        ),
    ]
