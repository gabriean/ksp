from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0024_waldbestandkarte'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='waldbestandkarte',
            table='waldbestandkearte',
        ),
    ]
