import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0023_nutzung_using_prev_treeobs'),
    ]

    operations = [
        migrations.CreateModel(
            name='WaldBestandKarte',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('geom', django.contrib.gis.db.models.fields.PolygonField(srid=2056)),
                ('entwicklungstufe', models.ForeignKey(blank=True, db_column='entw', null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.DevelStage')),
                ('mischungsgrad', models.ForeignKey(blank=True, db_column='mg', null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.ForestMixture')),
                ('schlussgrad', models.ForeignKey(blank=True, db_column='sg', null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.CrownClosure')),
            ],
        ),
    ]
