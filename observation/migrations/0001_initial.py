import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('gemeinde', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='EinwuchsProSpec',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_allspec_einwuchs',
            },
        ),
        migrations.CreateModel(
            name='HolzProduktionProSpec',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_allspec',
            },
        ),
        migrations.CreateModel(
            name='HomepageView',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gemeinde', models.CharField(db_column='gemeindename', max_length=50)),
                ('jahr', models.SmallIntegerField(db_column='year', verbose_name='Aufnahmejahr')),
                ('probepunkte', models.IntegerField(db_column='Anzahl Probepunkte', verbose_name='Anzahl Probepunkte')),
                ('waldflaeche', models.DecimalField(db_column='theoretische Waldfläche ha', decimal_places=1, max_digits=5, verbose_name='theoretische Waldfläche pro ha')),
                ('probebaum_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('stammzahl_ha', models.IntegerField(db_column='Stammzahl pro ha', verbose_name='Stammzahl pro ha')),
                ('stammzahl_stdf', models.FloatField(db_column='%% Standardfehler', verbose_name='Sf [%]')),
                ('volumen_ha', models.DecimalField(db_column='Volumen pro ha', decimal_places=1, max_digits=5, verbose_name='Volumen [m3 pro ha]')),
                ('volumen_stdf', models.FloatField(db_column='%% Standardfehler2', verbose_name='Sf [%]')),
                ('grundflaeche_ha', models.DecimalField(db_column='Grundflaeche pro ha', decimal_places=1, max_digits=5, verbose_name='Grundflaeche [m2 pro ha]')),
                ('grundflaeche_stdf', models.FloatField(db_column='%% Standardfehler3', verbose_name='Sf [%]')),
                ('inv_period', models.SmallIntegerField()),
            ],
            options={
                'managed': False,
                'db_table': 'bv_web_homepage_holzproduktion',
            },
        ),
        migrations.CreateModel(
            name='NutzungProSpec',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_allspec_nutzung',
            },
        ),
        migrations.CreateModel(
            name='PlotInRegionView',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('region_name', models.CharField(max_length=100)),
                ('region_type', models.CharField(max_length=20)),
            ],
            options={
                'managed': False,
                'db_table': 'plot_in_region',
            },
        ),
        migrations.CreateModel(
            name='TotholzProSpec',
            fields=[
                ('id', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_und_allspec_totholz',
            },
        ),
        migrations.CreateModel(
            name='Acidity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Acidität',
                'db_table': 'lt_acidity',
                'verbose_name_plural': 'Aciditäten',
            },
        ),
        migrations.CreateModel(
            name='AdminRegion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(max_length=10, unique=True)),
                ('name', models.CharField(max_length=100)),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=2056)),
            ],
            options={
                'db_table': 'adminregion',
            },
        ),
        migrations.CreateModel(
            name='CrownClosure',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
                ('bwstru1_val', models.SmallIntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Schlussgrad',
                'db_table': 'lt_crown_closure',
                'verbose_name_plural': 'Schlussgrade',
            },
        ),
        migrations.CreateModel(
            name='CrownClosure2015',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
                ('bwstru1_val', models.SmallIntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Schlussgrad vor 2015',
                'db_table': 'lt_crown_closure_2015',
                'verbose_name_plural': 'Schlussgrade vor 2015',
            },
        ),
        migrations.CreateModel(
            name='CrownForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(blank=True, default='')),
            ],
            options={
                'verbose_name': 'Kronenform',
                'db_table': 'lt_crown_form',
                'verbose_name_plural': 'Kronenformen',
            },
        ),
        migrations.CreateModel(
            name='CrownLength',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(blank=True, default='')),
            ],
            options={
                'verbose_name': 'Kronenlänge',
                'db_table': 'lt_crown_len',
                'verbose_name_plural': 'Kronenlängen',
            },
        ),
        migrations.CreateModel(
            name='Damage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Schadenart',
                'db_table': 'lt_damage',
                'verbose_name_plural': 'Schadenarten',
            },
        ),
        migrations.CreateModel(
            name='DamageCause',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Schadensursache',
                'db_table': 'lt_damage_cause',
                'verbose_name_plural': 'Schadensursachen',
            },
        ),
        migrations.CreateModel(
            name='DevelStage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=2, unique=True)),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
                ('bwstru1_val', models.SmallIntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Entwicklungsstufe',
                'ordering': ('code',),
                'db_table': 'lt_devel_stage',
                'verbose_name_plural': 'Entwicklungsstufen',
            },
        ),
        migrations.CreateModel(
            name='DevelStage2015',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
                ('bwstru1_val', models.SmallIntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Entwicklungsstufe vor 2015',
                'db_table': 'lt_devel_stage_2015',
                'verbose_name_plural': 'Entwicklungsstufen vor 2015',
            },
        ),
        migrations.CreateModel(
            name='FoliageDensity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_foliagedensity',
            },
        ),
        migrations.CreateModel(
            name='ForestForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Waldform',
                'db_table': 'lt_forest_form',
                'verbose_name_plural': 'Waldformen',
            },
        ),
        migrations.CreateModel(
            name='ForestMixture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100, verbose_name='Beschreibung')),
            ],
            options={
                'verbose_name': 'Mischungsgrad',
                'db_table': 'lt_forest_mixture',
                'verbose_name_plural': 'Mischungsgrade',
            },
        ),
        migrations.CreateModel(
            name='Gap',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_gap',
            },
        ),
        migrations.CreateModel(
            name='GefahrPotential',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Geology',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Geologie',
                'db_table': 'lt_geology',
                'verbose_name_plural': 'Geologie',
            },
        ),
        migrations.CreateModel(
            name='Inventory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, unique=True, verbose_name='Inventar')),
                ('team', models.CharField(max_length=100, verbose_name='Aufnahmeteam')),
                ('period', models.SmallIntegerField(choices=[(0, 'Alte Inventare (Ormalingen, Pratteln)'), (1, 'Erste Phase (1987-1999)'), (2, 'Zweite Phase (2000-2014)'), (3, 'Dritte Phase (2017-)')], null=True, verbose_name='Aufnahmeperiode')),
                ('inv_from', models.DateField(verbose_name='Von')),
                ('inv_to', models.DateField(verbose_name='Bis')),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=2056)),
                ('remarks', models.TextField(blank=True, verbose_name='Bemerkungen')),
                ('municipality', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='gemeinde.Gemeinde', verbose_name='Gemeinde')),
            ],
            options={
                'verbose_name': 'Inventar',
                'db_table': 'inventory',
                'verbose_name_plural': 'Inventare',
            },
        ),
        migrations.CreateModel(
            name='Layer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(blank=True, default='')),
            ],
            options={
                'db_table': 'lt_layer',
            },
        ),
        migrations.CreateModel(
            name='OwnerType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_owner_type',
            },
        ),
        migrations.CreateModel(
            name='Phytosoc',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=6)),
                ('description', models.CharField(max_length=200, verbose_name='Beschreibung')),
                ('inc_class', models.SmallIntegerField(verbose_name='Ertragsklasse')),
                ('ecol_grp', models.CharField(max_length=1, verbose_name='Ökologische Gruppe')),
                ('bwnatur', models.SmallIntegerField(blank=True, db_column='BWNATURN_WSL', null=True)),
            ],
            options={
                'verbose_name': 'Phytosoziologie',
                'db_table': 'phytosoc',
                'verbose_name_plural': 'Phytosoziologie',
            },
        ),
        migrations.CreateModel(
            name='Plot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.PositiveIntegerField()),
                ('the_geom', django.contrib.gis.db.models.fields.PointField(srid=2056)),
                ('point_exact', django.contrib.gis.db.models.fields.PointField(blank=True, null=True, srid=2056)),
                ('slope', models.SmallIntegerField(blank=True, null=True, verbose_name='Neigung')),
                ('exposition', models.CharField(blank=True, choices=[('_', 'flach'), ('N', 'Norden'), ('S', 'Süden')], default='', max_length=1)),
                ('sealevel', models.SmallIntegerField(verbose_name='Höhe über Meer (m)')),
                ('checked', models.BooleanField(default=False)),
                ('phytosoc', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Phytosoc')),
            ],
            options={
                'db_table': 'plot',
            },
        ),
        migrations.CreateModel(
            name='PlotObs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('year', models.SmallIntegerField(db_index=True, verbose_name='Aufnahmejahr')),
                ('inv_period', models.SmallIntegerField(null=True, verbose_name='Aufnahmeperiode')),
                ('area', models.SmallIntegerField(blank=True, null=True)),
                ('subsector', models.CharField(blank=True, max_length=2)),
                ('evaluation_unit', models.CharField(blank=True, max_length=4)),
                ('forest_clearing', models.BooleanField(default=False)),
                ('stand', models.SmallIntegerField(blank=True, null=True)),
                ('forest_edgef', models.DecimalField(decimal_places=1, max_digits=2, verbose_name='Waldrandfaktor')),
                ('gwl', models.FloatField(verbose_name='Gesamtwuchsleistung (gwl)')),
                ('remarks', models.TextField(blank=True, verbose_name='Bermerkungen')),
                ('iprobenr', models.SmallIntegerField(blank=True, null=True)),
                ('iflae', models.DecimalField(blank=True, decimal_places=6, max_digits=10, null=True)),
                ('itflae', models.DecimalField(blank=True, decimal_places=6, max_digits=10, null=True)),
                ('ikalter', models.IntegerField(blank=True, null=True)),
                ('ik6', models.IntegerField(blank=True, null=True)),
                ('ik7', models.IntegerField(blank=True, null=True)),
                ('ik8', models.IntegerField(blank=True, null=True)),
                ('ik9', models.IntegerField(blank=True, null=True)),
                ('izeitpkt', models.CharField(blank=True, max_length=1)),
                ('abt', models.CharField(blank=True, max_length=10)),
                ('ikflag', models.IntegerField(blank=True, null=True)),
                ('ifoa', models.CharField(blank=True, max_length=5)),
                ('irevf', models.IntegerField(blank=True, null=True)),
                ('idistr', models.CharField(blank=True, max_length=2)),
                ('iabt', models.CharField(blank=True, max_length=10)),
                ('istao', models.CharField(blank=True, max_length=10)),
                ('ia', models.CharField(blank=True, max_length=1)),
                ('ib', models.IntegerField(blank=True, null=True)),
                ('ic', models.CharField(blank=True, max_length=1)),
                ('id_2', models.CharField(blank=True, max_length=5)),
                ('ie', models.CharField(blank=True, max_length=2)),
                ('ibgr9', models.DecimalField(blank=True, decimal_places=6, max_digits=10, null=True)),
            ],
            options={
                'db_table': 'plot_obs',
            },
        ),
        migrations.CreateModel(
            name='Rank',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('explanation', models.TextField(blank=True, default='')),
            ],
            options={
                'verbose_name': 'Soziale Stellung',
                'db_table': 'lt_rank',
                'verbose_name_plural': 'Soziale Stellungen',
            },
        ),
        migrations.CreateModel(
            name='RegenFegen',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'lt_regenfegen',
            },
        ),
        migrations.CreateModel(
            name='RegenObs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('perc', models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True)),
                ('fegen', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.RegenFegen', verbose_name='Fegen Kategorie')),
            ],
            options={
                'db_table': 'regen_obs',
            },
        ),
        migrations.CreateModel(
            name='RegenType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Verjüngungsart',
                'db_table': 'lt_regen_type',
                'verbose_name_plural': 'Verjüngungsarten',
            },
        ),
        migrations.CreateModel(
            name='RegenValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=20)),
            ],
            options={
                'verbose_name': 'Verjüngungswert',
                'db_table': 'lt_regenvalue',
                'verbose_name_plural': 'Verjüngungswerte',
            },
        ),
        migrations.CreateModel(
            name='RegenVerbiss',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'lt_regenverbiss',
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_region',
            },
        ),
        migrations.CreateModel(
            name='RegionType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
            ],
            options={
                'verbose_name': 'Regiontyp',
                'db_table': 'regiontype',
                'verbose_name_plural': 'Regiontypen',
            },
        ),
        migrations.CreateModel(
            name='Relief',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_relief',
            },
        ),
        migrations.CreateModel(
            name='Schutzwald',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('haupt_gef_pot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='observation.GefahrPotential', verbose_name='Hauptgefahrpotential')),
                ('region', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='observation.AdminRegion')),
            ],
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_sector',
            },
        ),
        migrations.CreateModel(
            name='SoilCompaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Bodenverdichtung',
                'db_table': 'lt_soil_compaction',
                'verbose_name_plural': 'Bodenverdichtungen',
            },
        ),
        migrations.CreateModel(
            name='StandStructure',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SmallIntegerField()),
                ('description', models.CharField(max_length=100)),
                ('bwstru1_val', models.SmallIntegerField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Bestandesstruktur',
                'db_table': 'lt_stand_struct',
                'verbose_name_plural': 'Bestandesstrukturen',
            },
        ),
        migrations.CreateModel(
            name='Stem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
                ('dead_wood_volume', models.DecimalField(decimal_places=2, max_digits=3)),
            ],
            options={
                'verbose_name': 'Schaft',
                'db_table': 'lt_stem',
                'verbose_name_plural': 'Schäfte',
            },
        ),
        migrations.CreateModel(
            name='SurveyType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_survey_type',
            },
        ),
        migrations.CreateModel(
            name='Tree',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.PositiveSmallIntegerField(help_text='tree number')),
                ('azimuth', models.SmallIntegerField(help_text='angle in Grads')),
                ('distance', models.DecimalField(decimal_places=2, help_text='[in dm]', max_digits=6)),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='observation.Plot')),
            ],
            options={
                'verbose_name': 'Baum',
                'db_table': 'tree',
                'verbose_name_plural': 'Bäume',
            },
        ),
        migrations.CreateModel(
            name='TreeObs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dbh', models.SmallIntegerField(verbose_name='BHD')),
                ('stem_forked', models.BooleanField(blank=True, null=True, verbose_name='Zwiesel')),
                ('stem_height', models.IntegerField(blank=True, null=True, verbose_name='Stammhöhe')),
                ('woodpecker', models.BooleanField(blank=True, null=True, verbose_name='Spechtlöcher')),
                ('remarks', models.TextField(blank=True, verbose_name='Bemerkungen')),
                ('ianteil', models.DecimalField(blank=True, decimal_places=6, max_digits=10, null=True)),
                ('iflag', models.IntegerField(blank=True, null=True)),
                ('ih', models.DecimalField(blank=True, decimal_places=6, max_digits=8, null=True)),
                ('ibon', models.DecimalField(blank=True, decimal_places=6, max_digits=8, null=True)),
                ('ibanr', models.IntegerField(blank=True, null=True)),
                ('ialter', models.IntegerField(blank=True, null=True)),
                ('ikreis', models.IntegerField(blank=True, null=True)),
                ('iintens2', models.CharField(blank=True, max_length=1)),
                ('izstam', models.CharField(blank=True, max_length=1)),
                ('ianzahl', models.DecimalField(blank=True, decimal_places=6, max_digits=10, null=True)),
                ('schicht', models.IntegerField(blank=True, null=True)),
                ('iu', models.CharField(blank=True, max_length=20)),
                ('iz', models.IntegerField(blank=True, null=True)),
                ('ash_dieback', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.FoliageDensity', verbose_name='Eschentriebsterben')),
                ('crown_form', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.CrownForm', verbose_name='Kronenform')),
                ('crown_length', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.CrownLength', verbose_name='Kronenlänge')),
                ('damage', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Damage', verbose_name='Schaden')),
                ('damage_cause', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.DamageCause', verbose_name='Ursache')),
                ('layer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Layer', verbose_name='Schicht')),
            ],
            options={
                'db_table': 'tree_obs',
            },
        ),
        migrations.CreateModel(
            name='TreeSpecies',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('species', models.CharField(max_length=100)),
                ('code', models.SmallIntegerField(blank=True, help_text='National inventory value', null=True)),
                ('abbrev', models.CharField(max_length=4, unique=True)),
                ('is_tree', models.BooleanField(default=True, help_text='True if this is a real tree')),
                ('is_special', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'tree_spec',
            },
        ),
        migrations.CreateModel(
            name='UpdatedValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('table_name', models.CharField(max_length=50)),
                ('row_id', models.IntegerField()),
                ('field_name', models.CharField(max_length=50)),
                ('old_value', models.CharField(max_length=255)),
                ('new_value', models.CharField(max_length=255)),
                ('stamp', models.DateTimeField(auto_now_add=True)),
                ('comment', models.TextField(blank=True, default='')),
            ],
            options={
                'db_table': 'updated_value',
            },
        ),
        migrations.CreateModel(
            name='Vita',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Lebenslauf',
                'db_table': 'lt_vita',
                'verbose_name_plural': 'Lebensläufe',
            },
        ),
        migrations.CreateModel(
            name='Biotopwert',
            fields=[
                ('id', models.OneToOneField(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs', verbose_name='OBS ID')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('biolfi1m', models.FloatField(db_column='BIOLFI1M', verbose_name='Biotopwert (BIOLFI1M)')),
                ('bioklass', models.SmallIntegerField(db_column='Biotopwert Klassen', verbose_name='Biotopwert Klasse')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_biotopwert_klassen_pro_plotobs_year_ownertype',
            },
        ),
        migrations.CreateModel(
            name='BodenSchaden',
            fields=[
                ('id', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('code0', models.SmallIntegerField(verbose_name='nicht, wenig verdichtet (0-10%)')),
                ('code1', models.SmallIntegerField(verbose_name='teilweise verdichtet (10-30%)')),
                ('code2', models.SmallIntegerField(verbose_name='flächig verdichtet >30%')),
                ('code0_perc', models.FloatField(verbose_name='nicht, wenig verdichtet (0-10%) (in %)')),
                ('code1_perc', models.FloatField(verbose_name='teilweise verdichtet (10-30%) (in %)')),
                ('code2_perc', models.FloatField(verbose_name='flächig verdichtet >30% (in %)')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_bodenschaden',
            },
        ),
        migrations.CreateModel(
            name='BWARTEN',
            fields=[
                ('plot_obs', models.OneToOneField(on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs', verbose_name='OBS ID')),
                ('bwarten', models.IntegerField(db_column='BWARTEN', verbose_name='Gehölzartenvielfalt (BWARTEN)')),
                ('num_spec', models.IntegerField(db_column='Anzahl Baumarten', verbose_name='Anzahl Baumarten')),
                ('special_spec', models.IntegerField(db_column='Special Species', verbose_name='Special Species')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_bwarten_anz_baumarten_special_species',
            },
        ),
        migrations.CreateModel(
            name='BWNATURN',
            fields=[
                ('plot_obs', models.OneToOneField(on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs', verbose_name='OBS ID')),
                ('code', models.CharField(max_length=6)),
                ('bwnaturn', models.IntegerField(db_column='BWNATURN', verbose_name='Naturnähe des Nadelholzanteils (BWNATURN)')),
                ('fichte_perc', models.FloatField(db_column='%% Basalflächenanteil der Fichte', verbose_name='% Basalflächenanteil der Fichte')),
                ('nadel_perc', models.FloatField(db_column='%% Basalflächenanteil Nadelholz', verbose_name='% Basalflächenanteil Nadelholz')),
                ('nadel_wtanne_perc', models.FloatField(db_column='%% Basalflächenanteil Nadelholz ohne Tanne', verbose_name='% Basalflächenanteil Nadelholz ohne Tanne')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_bwnaturn_ndh_fi_ndh_ohne_ta',
            },
        ),
        migrations.CreateModel(
            name='BWSTRU1M',
            fields=[
                ('id', models.OneToOneField(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs', verbose_name='OBS ID')),
                ('bwstru1m', models.IntegerField(db_column='BWSTRU1M', verbose_name='Strukturvielfalt (BWSTRU1M)')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_bwstru1m',
            },
        ),
        migrations.CreateModel(
            name='Einwuchs',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_einwuchs',
            },
        ),
        migrations.CreateModel(
            name='HolzProduktion',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_grundf_vol_yx_pro_plotobs',
            },
        ),
        migrations.CreateModel(
            name='Nutzung',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_nutzung',
            },
        ),
        migrations.CreateModel(
            name='Totholz',
            fields=[
                ('plotobs', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs')),
                ('year', models.SmallIntegerField(verbose_name='Jahr')),
                ('anzahl_baume_abs', models.IntegerField(db_column='Anzahl Probebaeume', verbose_name='Anzahl Probebäume')),
                ('anzahl_baume_rel', models.FloatField(db_column='Stammzahl pro ha', verbose_name='Stammzahl/ha')),
                ('volumen_abs', models.DecimalField(db_column='Volumen m3', decimal_places=1, max_digits=5, verbose_name='Volumen [m3]')),
                ('volumen_rel', models.FloatField(db_column='Volumen pro ha', verbose_name='Volumen [m3/ha]')),
                ('grundflaeche_abs', models.FloatField(db_column='Grundflaeche m2', verbose_name='Grundfläche [m2]')),
                ('grundflaeche_rel', models.FloatField(db_column='Grundflaeche pro ha', verbose_name='Grundfläche [m2/ha]')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_grundf_vol_yx_pro_plotobs_totholz',
            },
        ),
        migrations.CreateModel(
            name='Zuwachs',
            fields=[
                ('id', models.ForeignKey(db_column='id', on_delete=django.db.models.deletion.DO_NOTHING, primary_key=True, serialize=False, to='observation.PlotObs')),
                ('leb_stem', models.IntegerField(verbose_name='Lebende Stammzahl')),
                ('leb_growth_ha', models.FloatField(verbose_name='Lebender Zuwachs/ha/Jahr')),
                ('ein_stem', models.IntegerField(verbose_name='Einwuchs Stammzahl')),
                ('ein_growth_ha', models.FloatField(verbose_name='Einwuchs Zuwachs/ha/Jahr')),
                ('mor_stem', models.IntegerField(verbose_name='Tote Stammzahl')),
                ('mor_growth_ha', models.FloatField(verbose_name='Tote/genutzte Bäume Zuwachs/ha/Jahr')),
                ('growth_ha_total', models.FloatField(verbose_name='Totaler Zuwachs/ha/Jahr')),
            ],
            options={
                'managed': False,
                'db_table': 'bv_zuwachs_total',
            },
        ),
        migrations.AddField(
            model_name='treeobs',
            name='obs',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='observation.PlotObs'),
        ),
        migrations.AddField(
            model_name='treeobs',
            name='rank',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Rank', verbose_name='Stellung'),
        ),
        migrations.AddField(
            model_name='treeobs',
            name='stem',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Stem', verbose_name='Schaftbruch'),
        ),
        migrations.AddField(
            model_name='treeobs',
            name='survey_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='observation.SurveyType'),
        ),
        migrations.AddField(
            model_name='treeobs',
            name='tree',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='observation.Tree'),
        ),
        migrations.AddField(
            model_name='treeobs',
            name='vita',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='observation.Vita', verbose_name='Lebenslauf'),
        ),
        migrations.AddField(
            model_name='tree',
            name='spec',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='observation.TreeSpecies'),
        ),
        migrations.AddField(
            model_name='regenobs',
            name='obs',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='observation.PlotObs'),
        ),
        migrations.AddField(
            model_name='regenobs',
            name='perc_an',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='regenobs_an', to='observation.RegenValue', verbose_name='Anwuchs (10-40cm)'),
        ),
        migrations.AddField(
            model_name='regenobs',
            name='perc_auf',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='regenobs_auf', to='observation.RegenValue', verbose_name='Aufwuchs (41-130cm)'),
        ),
        migrations.AddField(
            model_name='regenobs',
            name='spec',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='observation.TreeSpecies'),
        ),
        migrations.AddField(
            model_name='regenobs',
            name='verbiss',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.RegenVerbiss', verbose_name='Verbisskategorie'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='acidity',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Acidity', verbose_name='Azidität'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='crown_closure',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='observation.CrownClosure'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='forest_form',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.ForestForm', verbose_name='Waldform'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='forest_mixture',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='observation.ForestMixture'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='gap',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Gap', verbose_name='Blösse'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='geology',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Geology', verbose_name='Geologie'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='inv_team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='observation.Inventory'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='municipality',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='gemeinde.Gemeinde', verbose_name='Gemeinde'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='owner_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.OwnerType', verbose_name='Besitzertyp'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='plot',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='observation.Plot', verbose_name='Aufnahmepunkt (plot)'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='regen_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.RegenType', verbose_name='Verjüngungsart'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Region'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='relief',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Relief'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='sector',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.Sector', verbose_name='Sektor'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='soil_compaction',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.SoilCompaction', verbose_name='Bodenverdichtung'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_crown_closure',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='plotobs_stand', to='observation.CrownClosure', verbose_name='Schlussgrad'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_crown_closure2015',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.CrownClosure2015', verbose_name='Schlussgrad vor 2015'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_devel_stage',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.DevelStage', verbose_name='Entwicklungsstufe'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_devel_stage2015',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.DevelStage2015', verbose_name='Entwicklungsstufe vor 2015'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_forest_mixture',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='plotobs_stand', to='observation.ForestMixture', verbose_name='Mischungsgrad'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='stand_structure',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='observation.StandStructure'),
        ),
        migrations.AddField(
            model_name='adminregion',
            name='region_type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='observation.RegionType'),
        ),
        migrations.AlterUniqueTogether(
            name='treeobs',
            unique_together={('obs', 'tree')},
        ),
        migrations.AlterUniqueTogether(
            name='plotobs',
            unique_together={('plot', 'year')},
        ),
    ]
