from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0019_ownertype2_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotobs',
            name='owner',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='observation.Owner', verbose_name='Eigentümer'),
        ),
    ]
