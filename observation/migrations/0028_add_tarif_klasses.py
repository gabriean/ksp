from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0027_add_bv_eschentriebsterben_view'),
    ]

    operations = [
        migrations.AddField(
            model_name='phytosoc',
            name='tarif_klass20',
            field=models.CharField(blank=True, max_length=10),
        ),
        migrations.AddField(
            model_name='treespecies',
            name='tarif_klass20',
            field=models.CharField(blank=True, choices=[('fichte', 'Fichte (HAHBART1)'), ('tanne', 'Tanne (HAHBART2)'), ('buche', 'Buche (HAHBART7)'), ('ubrige_laub', 'übrige Laubhölzer (HAHBART12)'), ('ubrige_nad', 'übrige Nadelhölzer (HAHBART6)')], max_length=15),
        ),
    ]
