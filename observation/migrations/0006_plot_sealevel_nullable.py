from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0005_inventory_excluded_plots'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plot',
            name='sealevel',
            field=models.SmallIntegerField(null=True, verbose_name='Höhe ü. Meer'),
        ),
    ]
