from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0007_treespecies_is_historic'),
    ]

    operations = [
        migrations.AddConstraint(
            model_name='plotobs',
            constraint=models.CheckConstraint(check=models.Q(('forest_edgef__gte', 0), ('forest_edgef__lte', 1)), name='forest_edgef_betw_0_1'),
        ),
    ]
