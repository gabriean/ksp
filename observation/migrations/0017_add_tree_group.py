from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0016_adminregion_to_owner'),
    ]

    operations = [
        migrations.CreateModel(
            name='TreeGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=25, unique=True)),
            ],
            options={
                'db_table': 'tree_group',
            },
        ),
        migrations.AddField(
            model_name='treespecies',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=models.deletion.SET_NULL, to='observation.TreeGroup'),
        ),
    ]
