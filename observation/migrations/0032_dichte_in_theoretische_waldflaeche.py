from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0031_waldf_from_bestand_function'),
    ]

    operations = [
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW bv_grundf_vol_yx_pro_plotobs AS
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.id AS plot_obs_id,
    plot_obs.year,
    plot_obs.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0) AS "Volumen m3",
    COALESCE(subq."Grundflaeche m2", 0) AS "Grundflaeche m2",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(plot_obs.forest_edgef), 0) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3" * ksp_lokale_dichte(plot_obs.forest_edgef), 0) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche m2" * ksp_lokale_dichte(plot_obs.forest_edgef)::double precision, 0) AS "Grundflaeche pro ha",
    plot_obs.forest_edgef,
    inventory.period AS inv_period,
    inventory.municipality_id AS inv_municipality_id,
    plot_obs.density
   FROM ( SELECT tree_obs.obs_id,
            count(*) AS "Anzahl Probebaeume",
            sum(ksp_volume_et_bl(tree_obs.dbh)) AS "Volumen m3",
            sum(ksp_grundflaeche_bl(tree_obs.dbh)) AS "Grundflaeche m2"
           FROM tree_obs
             JOIN tree ON tree_obs.tree_id = tree.id
          WHERE (tree_obs.vita_id = 2 OR tree_obs.vita_id = 3) AND tree_obs.dbh > 11
          GROUP BY tree_obs.obs_id) subq
     RIGHT JOIN plot_obs ON subq.obs_id = plot_obs.id
     LEFT JOIN inventory ON plot_obs.inv_team_id = inventory.id;""",
            "DROP VIEW bv_grundf_vol_yx_pro_plotobs"
        ),

        migrations.RunSQL("""
CREATE OR REPLACE VIEW public.bv_web_homepage_holzproduktion
 AS
 SELECT row_number() OVER () AS id,
    d.gemeindename,
    b.inv_period,
    string_agg(DISTINCT b.year::text, '-'::text) AS year,
    sum(b."Anzahl Probebaeume")::integer AS "Anzahl Probebaeume",
    avg(b."Stammzahl pro ha")::integer AS "Stammzahl pro ha",
    stddev(b."Stammzahl pro ha")::double precision / sqrt(count(*)::double precision) / avg(b."Stammzahl pro ha")::double precision * 100::double precision AS "% Standardfehler",
    avg(b."Volumen pro ha")::numeric(5,1) AS "Volumen pro ha",
    stddev(b."Volumen pro ha")::double precision / sqrt(count(*)::double precision) / avg(b."Volumen pro ha")::double precision * 100::double precision AS "% Standardfehler2",
    avg(b."Grundflaeche pro ha")::numeric(5,1) AS "Grundflaeche pro ha",
    stddev(b."Grundflaeche pro ha") / sqrt(count(*)::double precision) / avg(b."Grundflaeche pro ha") * 100::double precision AS "% Standardfehler3",
    sum(ksp_lokale_dichte(b.forest_edgef) * 0.03 * b.density / 10000)::numeric(5,1) AS "theoretische Waldfläche ha",
    count(b.inv_period) AS "Anzahl Probepunkte",
    d.waldflaeche as waldflaeche_bestand
   FROM bv_grundf_vol_yx_pro_plotobs b
     FULL JOIN gemeinden_with_flaeche d ON b.inv_municipality_id = d.gid
  GROUP BY d.gemeindename, d.waldflaeche, b.inv_period
  ORDER BY d.gemeindename, b.inv_period""",
        ),
    ]
