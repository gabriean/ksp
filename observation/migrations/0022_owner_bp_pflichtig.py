from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0021_drop_plot_in_owner_materialized_view'),
    ]

    operations = [
        migrations.AddField(
            model_name='owner',
            name='bp_pflichtig',
            field=models.BooleanField(default=False, verbose_name='betriebsplanpflichtig'),
        ),
    ]
