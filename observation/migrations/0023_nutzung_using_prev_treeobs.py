from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0022_owner_bp_pflichtig'),
    ]

    operations = [
        migrations.RunSQL("""
CREATE OR REPLACE VIEW public.bv_grundf_vol_yx_pro_plotobs_nutzung AS 
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.id AS plot_obs_id,
    plot_obs.year,
    plot_obs.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0::bigint) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0::numeric) AS "Volumen m3",
    COALESCE(subq."Grundflaeche m2", 0::double precision) AS "Grundflaeche m2",
    COALESCE(subq."Anzahl Probebaeume", 0::bigint)::numeric * ksp_lokale_dichte(plot_obs.forest_edgef) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3", 0::numeric) * ksp_lokale_dichte(plot_obs.forest_edgef) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche m2", 0::double precision) * ksp_lokale_dichte(plot_obs.forest_edgef)::double precision AS "Grundflaeche pro ha",
    plot_obs.forest_edgef
   FROM ( SELECT tree_obs.obs_id,
            count(*) AS "Anzahl Probebaeume",
            sum(ksp_volume_et_bl(tobsb.dbh)) AS "Volumen m3",
            sum(ksp_grundflaeche_bl(tobsb.dbh)) AS "Grundflaeche m2",
            max(poa.year) - max(pob.year) AS year_diff
           FROM tree_obs
             JOIN tree ON tree_obs.tree_id = tree.id
             JOIN tree_obs tobsb ON tree_obs.tree_id = tobsb.tree_id
             LEFT JOIN plot_obs poa ON tree_obs.obs_id = poa.id
             LEFT JOIN inventory inv1 ON poa.inv_team_id = inv1.id
             LEFT JOIN plot_obs pob ON tobsb.obs_id = pob.id
             LEFT JOIN inventory inv0 ON pob.inv_team_id = inv0.id
         WHERE tree_obs.vita_id = 1 AND inv1.ordering = (inv0.ordering + 1)
          GROUP BY tree_obs.obs_id) subq
     FULL JOIN plot_obs ON subq.obs_id = plot_obs.id;"""
        ),
    ]
