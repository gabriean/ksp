from django.db import migrations


def migrate_owners(apps, schema_editor):
    Owner = apps.get_model("observation", "Owner")
    AdminRegion = apps.get_model("observation", "AdminRegion")
    for ar in AdminRegion.objects.filter(region_type__name='Eigentümer'):
        Owner.objects.create(nr=ar.nr, name=ar.name, geom=ar.geom)
        ar.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0015_owner'),
    ]

    operations = [migrations.RunPython(migrate_owners, migrations.RunPython.noop)]
