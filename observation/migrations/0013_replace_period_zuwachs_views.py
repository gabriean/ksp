from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('observation', '0012_view_secondobs_based_on_ordering'),
    ]

    operations = [
        migrations.RunSQL("""
 CREATE OR REPLACE VIEW bv_zuwachs_lebend AS
 SELECT tobsa.id,
    tobsa.tree_id,
    tobsa.obs_id,
    inv0.ordering AS invent0,
    inv1.ordering AS invent1,
    tobsb.dbh AS dbh0,
    tobsa.dbh AS dbh1,
    ksp_volume_et_bl(tobsb.dbh) AS vol0,
    ksp_volume_et_bl(tobsa.dbh) AS vol1,
    ksp_grundflaeche_bl(tobsb.dbh) AS grundf0,
    ksp_grundflaeche_bl(tobsa.dbh) AS grundf1,
    tobsa.dbh - tobsb.dbh AS dbh_diff,
    poa.year - pob.year AS year_diff,
    ksp_grundflaeche_bl(tobsa.dbh) - ksp_grundflaeche_bl(tobsb.dbh) AS grundf_diff,
    GREATEST(0::double precision, (ksp_volume_et_bl(tobsa.dbh) - ksp_volume_et_bl(tobsb.dbh))::double precision / NULLIF(poa.year - pob.year, 0)::double precision) AS growth_year
   FROM tree_obs tobsa
     JOIN tree_obs tobsb ON tobsa.tree_id = tobsb.tree_id
     LEFT JOIN plot_obs poa ON tobsa.obs_id = poa.id
     LEFT JOIN inventory inv1 ON poa.inv_team_id = inv1.id
     LEFT JOIN plot_obs pob ON tobsb.obs_id = pob.id
     LEFT JOIN inventory inv0 ON pob.inv_team_id = inv0.id
     LEFT JOIN lt_vita vitaa ON tobsa.vita_id = vitaa.id
     LEFT JOIN lt_vita vitab ON tobsb.vita_id = vitab.id
  WHERE vitaa.code::text = 'l'::text AND (vitab.code::text = ANY (ARRAY['e'::character varying::text, 'l'::character varying::text])) AND inv1.ordering = (inv0.ordering + 1);"""
        ),

        migrations.RunSQL("""
 CREATE OR REPLACE VIEW bv_zuwachs_einwuchs AS
 SELECT tobs.id,
    tobs.tree_id,
    tobs.obs_id,
    po_prev.ordering AS invent0,
    inventory.ordering AS invent1,
    tobs.dbh,
    tobs.dbh - 11 AS dbh_diff,
    po.year - po_prev.year AS year_diff,
    GREATEST(0::double precision, (ksp_volume_et_bl(tobs.dbh) - ksp_volume_et_bl(11::smallint))::double precision / (po.year - po_prev.year)::double precision / 2.0::double precision) AS growth_year
   FROM tree_obs tobs
     LEFT JOIN plot_obs po ON tobs.obs_id = po.id
     LEFT JOIN inventory ON po.inv_team_id = inventory.id
     LEFT JOIN plot ON po.plot_id = plot.id
     LEFT JOIN (
        SELECT po2.id, po2.plot_id, po2.year, inventory.ordering FROM plot_obs po2
        LEFT JOIN inventory on po2.inv_team_id=inventory.id
     ) po_prev ON plot.id = po_prev.plot_id AND inventory.ordering = (po_prev.ordering + 1)
     LEFT JOIN lt_vita vita ON tobs.vita_id = vita.id
  WHERE vita.code::text = 'e'::text;"""
        ),    ]
