import re

from django import forms
from django.contrib import admin, messages
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.forms import OSMWidget, PointField
from django.contrib.gis.gdal import CoordTransform, SpatialReference
from django.core import serializers
from django.http import HttpResponse, JsonResponse

from . import models


class MyOSMWidget(OSMWidget):
    """ Overriden OSMWidget to set local JS files (collected with django-remote-finder). """
    default_lon = 7.65
    default_lat = 47.47

    class Media:
        extend = False
        css = {
            'all': ('css/ol.css', 'gis/css/ol3.css')
        }
        js = (
            'js/ol3.js',
            'gis/js/OLMapWidget.js',
        )


@admin.register(models.Inventory)
class InventoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'inv_from', 'inv_to', 'period', 'ordering')
    list_filter = ('period',)
    search_fields = ('name',)
    ordering = ('name', 'inv_from')
    formfield_overrides = {
        gis_models.MultiPolygonField: {'widget': MyOSMWidget},
    }


class CoordField(PointField):
    widget = forms.TextInput

    def clean(self, value):
        # Accept decimal coords notation (47 35'38,1367 N 7 40'49,9824 E)
        m = re.match(r'(\d+)[°\s]+(\d+)[\'′\s]+([\d,\.]+)\s?[NS]' +
                     r'\s?(\d+)[°\s]+(\d+)[\'′\s]+([\d,\.]+)\s?[EW]',
                     value)
        if m:
            deg_lat, min_lat, sec_lat, deg_lon, min_lon, sec_lon = m.groups()
            lat = int(deg_lat) + int(min_lat)/60 + float(sec_lat.replace(',', '.'))/3600
            lon = int(deg_lon) + int(min_lon)/60 + float(sec_lon.replace(',', '.'))/3600
            return "SRID=4326;POINT(%(lon)s %(lat)s)" % {'lon': lon, 'lat': lat}
        return super().clean(value)


@admin.register(models.Plot)
class PlotAdmin(admin.ModelAdmin):
    list_display = ('nr', 'sealevel')
    search_fields = ('nr',)

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        if db_field.name in ('the_geom', 'point_exact'):
            return CoordField(widget=forms.TextInput(attrs={'size': 60}), **kwargs)
        return super().formfield_for_dbfield(db_field, request, **kwargs)


def export_to_json(modeladmin, request, queryset):
    def get_objects():
        for plotobs in queryset:
            yield plotobs.plot
            yield plotobs
            for treeobs in plotobs.treeobs_set.all():
                yield treeobs.tree
                yield treeobs
            for regenobs in plotobs.regenobs_set.all():
                yield regenobs

    response = HttpResponse(content_type="application/json")
    serializers.serialize("json", get_objects(), indent=2, use_natural_foreign_keys=True, stream=response)
    return response


def export_to_geojson(modeladmin, request, queryset):
    struct = {'type': "FeatureCollection", "features": []}
    trans_to_wgs84 = CoordTransform(SpatialReference(2056), SpatialReference(4326))
    plot_fields = ('nr', 'phytosoc', 'slope', 'exposition', 'sealevel')
    plotobs_fields = queryset[0].visible_fields(exclude_empty=False)
    regen_fields = ('perc', 'perc_an', 'perc_auf', 'verbiss', 'fegen')
    tree_fields = ('spec', 'nr', 'azimuth', 'distance')
    treeobs_fields = (
        'survey_type', 'layer', 'dbh', 'rank', 'vita', 'damage', 'damage_cause',
        'crown_length', 'crown_form', 'stem', 'stem_forked', 'stem_height', 'woodpecker',
        'ash_dieback', 'remarks',
    )

    def str_or_none(val):
        return val if val is None or val is True or val is False else str(val)

    def serialize(obj, fields):
        return {fname: str_or_none(getattr(obj, fname)) for fname in fields}

    for plotobs in queryset:
        pt = plotobs.plot.the_geom  #plotobs.plot.point_exact or plotobs.plot.the_geom
        pt.transform(trans_to_wgs84)
        props = serialize(plotobs.plot, plot_fields)
        props.update(serialize(plotobs, plotobs_fields))
        props['regenobs'] = {str(ro.spec): serialize(ro, regen_fields) for ro in plotobs.regenobs_set.all()}

        struct["features"].append({
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": pt.coords,
            },
            "properties": props,
        })
        for treeobs in plotobs.treeobs_set.all():
            pt = treeobs.tree.point
            pt.transform(trans_to_wgs84)
            props = serialize(treeobs.tree, tree_fields)
            props.update(serialize(treeobs, treeobs_fields))
            struct["features"].append({
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": pt.coords,
                },
                "properties": props,
            })
    return JsonResponse(struct)


class RegenObsInline(admin.TabularInline):
    model = models.RegenObs
    extra = 0


@admin.register(models.PlotObs)
class PlotObsAdmin(admin.ModelAdmin):
    list_display = ('plot', 'year', 'municipality')
    search_fields = ('plot__nr',)
    raw_id_fields = ('plot', 'owner')
    actions = [export_to_json, export_to_geojson]
    inlines = [RegenObsInline]
    fieldsets = (
        (None, {
            # Omitted: forest_mixture, crown_closure
            'fields': (
                'plot', 'year', 'inv_team', 'owner', 'owner_type', 'municipality', 'region',
                'sector', 'area', 'subsector', 'evaluation_unit', 'forest_clearing',
                'gap', 'stand', 'stand_devel_stage', 'stand_devel_stage2015',
                'stand_forest_mixture', 'stand_crown_closure', 'stand_crown_closure2015',
                'soil_compaction', 'forest_form', 'regen_type', 'stand_structure',
                'forest_edgef', 'gwl', 'relief', 'geology', 'acidity', 'remarks',
            )
        }),
        ('Unknown fields', {
            'classes': ('collapse',),
            'fields': (
                'iprobenr', 'iflae', 'itflae', 'ikalter', 'ik6', 'ik7', 'ik8', 'ik9',
                'izeitpkt', 'abt', 'ikflag', 'ifoa', 'irevf', 'idistr', 'iabt', 'istao',
                'ia', 'ib', 'ic', 'id_2', 'ie', 'ibgr9',
            )
        }),
    )


@admin.register(models.TreeSpecies)
class TreeSpeciesAdmin(admin.ModelAdmin):
    list_display = ('species', 'abbrev', 'group', 'tarif_klass20', 'is_tree', 'is_historic', 'is_special')
    ordering = ('species',)


@admin.register(models.Tree)
class TreeAdmin(admin.ModelAdmin):
    raw_id_fields = ('plot',)
    search_fields = ('plot__nr',)


@admin.register(models.Phytosoc)
class PhytosocAdmin(admin.ModelAdmin):
    search_fields = ('code', 'description')
    list_display = ('code', 'description', 'inc_class', 'ecol_grp', 'bwnatur', 'tarif_klass20')
    ordering = ('code',)


@admin.register(models.TreeObs)
class TreeObsAdmin(admin.ModelAdmin):
    search_fields = ('id', 'obs__plot__nr',)
    raw_id_fields = ('obs', 'tree')


@admin.register(models.RegenObs)
class RegenObsAdmin(admin.ModelAdmin):
    list_display = ('obs', 'spec', 'perc')
    raw_id_fields = ('obs',)


@admin.register(models.AdminRegion)
class AdminRegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'nr', 'region_type')
    list_filter = ('region_type',)
    ordering = ('name',)
    search_fields = ('name', 'nr')
    formfield_overrides = {
        gis_models.GeometryField: {'widget': MyOSMWidget},
    }

    def get_readonly_fields(self, request, obj=None):
        fields = super().get_readonly_fields(request, obj=obj)
        if obj is not None and obj.region_type.name != 'Freiperimeter':
            fields += ('geom',)
        return fields

    def save_model(self, request, *args, **kwargs):
        self.message_user(
            request,
            "Don't forget to update the plot_in_region materialized view (REFRESH "
            "MATERIALIZED VIEW plot_in_region;) so as request grouping fully works "
            "with the new data.",
            messages.WARNING
        )
        super().save_model(request, *args, **kwargs)


@admin.register(
    models.DevelStage, models.DevelStage2015, models.CrownClosure, models.CrownClosure2015,
    models.StandStructure,
)
class LTWithBwstru1Admin(admin.ModelAdmin):
    list_display = ('description', 'code', 'bwstru1_val')
    ordering = ('id',)


@admin.register(models.OwnerType2)
class OwnerType2Admin(admin.ModelAdmin):
    list_display = ['name', 'status']


@admin.register(models.Owner)
class OwnerAdmin(admin.ModelAdmin):
    list_display = ['name', 'nr', 'otype']


admin.site.register(models.OwnerType)
admin.site.register(models.TreeGroup)
admin.site.register(models.Region)
admin.site.register(models.Sector)
admin.site.register(models.Gap)
admin.site.register(models.SoilCompaction)
admin.site.register(models.ForestForm)
admin.site.register(models.RegenType)
admin.site.register(models.RegenValue)
admin.site.register(models.ForestMixture)
admin.site.register(models.Relief)
admin.site.register(models.Acidity)
admin.site.register(models.Geology)
admin.site.register(models.SurveyType)
admin.site.register(models.Layer)
admin.site.register(models.Rank)
admin.site.register(models.Vita)
admin.site.register(models.Damage)
admin.site.register(models.DamageCause)
admin.site.register(models.CrownForm)
admin.site.register(models.CrownLength)
admin.site.register(models.Stem)
admin.site.register(models.FoliageDensity)
admin.site.register(models.UpdatedValue)
admin.site.register(models.RegionType)
