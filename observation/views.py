import csv
import inspect
import json
import re
import tempfile
import urllib
from collections import Counter, namedtuple, OrderedDict
from datetime import date
from pathlib import Path
from zipfile import ZipFile

from django.apps import apps
from django.conf import settings
from django.contrib.gis.db.models.functions import Transform
from django.contrib.gis.geos import GEOSGeometry, Point
from django.db import connection, transaction
from django.db.models import (
    Case, CharField, Count, F, Model, Q, Value, When
)
from django.forms import ChoiceField
from django.http import (
    Http404, HttpResponse, HttpResponseForbidden, HttpResponseRedirect, JsonResponse,
)
from django.shortcuts import get_object_or_404, render
from django.template import TemplateDoesNotExist, loader
from django.urls import reverse, reverse_lazy
from django.utils.encoding import force_text
from django.utils.text import format_lazy
from django.views.generic import DetailView, FormView, View, TemplateView
from django.views.static import serve

from gemeinde.models import Gemeinde
from document.models import Document
# from afw.models import WaldBestandLaufend

from .forms import TreeEditForm, TreeReconcileForm
from .models import (
    AdminRegion, GefahrPotential, Inventory, Owner, Plot, PlotObs,
    TreeObs, WaldBestandKarte, db_views,
)
from .models.db_views import DBView, HomepageView
from .queries import VIEW_MAP

ViewTuple = namedtuple('ViewTuple', ['name', 'oid'])

inv_choice = ChoiceField(choices=(
    ('', "Alle Aufnahmen"), ('1', "Erste Aufnahme"), ('2', "Zweite Aufnahme"),
    ('3', "Dritte Aufnahme"),
))

BACKUPS_PATH = Path(settings.BASE_DIR) / 'backups'

descriptions = {
    'stammzahl': {
        'short': 'Summe aller Probebäume pro Flächeneinheit (ha).',
        'long': format_lazy(
            'Die Anzahl <a href="{docs_url}#probebaume">Probebäume</a> pro '
            '<a href="{docs_url}#probeflache-gultig">Kontrollstichproben-Punkt</a> werden  '
            'gezählt und mit der <a href="{func_url}">lokalen Dichte</a> gewichtet.',
            docs_url=reverse_lazy('docs'),
            func_url=reverse_lazy('func_def', args=['ksp_lokale_dichte'])
        )
    },
    'grundflache': {
        'short': 'Stammquerschnittfläche eines Probebaumes in 1.3 m Höhe (BHD-Messstelle).',
        'long': format_lazy(
            'Summe der Stammquerschnittflächen aller '
            '<a href="{docs_url}#probebaume">Probebäume</a> einer Waldfläche pro ha  '
            '[m<sup>2</sup>/ha]. Siehe <a href="{func_url}">Datenbankdefinition</a>.',
            docs_url=reverse_lazy('docs'),
            func_url=reverse_lazy('func_def', args=['ksp_grundflaeche_bl'])
        )
    },
    'volumen': {
        'short': 'Schaftholzvolumen in Rinde der Probebäume.',
        'long': format_lazy(
            'Das Baumvolumen eines <a href="{docs_url}#probebaume">Probebaums</a> wird '
            'mit dem <a href="{ein_url}">Einheitstarif BL</a> berechnet. Dieser '
            'Einheitstarif unterscheidet weder Laub- noch Nadelholz und ist für den '
            'ganzen Kanton einheitlich.',
            docs_url=reverse_lazy('docs'),
            ein_url=reverse_lazy('doc-specific', args=['einheitstarif'])
        )
    },
}


class AdminRegionGeoJson(View):
    def get(self, request, *args, **kwargs):
        region = get_object_or_404(AdminRegion, pk=request.GET.get('regionpk'))
        geojson = {
            "type": "Feature",
            "id": region.pk,
            "properties": {
                "name": region.name,
            },
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": region.geom.coords,
            }
        }
        return JsonResponse(geojson)


class GemeindenView(TemplateView):
    """
    Calculate the number of PlotObs per municipality and year.
    """
    template_name = 'gemeinden.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        # {'Aesch': {'obj': <Aesch Gemeinde>}, ... }
        gemeinden = OrderedDict([(gem.name, {'obj': gem})
                                 for gem in Gemeinde.objects.all().order_by(
                                    Case(When(kanton='BL', then=Value('')),
                                         When(kanton='BS', then=Value('')), default=F('kanton'),
                                         output_field=CharField()),
                                    'name')])
        figures = HomepageView.objects.all()
        inv_period = self.request.GET.get('aufn', None)
        if inv_period:
            figures = figures.filter(inv_period=int(inv_period))
        for fig in figures:
            gemeinden[fig.gemeinde].setdefault('years', []).append(fig)
        context.update({
            'gemeinden': gemeinden,
            'db_view': view_tuple(HomepageView._meta.db_table),
            'inv_choice': inv_choice.widget.render('inventory_filter', inv_period),
            'descriptions': descriptions,
        })
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('format') == 'csv':
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="basel-gemeinden.csv"'
            writer = csv.writer(response, delimiter=";")
            writer.writerow([
                'Gemeinde', 'Jahr', 'Anzahl Probepunkte', 'Anzahl Bäume', 'theoretische Waldfläche pro ha',
                'Stammzahl pro ha', 'Volumen [m3 pro ha]', 'Grundflaeche [m2 pro ha]',
            ])
            for name, infos in context['gemeinden'].items():
                if 'years' not in infos:
                    writer.writerow([data.gemeinde] + ['-'] * 7)
                    continue
                for data in infos['years']:
                    writer.writerow([
                        data.gemeinde, data.jahr, data.probepunkte, data.probebaum_abs,
                        data.waldflaeche, data.stammzahl_ha, data.volumen_ha, data.grundflaeche_ha,
                    ])
            return response
        return super(GemeindenView, self).render_to_response(context, **response_kwargs)


class GemeindeDetailView(TemplateView):
    template_name = 'gemeinde_detail.html'

    def get_context_data(self, **context):
        context = super(GemeindeDetailView, self).get_context_data(**context)
        gemeinde = get_object_or_404(Gemeinde, pk=self.kwargs['pk'])
        inventories = list(gemeinde.inventory_set.annotate(num_plots=Count('plotobs')).order_by('inv_from'))
        passed_inventories = [inv for inv in inventories if inv.inv_to < date.today() or inv.num_plots > 0]
        context.update({
            'gemeinde': gemeinde,
            'center': gemeinde.the_geom.centroid,
            'passed_inventories': passed_inventories,
            'current_inventories': [inv for inv in inventories if inv not in passed_inventories],
            'regions': AdminRegion.objects.all().order_by('region_type', 'name'),
        })
        return context


class MultiPlotJSONView(View):
    """Base class to serialize multiple points into a JSON response."""
    def get(self, request, *args, **kwargs):
        geojson = {"type": "FeatureCollection", "features": []}
        for pt in self.get_queryset():
            geojson["features"].append(self.handle_point(pt))
        return JsonResponse(geojson)

    def get_queryset(self):
        return NotImplementedError

    def handle_point(self, obj):
        return obj.as_geojson(geom_field='geom')


class GemeindePlotsView(MultiPlotJSONView):
    def get_queryset(self):
        self.gemeinde = get_object_or_404(Gemeinde, pk=self.kwargs['pk'])
        return Plot.objects.filter(the_geom__within=self.gemeinde.the_geom
                ).annotate(geom=Transform('the_geom', 4326))

    def handle_point(self, obj):
        feature = obj.as_geojson(geom_field='geom')
        for year in feature["properties"]["obsURLs"].keys():
            feature["properties"]["obsURLs"][year] += '?srid=4326'
        feature["properties"]["municipality_id"] = self.gemeinde.pk
        feature["properties"]["municipality_name"] = str(self.gemeinde)
        feature["properties"]["coords_2056"] = [
            int(round(c, 0)) for c in obj.the_geom.coords
        ]
        # Add infos from Bestandeskarte
        try:
            bestand = WaldBestandKarte.objects.select_related(
                'entwicklungstufe', 'mischungsgrad', 'schlussgrad'
            ).get(geom__contains=obj.the_geom)
        except (WaldBestandKarte.DoesNotExist, WaldBestandKarte.MultipleObjectsReturned):
            pass
        else:
            def to_list(val):
                return [val.pk, str(val)] if val is not None else []
            feature["properties"]["stand_devel_stage"] = to_list(
                bestand.entwicklungstufe)
            feature["properties"]["stand_forest_mixture"] = to_list(
                bestand.mischungsgrad)
            feature["properties"]["stand_crown_closure"] = to_list(
                bestand.schlussgrad)
        return feature


class InventoryView(DetailView):
    model = Inventory
    template_name = 'inventory_detail.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        geo_struct = {"type": "FeatureCollection", "features": []}
        for vals in self.object.plotobs_set.values('id', 'plot__the_geom'):
            geo_struct['features'].append({
                "type": "Feature",
                "geometry": eval(GEOSGeometry(vals['plot__the_geom']).json),
                "properties": {'id': vals['id']},
            })
        context.update({
            'center': self.object.municipality.the_geom.centroid,
            'plot_obs': geo_struct,
        })
        return context


class InventoryPlotObsView(MultiPlotJSONView):
    def get_queryset(self):
        inventory = get_object_or_404(Inventory, pk=self.kwargs['pk'])
        return inventory.plotobs_set.all()

    def handle_point(self, obj):
        return {
            "type": "Feature",
            "geometry": eval(obj.plot.the_geom.json),
            "properties": {"pk": obj.pk, "url": reverse('plotobs_detail', args=[obj.pk])},
        }


class InventoryPlotNoObsView(MultiPlotJSONView):
    """Plots inside inventory without any PlotObs for this inventory."""

    def get(self, request, *args, **kwargs):
        self.inventory = get_object_or_404(Inventory, pk=self.kwargs['pk'])
        self.excluded_ids = self.inventory.excluded_plots or []
        prev_inv = self.inventory.previous
        if prev_inv:
            self.plot_ids_from_previous_inv = list(prev_inv.plotobs_set.values_list('plot_id', flat=True))
        else:
            self.plot_ids_from_previous_inv = []
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return Plot.objects.filter(the_geom__intersects=self.inventory.geom
            ).annotate(has_obs=Count('plotobs', filter=Q(plotobs__inv_team=self.inventory))
            ).filter(has_obs=0)

    def handle_point(self, obj):
        return {
            "type": "Feature",
            "geometry": eval(obj.the_geom.json),
            "properties": {
                "pk": obj.pk,
                "excluded": obj.pk in self.excluded_ids,
                "in_previous": obj.pk in self.plot_ids_from_previous_inv,
                "url": reverse('plot_detail_embed', args=[self.inventory.pk, obj.pk]),
            },
        }


class InventoryExcludePlotsView(View):
    """
    plot ids included in POSTed ptIds will be added to inventory excluded plots
    (or removed if op = reinclude.
    """
    def post(self, request, *args, **kwargs):
        inventory = get_object_or_404(Inventory, pk=kwargs['pk'])
        point_ids = [int(pt) for pt in request.POST.getlist('ptIds[]')]
        op = request.POST.get('op')
        if op == 'reinclude':
            excluded = inventory.excluded_plots or []
            [excluded.remove(pt_id) for pt_id in point_ids]
            inventory.excluded_plots = excluded
        else:
            inventory.excluded_plots = (inventory.excluded_plots or []) + point_ids
        inventory.save()
        # Return the affected plots (for reinsertion in map)
        geojson = {"type": "FeatureCollection", "features": []}
        plots = Plot.objects.filter(pk__in=point_ids)
        for plot in plots:
            geojson["features"].append({
                "type": "Feature",
                "geometry": eval(plot.the_geom.json),
                "properties": {
                    "pk": plot.pk,
                    "excluded": op == 'exclude',
                    "url": reverse('plot_detail_embed', args=[inventory.pk, plot.pk]),
                },
            })
        return JsonResponse({'result': 'OK', 'features': geojson})


class InventoryAddPlotView(View):
    def post(self, request, *args, **kwargs):
        inventory = get_object_or_404(Inventory, pk=kwargs['pk'])
        longit = request.POST.get('longitude')
        latit = request.POST.get('latitude')
        number = int(longit[2:5] + latit[2:5])
        point = Point(int(longit), int(latit), srid=2056)
        plot = Plot.objects.create(nr=number, the_geom=point, checked=True)
        plot.set_elevation_from_swisstopo()
        geojson = {"type": "FeatureCollection", "features": [{
            "type": "Feature",
            "geometry": eval(plot.the_geom.json),
            "properties": {
                "pk": plot.pk,
                "excluded": False,
                "in_previous": False,
                "url": reverse('plot_detail_embed', args=[inventory.pk, plot.pk]),
            },
        }]}
        return JsonResponse({'result': 'OK', 'features': geojson})


class InventoryPlotsFromShape(View):
    def post(self, request, *args, **kwargs):
        inventory = get_object_or_404(Inventory, pk=kwargs['pk'])
        shapefile = request.FILES.get('shapefile')
        if not shapefile.name.lower().endswith('.zip'):
            return JsonResponse({'result': 'Error', 'error': "The file does not ends with .zip"})
        # unzip file and read shapefile input
        with tempfile.TemporaryDirectory() as tmpdirname:
            from django.contrib.gis.gdal import DataSource

            with ZipFile(shapefile, 'r') as zipObj:
                zipObj.extractall(tmpdirname)
            try:
                shp_path = list(Path(tmpdirname).glob('*.shp'))[0]
            except IndexError:
                return JsonResponse({'result': 'Error', 'error': "Unable to find a .shp file in the zip archive"})
            ds = DataSource(str(shp_path))  # str can be removed in Django 3.2
            layer = ds[0]
            new_plots = []
            existings = 0
            outside = 0
            with transaction.atomic():
                for feature in layer:
                    # Check if coords are inside inventory
                    inside = inventory.geom.contains(feature.geom.geos)
                    if not inside:
                        outside += 1
                        continue
                    # Check if coords exist
                    existing = Plot.objects.filter(the_geom=feature.geom.geos).exists()
                    if existing:
                        existings += 1
                        continue

                    plot = Plot(nr=feature.get('id') if 'id' in layer.fields else 0, the_geom=feature.geom.geos)
                    plot.save()
                    new_plots.append(plot)

        # Return features
        geojson = {"type": "FeatureCollection", "features": []}
        if new_plots:
            geojson["features"] = [{
                "type": "Feature",
                "geometry": eval(plot.the_geom.json),
                "properties": {
                    "pk": plot.pk,
                    "excluded": False,
                    "in_previous": False,
                    "url": reverse('plot_detail_embed', args=[inventory.pk, plot.pk]),
                },
            } for plot in new_plots]
        return JsonResponse({
            'result': 'OK', 'features': geojson,
            'new': len(new_plots), 'outside': outside, 'existing': existings,
        })


class InventorySavePolygonView(View):
    def post(self, request, *args, **kwargs):
        inventory = get_object_or_404(Inventory, pk=kwargs['pk'])
        inventory.geom = GEOSGeometry(request.POST.get('geom'))
        inventory.save()
        return JsonResponse({'result': 'OK'})


class PlotDetailView(DetailView):
    model = Plot
    template_name = 'plot_detail.html'

    def post(self, request, *args, **kwargs):
        """
        Allow manually marking a plot as checked. Could be removed once cleaning plots is finished.
        """
        if request.POST.get('checked') == 'true':
            plot = get_object_or_404(Plot, pk=kwargs['pk'])
            plot.checked = True
            plot.save()
            return HttpResponseRedirect(request.path)

    def get_context_data(self, **context):
        context = super(PlotDetailView, self).get_context_data(**context)
        gem = Gemeinde.objects.filter(the_geom__contains=self.object.the_geom).first()
        if gem:
            neighbours = Gemeinde.objects.filter(the_geom__bboverlaps=gem.the_geom).exclude(pk=gem.pk)
        else:
            # Not in a recognized municipality, find the nearest one(s)
            distance = self.object.the_geom.distance(self.object.plotobs_set.first().municipality.the_geom) + 1
            neighbours = Gemeinde.objects.filter(the_geom__dwithin=(self.object.the_geom, distance))
        observations = self.object.plotobs_set.prefetch_related('treeobs_set').order_by('year')

        # Compare trees between observations and mark trees found in all plotobs as `found`
        counter = Counter()
        trees = [obs.treeobs_set.select_related('tree', 'tree__spec', 'vita').all().order_by('tree__nr')
                 for obs in observations]
        for treelist in trees:
            counter.update([tr.tree_id for tr in treelist])
        for treelist in trees:
            for tobs in treelist:
                if counter[tobs.tree_id] == len(trees):
                    tobs.css = 'sync'
                elif counter[tobs.tree_id] > 1:
                    tobs.css = 'sync_partial'
                else:
                    tobs.css = 'nosync'
        observations = zip(observations, trees)
        context.update({
            'observations': observations,
            'real_municip': gem,
            'neighbours': neighbours,
        })
        return context


class PlotDetailEmbedView(DetailView):
    """Plot details shown when a master map wants embedded detail about a plot."""
    model = Plot
    pk_url_kwarg = 'plot_pk'
    template_name = 'plot_detail_embed.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        inventory = get_object_or_404(Inventory, pk=self.kwargs['pk'])
        context.update({
            'plotobs': self.object.plotobs_set.exclude(inv_team=inventory).order_by('inv_team__inv_from'),
            'excluded': self.object.pk in (inventory.excluded_plots or [])
        })
        return context


class PlotObsDataView(MultiPlotJSONView):
    def get_queryset(self):
        return PlotObs.objects.select_related('plot').filter(
            year=self.request.GET.get('year'), municipality_id=self.request.GET.get('gem')
        )

    def handle_point(self, obj):
        return {
            "type": "Feature",
            "geometry": eval(obj.plot.the_geom.json),
            "properties": {"pk": obj.pk, "url": reverse('plotobs_detail', args=[obj.pk])},
        }


class PlotObsDetailView(TemplateView):
    output = 'json'

    def get_template_names(self):
        return 'plotobs_detail_core.html' if self.request.is_ajax() else 'plotobs_detail.html'

    def get_context_data(self, **context):
        self.plotobs = get_object_or_404(PlotObs.objects.select_related('plot'), pk=self.kwargs['pk'])
        srid = self.request.GET.get('srid')
        center_geom = self.plotobs.plot.the_geom
        center_geom_exact = self.plotobs.plot.point_exact
        if srid is not None:
            center_geom.transform(srid)
            if center_geom_exact:
                center_geom_exact.transform(srid)
        return {
            'geojson': self.plotobs.as_geojson(dumped=False, srid=srid, verbose=self.output != 'json'),
            'center': center_geom,
            'center_exact': center_geom_exact,
            'plotobs': self.plotobs,
            'hidden_props': ('id', 'area', 'subsector', 'evaluation_unit', 'abt',
                             'type', 'Aufnahmepunkt (plot)', 'Aufnahmejahr'),
        }

    def render_to_response(self, context, **kwargs):
        if self.output == 'json':
            return JsonResponse(context['geojson'])
        else:
            plot = self.plotobs.plot
            props = context['geojson']['features'][0]["properties"].copy()
            props.update({
                'Phytosoziologie': plot.phytosoc, 'Neigung': plot.slope,
                'Exposition': plot.exposition, 'Höhe ü. Meer': plot.sealevel,
            })
            context.update({
                'properties': props,
                'geojson': json.dumps(context['geojson']),
                'gemeinde': self.plotobs.municipality,
                'treeobs': self.plotobs.treeobs_set.select_related('tree').all().order_by('tree__nr'),
                'siblings': self.plotobs.plot.plotobs_set.exclude(pk=self.plotobs.pk),
            })
            return super(PlotObsDetailView, self).render_to_response(context, **kwargs)

def plots_to_check(request):
    context = {
        'plots': Plot.objects.filter(checked=False).order_by('nr'),
    }
    return render(request, 'plots_to_check.html', context)

class TreeReconcileView(FormView):
    form_class = TreeReconcileForm
    template_name = 'tree_reconcile.html'

    def get_form_kwargs(self):
        kwargs = super(TreeReconcileView, self).get_form_kwargs()
        kwargs.update({
            'tree1': get_object_or_404(TreeObs, pk=self.request.GET.get('tree1')),
            'tree2': get_object_or_404(TreeObs, pk=self.request.GET.get('tree2')),
        })
        return kwargs

    def form_valid(self, form):
        form.save()
        return HttpResponse('OK')


class TreeEditView(FormView):
    form_class = TreeEditForm
    template_name = 'tree_edit.html'

    def get_form_kwargs(self):
        kwargs = super(TreeEditView, self).get_form_kwargs()
        kwargs.update({'tree_obs': get_object_or_404(TreeObs, pk=self.kwargs['pk'])})
        return kwargs

    def form_valid(self, form):
        form.save()
        return HttpResponse('OK')


class PermissiveEncoder(json.JSONEncoder):
    """
    Custom encoder to simply return 'null' for unserializable objects.
    """
    def default(self, obj):
        if isinstance(obj, type) and issubclass(obj, Model):
            return dict([(f.name, f.verbose_name) for f in obj._meta.fields])
        try:
            return super().default(obj)
        except TypeError:
            return None


class DataPageView(TemplateView):
    template_name = 'data_page.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context['perimeters'] = [
            {
                'items': Gemeinde.objects.all().order_by('name'),
                'title': "Gemeinden",
                'infos': ("Politische Gemeinden der beiden Basel sowie einzelne "
                          "Gemeinden aus den Nachbarkantonen, in den Kontrollstichproben "
                          "durch das Amt für Wald beider Basel erhoben wurden."),
                'infos_url': reverse('doc-specific', args=['gemeinden']),
                'short_name': "gem",
            },
            {
                'items': AdminRegion.objects.filter(region_type__name='Forstkreis').order_by('name'),
                'title': "Forstkreise",
                'infos': "Alle Forstkreise der beiden Basel können ausgewählt werden.",
                'infos_url': reverse('doc-specific', args=['forstkreise']),
                'short_name': "fkreis",
            },
            {
                'items': AdminRegion.objects.filter(region_type__name='Forstrevier').order_by('name'),
                'title': "Forstreviere",
                'infos': "Alle Forstreviere der beiden Basel können ausgewählt werden.",
                'infos_url': reverse('doc-specific', args=['forstreviere']),
                'short_name': "frevier",
            },
            {
                'items': AdminRegion.objects.filter(region_type__name='Jagdrevier').order_by('name'),
                'title': "Jagdreviere",
                'infos': "Alle Jagdreviere der beiden Basel können ausgewählt werden.",
                'infos_url': reverse('doc-specific', args=['jagdreviere']),
                'short_name': "jrevier",
            },
            {
                'items': AdminRegion.objects.filter(region_type__name='WEP').order_by('name'),
                'title': "WEP",
                'infos': "Alle WEPs der beiden Basel können ausgewählt werden.",
                'infos_url': reverse('doc-specific', args=['wep']),
                'short_name': "wep",
            },
            {
                'items': Owner.objects.all().order_by('name'),
                'title': "Eigentümer",
                'infos': "Wichtigste Waldeigentümer der beiden Basel können ausgewählt werden.",
                'infos_url': reverse('doc-specific', args=['eigentuemer']),
                'short_name': "eigentumer",
            },
            {
                'items': GefahrPotential.objects.filter(pk__gt=0).order_by('pk'),
                'title': "Schutzwald",
                'infos': "Schutzwald/Gefahrpotential",
                'infos_url': reverse('doc-specific', args=['schutzwald']),
                'short_name': "schutzwald",
            },
            {
                'items': AdminRegion.objects.filter(region_type__name='Freiperimeter').order_by('name'),
                'title': "Freiperimeter",
                'infos': "Website-Administrator definierte Regionen.",
                'infos_url': reverse('doc-specific', args=['freiperimeter']),
                'short_name': "freiperimeter",
            },
        ]
        for item in context['perimeters']:
            if item.get('infos_url'):
                item['infos'] += " Klicken Sie für weitere Informationen."
        context.update({
            'views': json.dumps({k: v.as_dict() for k, v in VIEW_MAP.items()}, cls=PermissiveEncoder),
            'descriptions': descriptions,
        })
        return context


class DataGridView(TemplateView):
    template_name = 'data_grid.html'

    def get(self, request, *args, **kwargs):
        bio_analyse = self.request.GET.get('bio')
        if not bio_analyse:
            return self.render_to_response({'error': "Keine ausgewählte Analyse"})
        try:
            self.query = VIEW_MAP[bio_analyse]
        except KeyError:
            return self.render_to_response({'error': "Unerkannte Analysename"})

        if request.GET.get('metadata'):
            # Only return query metadata
            gem_names = Gemeinde.objects.filter(pk__in=request.GET.getlist('gem')).values_list('name', flat=True).order_by('name')
            return JsonResponse({
                'gemeinden': list(gem_names),
                'map_color_fields': self.query.get_map_color_fields(),
            }, safe=False)

        context = self.get_context_data(**kwargs)
        if request.GET.get('format') == 'json' and not 'error' in context:
            # JSON data for the map view
            return JsonResponse(as_geojson(context['field_names'], context['query'], context['gemeinden']))
        else:
            return self.render_to_response(context)

    def get_perimeter(self):
        """Get perimeter choices from request"""
        def params_to_int(key):
            return [int(v) for v in self.request.GET.getlist(key)]

        return {
            'gems': params_to_int('gem'),
            'regions': (params_to_int('fkreis') + params_to_int('frevier') +
                        params_to_int('jrevier') + params_to_int('wep') +
                        params_to_int('freiperimeter')),
            'owners': params_to_int('eigentumer'),
            'schutzwald': params_to_int('schutzwald'),
        }

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        # Compute data after request.GET
        if self.request.GET.get('no_aggr') == 'none':
            aggrs = []
        else:
            aggrs = (
                self.request.GET.getlist('yaggr') +
                self.request.GET.getlist('paggr') +
                self.request.GET.getlist('aggr')
            )
        stddev = bool(self.request.GET.get('stddev'))

        try:
            context.update(self.query.build_query(
                self.get_perimeter(), aggrs, stddev,
                as_json=self.request.GET.get('format') == 'json'
            ))
            if not aggrs:
                context['total'] = context['query'].count()
        except Exception as exc:
            if settings.DEBUG:
                raise
            else:
                context['error'] = "Leider ist ein Fehler aufgetreten. (%s)" % force_text(exc)
        else:
            context['db_view'] = view_tuple(context['query'].model._meta.db_table)
        return context

    def render_to_response(self, context, **response_kwargs):
        if self.request.GET.get('format') == 'csv' and 'error' not in context:
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="ksp-data-export.csv"'
            writer = csv.writer(response, delimiter=";")
            writer.writerow(context['field_names'])
            for line in context['query']:
                writer.writerow([to_csv(val) for val in line])
            # Print query parameters  at bottom of data
            writer.writerow([])
            writer.writerow(['Analyse:', self.query.descr])
            writer.writerow(context['perimeter_feedback'])
            def trans(val):
                return {
                    'year': 'Jahr', 'municipality': 'Gemeinde', 'inv_period': 'Aufnahmeperiode',
                    'stand_devel_stage': 'Entwicklungsstufe', 'stand_forest_mixture': 'Mischungsgrad',
                    'stand_crown_closure': 'Schlussgrad', 'phyto_code': 'Phytosoziologie',
                    'inc_class': 'Ertragsklasse', 'ecol_grp': 'Ökologische Gruppe',
                }.get(val, val).capitalize()
            writer.writerow(['Gruppierung:', ", ".join(trans(term) for term in context['aggrs'])])
            if context['latest_invs']:
                writer.writerow(['Nur letztes Inventar'])
            if context['stddev']:
                writer.writerow(['Mit Standardfehler'])
            return response
        return super().render_to_response(context, **response_kwargs)


class ViewList(TemplateView):
    template_name = "view_list.html"

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        # Get views from introspecting db_views
        db_view_list = []
        for name, obj in inspect.getmembers(db_views):
            if inspect.isclass(obj) and hasattr(obj, '_meta') and not obj._meta.managed:
                db_view_list.append(DBView(name=obj._meta.db_table))
        for db_view in db_view_list[:]:
            db_view_list.extend(db_view.sub_views(recurse=True, only_views=True))
        context['dbviews'] = sorted(set(db_view_list))
        return context


class ViewDefinition(TemplateView):
    template_name = "view_def.html"

    def get(self, request, *args, **kwargs):
        self.db_view = DBView(oid=int(self.kwargs['oid']))
        if not self.db_view.is_view:
            # Try as a table object
            return HttpResponseRedirect(reverse('table_def', args=[self.kwargs['oid']]))
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        # Find other views/tables referenced in the definition
        definition = self.db_view.definition
        for db_view in self.db_view.sub_views():
            definition = re.sub(
                r'(FROM|JOIN) %s ' % db_view.name,
                r'\1 <a href="%s">%s</a> ' % (reverse('view_def', args=[db_view.oid]), db_view.name),
                definition
            )
        func_matches = re.findall(r'(ksp_[a-z_]*)\(', definition)
        for func_name in set(func_matches):
            definition = definition.replace(
                func_name, '<a href="%s">%s</a>' % (
                    reverse('func_def', args=[func_name]), func_name
                )
            )
        context.update({
            'obj_type': "View",
            'name': self.db_view.name,
            'definition': definition,
            'comment': self.db_view.comment,
        })
        return context


class TableMixin:
    @property
    def lookup_model_whitelist(self):
        other_lookups = [
            'adminregion', 'phytosoc', 'crownclosure', 'develstage',
            'forestmixture', 'regiontype', 'relief',
        ]
        return [
            mod for mod in apps.get_app_config('observation').get_models()
            if mod._meta.db_table.startswith('lt_') or mod._meta.db_table in other_lookups
        ]

    @property
    def main_model_whitelist(self):
        main_table_names = [
            'inventory', 'plot', 'plot_obs', 'regen_obs', 'tree_obs', 'tree',
            'tree_spec', 'gemeindegrenzen_bsbl', 'waldbestandkarte',
        ]
        return [
            mod for mod in apps.get_app_config('observation').get_models()
            if mod._meta.db_table in main_table_names
        ]

    def decorate_tabledef(self, table):
        '''Link FKs to their tables'''
        definition = table.definition
        with connection.cursor() as cursor:
            for field, ext_table, ext_field in connection.introspection.get_key_columns(cursor, table.name):
                definition = definition.replace(
                    ' %s ' % field,
                    ' <a href="%s">%s</a> ' % (reverse('table_def', args=[ext_table]), field)
                )
        return definition


class TableListView(TableMixin, TemplateView):
    template_name = "table_list.html"

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        db_tables = [
            DBView(name=model._meta.db_table)
            for model in self.main_model_whitelist
        ]
        lt_tables = [
            DBView(name=model._meta.db_table)
            for model in self.lookup_model_whitelist
        ]
        context.update({
            'main_tables': sorted(set(db_tables)),
            'lookup_tables': sorted(set(lt_tables)),
        })
        return context


class TableDefinitionView(TableMixin, TemplateView):
    template_name = "view_def.html"

    def get(self, request, *args, **kwargs):
        try:
            oid = int(self.kwargs['oid_or_name'])
            self.table = DBView(oid=oid)
        except ValueError:
            self.table = DBView(name=self.kwargs['oid_or_name'])
        if self.table.name is None:
            raise Http404
        # If this table is allowed to display its values, redirect to the view
        # showing tables values.
        lookup_model = next(
            (mod for mod in self.lookup_model_whitelist if mod._meta.db_table == self.table.name),
            None
        )
        if lookup_model:
            return HttpResponseRedirect(reverse('data_model', args=[lookup_model._meta.model_name]))
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        context.update({
            'obj_type': "Table",
            'name': self.table.name,
            'comment': self.table.comment,
            'definition': self.decorate_tabledef(self.table),
        })
        return context


class ModelView(TableMixin, TemplateView):
    """
    View to display the content of a database table/view from a defined model
    """
    template_name = "model_data.html"

    def get_table_content(self, model_name):
        model = apps.get_model('observation', model_name)
        fields = [
            f for f in model._meta.get_fields()
            if not f.one_to_many and f.name not in ('id', 'geom')
        ]
        return {
            'field_names': [f.verbose_name for f in fields],
            'data': model.objects.all().values_list(*[f.name for f in fields]),
        }

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        if self.kwargs['model'].lower() not in [
                mod._meta.model_name for mod in self.lookup_model_whitelist]:
            raise Http404
        model = apps.get_model('observation', self.kwargs['model'])
        table = DBView(name=model._meta.db_table)
        context.update({
            'title': "%s (Tabelle '%s')" % (model._meta.verbose_name, table.name),
            'definition': self.decorate_tabledef(table),
            'comment': table.comment,
        })
        context.update(self.get_table_content(self.kwargs['model']))
        return context


class FunctionDefinitionView(TemplateView):
    template_name = "view_def.html"

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT pg_get_functiondef(oid) FROM pg_proc WHERE proname = %s",
                [self.kwargs['func_name']]
            )
            try:
                func_def = cursor.fetchone()[0]
            except TypeError:
                raise Http404
            cursor.execute(
                "SELECT obj_description(%s, 'pg_proc')",
                [func_oid(self.kwargs['func_name'])]
            )
            comment = cursor.fetchone()[0]

        context.update({
            'obj_type': "Function",
            'name': self.kwargs['func_name'],
            'definition': func_def,
            'comment': comment,
        })
        return context


class DocumentationView(TemplateView):
    template_name = "docs.html"

    def get_context_data(self, **context):
        context = super(DocumentationView, self).get_context_data(**context)
        context['docs'] = Document.objects.filter(published=True).order_by('category', '-weight')
        if self.request.user.is_staff:
            context['backups'] = sorted(BACKUPS_PATH.glob('*.pgdump'), reverse=True)
        return context


class DocSpecificView(TemplateView):
    """
    View to load specific documentation snippets, generally loaded in jQuery UI
    modal dialogs (therefore not inheriting from base.html).
    """
    def get_template_names(self):
        template_name = 'docs/%s.html' % self.kwargs['slug']
        try:
            loader.get_template(template_name)
        except TemplateDoesNotExist:
            raise Http404
        return [template_name]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['descriptions'] = descriptions
        return context


def waldbestand(request, inventory_pk):
    """Replacement (temp?) of afw.views.waldbestand."""
    inventory = get_object_or_404(Inventory, pk=inventory_pk)
    query = WaldBestandKarte.objects.filter(geom__intersects=inventory.geom
        ).annotate(geom_wgs84=Transform('geom', 4326)
        ).select_related('entwicklungstufe', 'mischungsgrad', 'schlussgrad')
    # Transform to GeoJSON
    geojson = {"type": "FeatureCollection", "features": []}
    for bestand in query:
        geojson["features"].append({
            "type": "Feature",
            "id": bestand.pk,
            "properties": {
                "id": bestand.pk,
                "popupContent": "Entwicklungstufe: %s<br>Mischungsgrad: %s<br>Schlussgrad: %s" % (
                    bestand.entwicklungstufe, bestand.mischungsgrad, bestand.schlussgrad)
            },
            "geometry": eval(bestand.geom_wgs84.json),
        })
    return JsonResponse(geojson)


VIEWS = {
    'anzahl_pro_gemein': {'name': "Anzahl Stichproben pro Gemeinde und Aufnahmejahr"},
    #'stammzahl_pro_gemein':
    'einwuchs_pro_gemein': {
        'name': "Einwuch pro Gemeinde (sv/ha)",
        'graph': ((1, "Einwuchs"), (2, "% Standardfehler"))},
    'biolfi1m_pro_gemein': {
        'name': "BIOLFI1M_pro Gemeinde",
        'graph': ((1, "Biotowert"), (3, "Wert der Schichtung"))},
    'biolfi1m_braendli_pro_gemein': {
        'name': "BIOLFI1M_pro Gemeinde_Klassen nach LFI3_lit_braendli",
        'graph': ((1, "high"), (2, "tends to be high"), (3, "tends to be low"), (4, "low"))},
}

def chart(request, view_name):
    try:
        from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
        from matplotlib.figure import Figure
    except ImportError:
        return HttpResponse()

    view_data = select_from_view(request, view_name)
    cols = VIEWS[view_name]['graph']
    xlabels = [str(Gemeinde.from_code(d[0])) for d in view_data]

    fig = Figure(facecolor='white')
    ax = fig.add_subplot(1, 1, 1)
    # Draw data axes
    for (col_index, col_name) in VIEWS[view_name]['graph']:
        data = [d[int(col_index)] for d in view_data]
        ax.plot(range(0, len(data)), data, 'o-', label=col_name)
        #ax.bar(range(0, len(data)), data, 0.35, label=col_name)

    ax.set_xticks(range(0, len(xlabels)))
    ax.set_xticklabels(xlabels, rotation=30, horizontalalignment='right')
    ax.grid(True)
    leg = ax.legend(loc='best', fancybox=True)
    leg.get_frame().set_alpha(0.5)
    fig.subplots_adjust(bottom=0.18, left=0.15)

    canvas = FigureCanvas(fig)
    response = HttpResponse(content_type='image/png')
    canvas.print_png(response)
    return response


def serve_backup(request, filename):
    if not request.user.is_staff:
        return HttpResponseForbidden("Unauthorized access")
    return serve(request, filename, BACKUPS_PATH)


# Utilities
def select_from_view(request, view_name):
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM "%s"' % VIEWS[view_name]['name'], [])
    return filter_by_gemein(request, cursor.fetchall())


def view_tuple(relname):
    """Return (view_name, view_oid) tuple"""
    cursor = connection.cursor()
    cursor.execute("select oid from pg_class WHERE relname=%s", [relname])
    try:
        oid = cursor.fetchone()[0]
    except TypeError:
        oid = None
    return ViewTuple(relname, oid)


def func_oid(func_name):
    with connection.cursor() as cursor:
        cursor.execute("SELECT oid FROM pg_catalog.pg_proc WHERE proname=%s", [func_name])
        try:
            oid = cursor.fetchone()[0]
        except TypeError:
            oid = None
    return oid


def filter_by_gemein(request, result, gem_field=0):
    selected_gems = request.COOKIES.get('sel_gems')

    def filter_gem(res):
        if isinstance(gem_field, int):
            return res[gem_field] in selected_gems
        else:
            return res.get(gem_field) in selected_gems
    if selected_gems:
        selected_gems = urllib.unquote(selected_gems).split(',')
        return filter(filter_gem, result)
    return result


def as_geojson(field_names, query, gemeinden):
    """
    Output a queryset `query` as GeoJSON, including geometries for
    municipality `gemeinden`.
    """
    struct = {"type": "FeatureCollection", "features": []}
    for line in query:
        struct['features'].append({
            "type": "Feature",
            "geometry": eval(GEOSGeometry(line[-1]).json),
            "properties": dict(zip(field_names[:-1], line[:-1])),
        })
    for gem in gemeinden:
        struct['features'].append({
            "type": "Feature",
            "geometry": eval(gem.the_geom.json),
            "properties": {'name': gem.name},
        })
    return struct


def to_csv(value):
    if value is None:
        return ''
    return value
