import logging
from collections import namedtuple, OrderedDict

from django.contrib.gis.db.models import Union
from django.core.exceptions import FieldDoesNotExist
from django.db.models import Avg, Count, F, FloatField, Sum, Value
from django.db.models.functions import Cast, Coalesce

from gemeinde.models import Gemeinde

from .functions import StdErrRel
from .models import AdminRegion, GefahrPotential, Owner, Phytosoc, PlotObs
# database views:
from .models import (
    BWARTEN, BWNATURN, BWSTRU1M, Biotopwert, Einwuchs,
    EinwuchsProSpec, EinwuchsProSpecGroup,
    HolzProduktion, HolzProduktionProSpec, HolzProduktionProSpecGroup,
    Nutzung, NutzungProSpec, NutzungProSpecGroup,
    Totholz, TotholzProSpec, TotholzProSpecGroup, Zuwachs, BodenSchaden,
    Giganten, Eschentriebsterben,
)
from .models.db_views import LatestInventories

logger = logging.getLogger(__name__)

Field = namedtuple('Field', ['name', 'accessor', 'fkey_target', 'vname'])


class QueryData:
    """
    'model': View class, 'descr': 'Verbose description',
    'model_by_spec': alternate model to use when grouping by species,
    'model_by_spec_group': alternate model to use when grouping by species group,
    'plot_obs': 'prefix to plot_obs.id',
    'annot_map' (optional): when an aggregation occurs, fields and corresponding annotation to use,
    """
    order_by = 'id'  # field on which to order
    plot_prefix = 'plot'
    annot_map = {}
    # Mapping to get the 'interesting' field on a foreign key join
    fkey_map = {
        'stand_devel_stage': 'description',
        'stand_crown_closure': 'description',
        'stand_forest_mixture': 'description',
        'stand_structure': 'description',
        'municipality': 'name',
        'gemeinde': 'name',
        'plot': 'nr',
        'otype': 'name',
        'spec': 'species',
        'group': 'name',
        'vita': 'code',
        'ash_dieback': 'description',
    }

    def __init__(self, **kwargs):
        self.map_color_fields = []
        for key, val in kwargs.items():
            setattr(self, key, val)

    def as_dict(self):
        return {key: getattr(self, key, '') for key in [
            'model', 'descr', 'plot_obs', 'plot_prefix', 'annot_map',
            'groupables', 'map_color_fields', 'order_by',
        ]}

    def get_fkey_target(self, fname):
        if fname in self.fkey_map:
            return '__%s' % self.fkey_map[fname]
        return ''

    def get_map_color_fields(self):
        return [
            (f.name, f.verbose_name) for f in self.model._meta.fields
            if f.name in self.map_color_fields
        ]

    def build_query(self, perimeter, aggrs, stddev=False, as_json=False):
        coalesce_with_0 = False
        view_model = self.model
        plot_obs_prefix = self.plot_obs
        self.fields = OrderedDict([
            (f.name, Field(f.name, f.name, self.get_fkey_target(f.name), f.verbose_name))
            for f in self.model._meta.fields
        ])
        if as_json:
            self.fields['the_geom'] = Field('the_geom', '%splot__the_geom' % self.plot_obs, '', 'Geometry')

        if aggrs:
            if 'spec' in aggrs and hasattr(self, 'model_by_spec'):
                view_model = self.model_by_spec
                specf = view_model._meta.get_field('spec')
                self.fields[specf.name] = Field(
                    specf.name, specf.name, self.get_fkey_target(specf.name), specf.verbose_name
                )
                coalesce_with_0 = True
            elif 'group' in aggrs and hasattr(self, 'model_by_spec_group'):
                view_model = self.model_by_spec_group
                groupf = view_model._meta.get_field('group')
                self.fields[groupf.name] = Field(
                    groupf.name, groupf.name, self.get_fkey_target(groupf.name), groupf.verbose_name
                )
                coalesce_with_0 = True

            if 'otype' in aggrs:
                self.fields['otype'] = Field(
                    'otype', '%sowner__otype' % plot_obs_prefix, self.get_fkey_target('otype'), 'EigentümerTyp'
                )
            if 'ostatus' in aggrs:
                self.fields['ostatus'] = Field(
                    'ostatus', '%sowner__otype__status' % plot_obs_prefix, '', 'Eigentümer Status'
                )
            # Adding Phytosoc field metadata when used in groupments
            if 'phyto_code' in aggrs:
                self.fields['phyto_code'] = Field(
                    'phyto_code', '%s__phytosoc__code' % self.plot_prefix, '', 'Phytosoziologie'
                )
            if 'inc_class' in aggrs:
                self.fields['inc_class'] = Field(
                    'inc_class', '%s__phytosoc__inc_class' % self.plot_prefix, '',
                    Phytosoc._meta.get_field('inc_class').verbose_name
                )
            if 'ecol_grp' in aggrs:
                self.fields['ecol_grp'] = Field(
                    'ecol_grp', '%s__phytosoc__ecol_grp' % self.plot_prefix, '',
                    Phytosoc._meta.get_field('ecol_grp').verbose_name
                )

        # Limit the query with choices from the "Perimeter" section
        if perimeter['gems']:
            query = view_model.objects.filter(**{'%smunicipality__pk__in' % plot_obs_prefix: perimeter['gems']})
            gem_qs = Gemeinde.objects.filter(pk__in=perimeter['gems']).order_by('name')
            perim_feedback = ("Gemeinden", ", ".join([gem.name for gem in gem_qs]))

        elif perimeter['regions']:
            query = view_model.objects.filter(
                **{'%s__the_geom__within' % self.plot_prefix:
                   AdminRegion.objects.filter(pk__in=perimeter['regions']).aggregate(geom=Union('geom'))['geom']}
            )
            reg_qs = AdminRegion.objects.filter(pk__in=perimeter['regions']).order_by('name')
            perim_feedback = ("Perimeter", ", ".join([reg.name for reg in reg_qs]))

        elif perimeter['owners']:
            query = view_model.objects.filter(
                **{'%sowner__pk__in' % plot_obs_prefix: perimeter['owners']}
            )
            owner_qs = Owner.objects.filter(pk__in=perimeter['owners']).order_by('name')
            perim_feedback = ("Eigentümer", ", ".join([o.name for o in owner_qs]))

        elif perimeter['schutzwald']:
            query = view_model.objects.filter(**{
                '%s__the_geom__within' % self.plot_prefix:
                    AdminRegion.objects.filter(
                        schutzwald__haupt_gef_pot__pk__in=perimeter['schutzwald']
                    ).aggregate(geom=Union('geom'))['geom']
            })
            gef_qs = GefahrPotential.objects.filter(pk__in=perimeter['schutzwald']).values('name')
            perim_feedback = ("Schutzwald", ", ".join([gef['name'] for gef in gef_qs]))

        else:
            raise ValueError("No selected perimeter")

        # If radio "latest inventories"
        latest_invs = False
        if 'letztes_inv' in aggrs:
            latest_invs = True
            aggrs.remove('letztes_inv')
        if latest_invs:
            latest_inv_ids = LatestInventories.objects.values_list('pk', flat=True)
            query = query.filter(
                 **{'%sinv_team_id__in' % plot_obs_prefix: latest_inv_ids}
            )

        field_names = [f.vname for f in self.fields.values()]  # Extract verbose names

        if aggrs:
            if 'year' not in aggrs and 'inv_period' not in aggrs and not latest_invs:
                # Always add year, as grouping and mixing years makes no sense
                aggrs.append('year')

            # aggr_crits will be passed to values_list()
            query, aggr_crits, field_names = self.build_aggregations(query, aggrs)

            # Only get grouped-by fields + fields with an available aggregation
            all_aggr_fields = {}
            all_aggr_fields.update(self.annot_map)
            annot_list = []
            for f in self.fields.values():
                # Last condition is a special case: when aggregating by bioklass,
                # no need to display the biolfi1m value.
                if (f.name not in all_aggr_fields or f.name in aggrs
                        or (f.name == 'biolfi1m' and 'bioklass' in aggrs)):
                    continue
                annot_alias = '%s__%s' % (f.name, all_aggr_fields[f.name].name.lower())
                if coalesce_with_0:
                    annot_list.append(all_aggr_fields[f.name](Coalesce(f.accessor, Value(0))))
                    # mandatory default_alias
                    annot_list[-1].source_expressions[0].name = annot_alias
                else:
                    annot_list.append(all_aggr_fields[f.name](f.accessor))
                field_names.append(self.fields[f.name].vname)
                if stddev and getattr(all_aggr_fields[f.name], 'name', '') in {'Avg', 'Sum'}:
                    annot_list.append(StdErrRel(f.accessor))
                    field_names.append("Sf [%]")

            annots, annot_names = self.add_final_annots()
            annot_list.extend(annots)
            field_names.extend(annot_names)
            # If we used values() instead, we may have less problem with ordering, ordering would
            # be handled later in the template based on a field mapping
            query = query.values_list(*aggr_crits).annotate(*annot_list)
            # Annotations that must come after the first annotate call.
            annots2, annot_names2 = self.add_final_annots2()
            if annots2:
                field_names.extend(annot_names2)
                query = query.annotate(*annots2)
            query = query.order_by(*aggr_crits)

        else:
            field_list = [f.accessor + f.fkey_target for f in self.fields.values()]
            query = query.values_list(*field_list).order_by(self.order_by)
        logger.debug(str(query.query))
        return {
            'query': query, 'field_names': field_names, 'ordering': self.get_ordering(field_names),
            'aggrs': aggrs, 'stddev': stddev, 'latest_invs': latest_invs,
            'perimeter_feedback': perim_feedback,
        }

    def build_aggregations(self, query, aggrs):
        aggr_crits = []
        field_names = []
        for fname in aggrs:
            # Privilege field join on plot_obs
            try:
                field = PlotObs._meta.get_field(fname)
                if field.auto_created and not field.concrete:
                    raise FieldDoesNotExist
                aggr_crits.append('%s%s%s' % (self.plot_obs, fname, self.get_fkey_target(fname)))
                field_names.append('*%s' % getattr(field, 'verbose_name', field.name))
            except FieldDoesNotExist:
                if fname in self.fields:
                    # Hopefully the field is in self.fields
                    aggr_crits.append(self.fields[fname].accessor + self.fields[fname].fkey_target)
                    field_names.append('*%s' % self.fields[fname].vname)
                elif fname == 'inv_period':
                    aggr_crits.append('%sinv_team__period' % self.plot_obs)
                    field_names.append('*Aufnahmeperiode')
                elif fname in {'forstkreis', 'forstrevier', 'jagdrevier', 'WEP', 'freiperimeter'}:
                    aggr_crits.append('%s__plotinregionview__region_name' % self.plot_prefix)
                    fname_cap = fname[0].upper() + fname[1:]
                    field_names.append('*%s' % fname_cap)
                    query = query.filter(
                        **{'%s__plotinregionview__region_type' % self.plot_prefix: fname_cap}
                    )
                elif fname == 'eigentümer':
                    aggr_crits.append('%sowner__name' % self.plot_obs)
                    field_names.append('*Eigentümer')
                elif fname == 'schutzwald':
                    aggr_crits.append(
                        '%s__plotinregionview__adminregion__schutzwald__haupt_gef_pot__name' % self.plot_prefix
                    )
                    field_names.append('*Schutzwaldgefahr')
                    query = query.filter(
                        **{'%s__plotinregionview__region_type' % self.plot_prefix: "Schutzwald"}
                    )
                else:
                    raise
        return query, aggr_crits, field_names

    def add_final_annots(self):
        return (
            [Count(self.plot_obs.rstrip('_'), distinct=True)],  # count on id/obs_id
            ['Anzahl KSP']
        )

    def add_final_annots2(self):
        return ([], [])

    def get_ordering(self, field_names):
        """Return a list of (index, [optional classes]) tuples."""
        return [
            (idx, ['sf'] if name == 'Sf [%]' else (['aggr'] if name.startswith('*') else []))
            for idx, name in enumerate(field_names)
        ]


class ZuwachsQuery(QueryData):
    model = Zuwachs
    descr = 'Zuwachs'
    plot_obs = 'id__'
    map_color_fields = []
    annot_map = {'leb_growth_ha': Avg, 'ein_growth_ha': Avg, 'mor_growth_ha': Avg, 'growth_ha_total': Avg}
    groupables = ['otype', 'ostatus']

    def get_ordering(self, field_names):
        """Move Totaler Zuwachs first, minimize other columns."""
        indices = super().get_ordering(field_names)
        tot_idx = field_names.index('Totaler Zuwachs/ha/Jahr')
        leb_idx = field_names.index('Lebender Zuwachs/ha/Jahr')
        indices.insert(leb_idx, indices.pop(tot_idx))
        moved = 1
        if field_names[indices[tot_idx][0]] == 'Sf [%]':
            indices.insert(leb_idx + 1, indices.pop(tot_idx + 1))
            moved += 1
        return [
            (tpl[0], tpl[1] if i < (leb_idx + moved) else tpl[1] + ['minim'])
            for i, tpl in enumerate(indices)
        ]


class BodenschadenQuery(QueryData):
    model = BodenSchaden
    descr = 'Bodenschäden'
    plot_obs = 'id__'
    map_color_fields = []
    annot_map = {'code0': Sum, 'code1': Sum, 'code2': Sum}
    groupables = ['spec', 'specgroup', 'otype', 'ostatus']

    def add_final_annots2(self):
        annots, names = super().add_final_annots2()
        annots.extend([(
            Cast(F('code0__sum'), output_field=FloatField()) /
            Cast(F('id__count'), output_field=FloatField()) *
            Value(100.0)
        ), (
            Cast(F('code1__sum'), output_field=FloatField()) /
            Cast(F('id__count'), output_field=FloatField()) *
            Value(100.0)
        ), (
            Cast(F('code2__sum'), output_field=FloatField()) /
            Cast(F('id__count'), output_field=FloatField()) *
            Value(100.0)
        )])
        # mandatory default_alias
        annots[-3].default_alias = 'code0_perc'
        annots[-2].default_alias = 'code1_perc'
        annots[-1].default_alias = 'code2_perc'
        names.extend([
            "nicht, wenig verdichtet (0-10%) (in %)",
            "teilweise verdichtet (10-30%) (in %)",
            "flächig verdichtet >30% (in %)",
        ])
        return annots, names


class GigantenQuery(QueryData):
    model = Giganten
    descr = 'Giganten'
    plot_obs = 'plot_obs_id__'

    def add_final_annots(self):
        return (
            [Count('id')],
            ['Anzahl Bäume']
        )


class EschentriebQuery(QueryData):
    model = Eschentriebsterben
    descr='Eschentriebsterben'
    plot_obs='plot_obs_id__'
    plot_prefix='plot_obs__plot'
    annot_map={'volumen_m3': Sum}

    def build_aggregations(self, query, aggrs):
        query, aggr_crits, field_names = super().build_aggregations(query, aggrs)
        # Force the ash_dieback grouping.
        aggr_crits.append('ash_dieback__description')
        field_names.append('*Eschentriebsterben')
        return query, aggr_crits, field_names

    def add_final_annots(self):
        annots, names = super().add_final_annots()
        annots.append(Count('id'))
        names.append('Stammzahl')
        return annots, names


VIEW_MAP = {
    'BWNATURN': QueryData(
        model=BWNATURN, descr='Naturnähe des Nadelholzanteils (BWNATURN)',
        plot_obs='plot_obs__', order_by='plot_obs',
        map_color_fields=['bwnaturn', 'fichte_perc', 'nadel_perc', 'nadel_wtanne_perc'],
        annot_map={'bwnaturn': Avg},
        groupables=['bwnaturn'],
    ),
    'BWARTEN': QueryData(
        model=BWARTEN, descr='Gehölzartenvielfalt (BWARTEN)',
        plot_obs='plot_obs__', plot_prefix='plot_obs__plot', order_by='plot_obs',
        map_color_fields=['bwarten', 'num_spec', 'special_spec'],
        annot_map={'bwarten': Avg, 'num_spec': Avg, 'special_spec': Avg},
        groupables=['bwarten'],
    ),
    'BWSTRU': QueryData(
        model=BWSTRU1M, descr='Gehölzartenvielfalt (BWARTEN)',
        plot_obs='id__', plot_prefix='id__plot',
        map_color_fields=['bwstru1m'],
        annot_map={
            'bwstru1m': Avg, 'stand_devel_stage': Avg,
            'stand_crown_closure': Avg, 'stand_structure': Avg
        },
        groupables=['bwstru1m'],
    ),
    'Biotopwert': QueryData(
        model=Biotopwert, descr='Biotopwert (BIOLFI1M)',
        plot_obs='id__',
        map_color_fields=['biolfi1m', 'bioklass'],
        annot_map={'biolfi1m': Avg},
        groupables=['otype', 'ostatus', 'bioklass'],
    ),
    'Stammzahl': QueryData(
        model=HolzProduktion, descr='Stammzahl, Volumen, Grundfläche',
        model_by_spec=HolzProduktionProSpec, model_by_spec_group=HolzProduktionProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
    ),
    'Totholz': QueryData(
        model=Totholz, descr='Totholz',
        model_by_spec=TotholzProSpec, model_by_spec_group=TotholzProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
    ),
    'Einwuchs': QueryData(
        model=Einwuchs, descr='Einwuchs',
        model_by_spec=EinwuchsProSpec, model_by_spec_group=EinwuchsProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
    ),
    'Nutzung': QueryData(
        model=Nutzung, descr='Nutzung',
        model_by_spec=NutzungProSpec, model_by_spec_group=NutzungProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
    ),
    'Zuwachs': ZuwachsQuery(),
    'Bodenschäden': BodenschadenQuery(),
    'Giganten': GigantenQuery(),
    'Eschentriebsterben': EschentriebQuery(),
    # Essentially to query verbose field names for some groupable keys not on views
    'plotobs': QueryData(model=PlotObs),
}
