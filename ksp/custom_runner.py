from django.test.runner import DiscoverRunner

class CustomRunner(DiscoverRunner):
    """
    This CutomRunner ensures that the 'afw' (unmanaged) database is created
    locally by tests.
    Makes afw unmanaged models managed for the duration of the test run.
    """
    def setup_databases(self, **kwargs):
        from django.db import connections
        # During tests, create the afw database locally
        del connections._databases['afw']['HOST']
        del connections._databases['afw']['PORT']
        del connections._databases['afw']['PASSWORD']
        return super().setup_databases(**kwargs)

    def setup_test_environment(self, **kwargs):
        from django.apps import apps
        for m in apps.get_app_config('afw').get_models():
            # Set managed = True and strip the schema part of the table names
            m._meta.managed = True
            m._meta.db_table =  m._meta.db_table.split('.')[1].strip('"')
        super().setup_test_environment(**kwargs)
