var CACHE_NAME = 'kspmobile-v1',
    debug = false,
    loginRequired = false,
    // No need to cache much at install time considering our "get-from-cache-or-fetch-and-cache" strategy.
    urlsToCache = [
      '/mobile/static/img/locked.svg',
      '/mobile/static/img/unlocked.svg',
      '/mobile/static/img/online.svg',
      '/mobile/static/img/offline.svg',
      '/mobile/static/img/plan.png',
      '/mobile/static/img/satellite.png',
      '/mobile/static/img/back.svg',
      '/mobile/static/img/okcontinue.svg',
      '/mobile/static/img/cancel.svg',
      '/mobile/static/img/newtree.svg',
      '/mobile/static/img/tree.svg',
      '/mobile/static/img/tree-dead.svg',
      '/mobile/static/img/tree-special.svg',
      '/mobile/static/img/tree-todo.svg'
    ];

self.addEventListener('install', event => {
    event.waitUntil(
        caches.open(CACHE_NAME).then(cache => {
            return cache.addAll(urlsToCache);
        })
    );
});

self.addEventListener('fetch', event => {
    // Return cached files if found in the cache
    if (loginRequired) {
        loginRequired = false;
        return Response.redirect('/login/');
    }
    event.respondWith(
        caches.match(event.request).then(from_cache => {
            // Even if a response comes from the cache, we still launch a network
            // request so as if the response is a HTTP 200, we replace the cached
            // instance by the new one (and ideally inform the client!).
            // Read https://ponyfoo.com/articles/progressive-networking-serviceworker
            // Evaluate adding {credentials: 'include'} for same origin requests
            var from_network = fetch(event.request.clone()).then(response => {
                // Check if we received a valid response (and from our origin with 'basic')
                if(!response || !response.ok || response.type !== 'basic') {
                    return response;
                }
                if (response.url.indexOf("/login/") >= 0) {
                    loginRequired = true;
                    return false;
                }
                if (!loginRequired && event.request.method == 'GET') {
                    // Cache the response
                    console.log("Caching " + event.request.url);
                    var responseToCache = response.clone();
                    caches.open(CACHE_NAME).then(cache => {
                        cache.put(event.request, responseToCache);
                    });
                }
                return response;
            }).catch(err => { return null; });
            if (debug)  return from_network || from_cache;
            else return from_cache || from_network;
        })
    );
});
