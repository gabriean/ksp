'use strict';
var AppVersion = "0.9.25";

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function isOnline() {
    return window.navigator.onLine;
}

function isArray(variable) {
    return Object.prototype.toString.call(variable) === '[object Array]';
}

function isBlank(form) {
    // FIXME: not valid for radio values
    for (var i=0; i < form.elements.length; i++) {
        if (!form.elements[i].name.length) continue;
        if (form.elements[i].value != '') return false;
    }
    return true;
}

function rounded(num, dec) { return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec); }

/*
 * Use the data-toggleimg img attribute to toggle its src image.
 * `img`: either the element itself or an ID.
 */
function toggleImg(img) {
    if (!(img instanceof HTMLElement)) {
        img = document.getElementById(img);
    }
    var newSrc = img.dataset.toggleimg;
    img.dataset.toggleimg = img.src;
    img.src = newSrc;
}

function totalHeight(elemId) {
    var style = document.defaultView.getComputedStyle(document.getElementById(elemId), '');
    var result = parseInt(style.getPropertyValue('height'), 10) + parseInt(style.getPropertyValue('margin-top'), 10) + parseInt(style.getPropertyValue('margin-bottom'), 10);
    if (isNaN(result)) return 0;
    return result;
}

function loadJSON(path) {
    return fetch(path).then(response => {
        if (response.url.indexOf("/login/") >= 0) {
            window.location.reload(true);
        }
        if (response.ok) {
            return response.json();
        } else {
            return Promise.reject({
                status: response.status,
                statusText: response.statusText
            });
        }
    });
}

function onEachFeature(feature, layer) {
    // Display property content for features
    if (feature.properties && feature.properties.popupContent) {
        layer.bindPopup(feature.properties.popupContent);
    }
}

function gradToRad(angle) {
     return angle * Math.PI / 200;
}

function gradToDeg(angle) {
    return 360 * angle / 400;
}

function degToRad(angle) {
  return angle * (Math.PI / 180);
}

function insertTableRow(tbody, templateSel, klass, values) {
    let template = document.querySelector(templateSel);
    let row = template.content.cloneNode(true);
    if (klass) row.firstElementChild.classList.add(klass);
    var tds = row.querySelectorAll("td");
    values.forEach((val, idx) => {
        tds[idx].textContent = val;
    });
    tbody.appendChild(row);
}

/*
 * Global variables
 *  - verbose_names is defined in the index.html template
 */
var db = new PouchDB('ksp', {adapter: 'idb'}),
    angleUnit = 'gon',
    currentPlot = null,
    thisYear = new Date().getFullYear(),
    unsyncCounter = null,
    progressBar = null,
    PLOTOBS_FORM_STEPS = 4,
    NEW_TREE_STEP = 999,
    FINAL_STEP = 1000,
    CENTER_INITIAL = [47.47, 7.65],
    ZOOM_INITIAL = 13,
    ZOOM_PLOT = 20;

var styles = {
    'BestandesKarte': {
        "color": "#ff7800",
        "weight": 1,
        "opacity": 0.65
    },
    'Erschliessung': {
        "color": "#ff0000",
        "weight": 2,
    }
}

var epsg2056Converter = proj4(
    proj4('EPSG:4326'),
    "+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=2600000 +y_0=1200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs"
);

/*
 * Map class
 */
var Map = function () {
    this.map = L.map('map-div');
    this.treeLayer = null;
    this.vLayers = {};
    this.centerOptions = {
        radius: 3,
        fillColor: "#ff1100"
    };
    this.map.createPane('highlightPane');
    this.map.getPane('highlightPane').style.zIndex = 650;
    this.orthoLayer = L.tileLayer.wms('https://geowms.bl.ch/', {
        layers: 'orthofotos_swissimage_2018_group',
        minZoom: 11,
        maxZoom: 21,
        maxNativeZoom: 18,
        useCache: true,
        crossOrigin: true
    });
    this.grundLayer = L.tileLayer.wms('https://geowms.bl.ch/', {
        layers: 'grundkarte_sw_group',
        minZoom: 11,
        maxZoom: 21,
        maxNativeZoom: 18,
        useCache: true,
        crossOrigin: true
    });
    this.kurvLayer = L.tileLayer.wms('https://geowms.bl.ch/', {
        layers: 'hoehenkurven_2018_raster',
        minZoom: 11,
        maxZoom: 21,
        maxNativeZoom: 18,
        format: 'image/png',
        transparent: true,
        useCache: true,
        crossOrigin: true
    });
    this.baseLayers = [this.grundLayer, this.kurvLayer];
    //L.tileLayer("http://tile.geoview.bl.ch/1.0.0/orthophoto_agi/default/20150422/21781/{z}/{x}/{y}.jpeg", {layers: 'orthofotos_agi'}).addTo(map);
    this.baseLayers.forEach(lay => lay.addTo(this.map));
    // Initially empty layers
    this.inventoryLayer = L.geoJSON().addTo(this.map);
    this.plotLayer = L.layerGroup().addTo(this.map);
};

Map.prototype.attachHandlers = function () {
    var self = this;
    this.map.on('moveend', (ev) => {
        // Store current center and zoom level
        var zoom = self.map.getZoom(),
            center = self.map.getCenter();
        db.get('current-state').then(currentState => {
            var changed = false;
            if (currentState.zoom !== zoom) {
                currentState.zoom = zoom;
                changed = true;
            }
            if (!center.equals(currentState.center)) {
                currentState.center = center;
                changed = true;
            }
            if (changed) {
                db.put(currentState).catch(err => {
                    // Ignoring any error, as this is not critical;
                    console.log("Non-fatal error: " + err);
                    return;
                });
            }
        });
    });
    // Display current position on the map
    this.longDiv = document.getElementById('currentLong');
    this.latDiv = document.getElementById('currentLat');
    if (navigator.geolocation) {
        navigator.geolocation.watchPosition(position => {
                self.setCurrentPosition(position);
            }, error => {
                console.log("Geolocation error: ", error.code, error.message);
            },
            {timeout: 10000, enableHighAccuracy: true, maximumAge: 5000
        });
    }
};

Map.prototype.setCurrentPosition = function (position) {
    // Diff from my position in CdF (debugging help)
    var devDiff = position.coords.longitude < 7 ? [0.457, 0.85] : [0, 0];
    if (!this.currentPositionLayer) {
        this.currentPositionLayer = L.marker(
            [0, 0], {
            icon: L.icon({iconUrl: staticImages.currentPos, iconSize: [20, 20]}),
            opacity: 0.9
        });
        this.map.addLayer(this.currentPositionLayer);
    }
    this.currentPositionLayer.setLatLng([
        position.coords.latitude + devDiff[0],
        position.coords.longitude + devDiff[1]
    ]);
    var projected = epsg2056Converter.forward([
        position.coords.longitude + devDiff[1],
        position.coords.latitude + devDiff[0]]
    );
    this.longDiv.innerHTML = rounded(projected[0], 2);
    this.latDiv.innerHTML = rounded(projected[1], 2);
};

Map.prototype.switchBaseLayers = function () {
    this.baseLayers.forEach(lay => this.map.removeLayer(lay));
    this.baseLayers = this.baseLayers[0] === this.orthoLayer ? [this.grundLayer, this.kurvLayer] : [this.orthoLayer];
    this.baseLayers.forEach(lay => lay.addTo(this.map));
};

Map.prototype.toCurrentPosition = function () {
    if (this.currentPositionLayer) {
        var newPos = this.currentPositionLayer.getLatLng();
        this.map.setView(newPos, Math.max(this.map.getZoom(), 16));
    } else {
        alert("Ihr Standort konnte leider nicht ermittelt werden.");
    }
};

Map.prototype.resetLayers = function () {
    if (this.vLayers.treeLayer) {
        this.vLayers.treeLayer.clearLayers();
    }
    if (this.vLayers.plotCircle) {
        this.map.removeLayer(this.vLayers.plotCircle);
    }
    if (this.vLayers.plotCenter) {
        this.map.removeLayer(this.vLayers.plotCenter);
    }
    if (this.vLayers.ghostLayer) {
        this.map.removeLayer(this.vLayers.ghostLayer);
    }
};

Map.prototype.highlightPoint = function(coords) {
    // Show the point at coords in red on the map
    this.unHighlight();
    if (isNaN(coords[0])) return;
    this.highlightLayer = L.circle(coords, {
        radius: 1, color: '#ff0000', pane: 'highlightPane'
    });
    this.map.addLayer(this.highlightLayer);
};

Map.prototype.unHighlight = function () {
    if (this.highlightLayer) {
        this.map.removeLayer(this.highlightLayer);
        this.highlightLayer = null;
    }
};

Map.prototype.removePointsLayer = function (layerType) {
    if (this.vLayers[layerType]) {
        this.map.removeLayer(this.vLayers[layerType]);
        this.vLayers[layerType] = null;
    }
};

Map.prototype.loadLayer = function (layerName, url) {
    var self = this;
    loadJSON(url).then(geojson => {
        if (self.vLayers[layerName]) {
            self.vLayers[layerName].clearLayers();
            self.vLayers[layerName].addData(geojson);
        } else {
            self.vLayers[layerName] = L.geoJSON(geojson, {
                style: styles[layerName],
                onEachFeature: onEachFeature
            });
            self.vLayers[layerName].addTo(self.map);
        }
    });
};

Map.prototype.showInventory = function (geojson, fitMap) {
    this.inventoryLayer.clearLayers();
    // Add inventory data to layer
    this.inventoryLayer.addData(geojson);
    if (fitMap) this.map.fitBounds(this.inventoryLayer.getBounds());
};

Map.prototype.resetToOverview = function() {
    // Reset map/info div sizes
    map.unHighlight();
    document.getElementById('map-container').classList.remove('focused');
    document.getElementById('infos-div').classList.remove('focused');
    document.getElementById('error').style.display = 'none';
    var self = this;
    setTimeout(() => { self.map.invalidateSize()}, 400);
    // Return to the inventory view
    if (this.map._stored_map_state) {
        this.map.setView(this.map._stored_map_state[0], this.map._stored_map_state[1]);
    } else {
        this.map.fitBounds(this.inventoryLayer.getBounds());
    }
    if (!this.map.hasLayer(this.plotLayer)) this.map.addLayer(this.plotLayer);
};

Map.prototype.focusOnPlot = function (coords, radius) {
    this.map.removeLayer(this.plotLayer);
    this.vLayers.plotCircle = L.circle(coords, radius).addTo(this.map);
    this.vLayers.plotCenter = L.circle(coords, 0.1).addTo(this.map);
    // Make the map div smaller when focused on plot.
    document.getElementById('map-container').classList.add('focused');
    document.getElementById('infos-div').classList.add('focused');
    var self = this;
    setTimeout(() => { self.map.invalidateSize()}, 400);
    this.map._stored_map_state = [this.map.getCenter(), this.map.getZoom()];
    this.map.setView(coords, ZOOM_PLOT);
};

/*
 * Store map data offline.
 * Vector data are already automatically cached through serviceworker.js.
 */
Map.prototype.storeOffline = function () {
    var bbox = this.map.getBounds(),
        zoomCurrent = this.map.getZoom(),
        zoomMax = this.baseLayers[0].options.maxNativeZoom;
    if (zoomMax - zoomCurrent > 4) {
        alert("Sie müssen einen kleineren Bereich auswählen um die Karte Daten lokale zu speichern.");
        return;
    }

    document.getElementById('progress').style.display = 'block';
    progressBar = new ProgressBar.Line('#progress', {
        color: '#008000', strokeWidth: 5
    });
    this.baseLayers[0].on('seedprogress', (seedData) => {
        //var percent = 100 - Math.floor(seedData.remainingLength / seedData.queueLength * 100);
        progressBar.set(progressBar.value() + (1 / seedData.queueLength));
    });
    this.baseLayers[0].on('seedend', (seedData) => {
        progressBar.destroy();
        progressBar = null;
        document.getElementById('progress').style.display = 'none';
        alert("Daten wurden erfolgreich gespeichert");
    });

    /*
     * Some figures about tiles caching:
     * tile size: 256x256
     * tile download size: 25Kb
     * tile size in db: (Base64), ~220Kb??
     * on one map level: 12 tiles
     * seeding:
     *   - 1 level: 42/48 tiles (1.1Mb dl / 10Mb stor.)
     *   - 2 levels: 140/150 tiles (3.6Mb dl / 35Mb stor.)
     *   - 3 levels: ~500 tiles (12.5Mb dl / 120Mb stor.)
     *   - 4 levels: ~2000 tiles (50Mb dl / 500Mb stor.)
     * Browser Storage docs:
     *  https://medium.com/dev-channel/offline-storage-for-progressive-web-apps-70d52695513c#.tva434uxh
     */
    this.baseLayers[0].seed(bbox, zoomCurrent, zoomMax);
    // Progress/end of seeding is handled in 'seedprogress'/'seedend' events
}

/*
 * TreeObs class
 */
var TreeObs = function (plotobs, data) {
    this.plotobs = plotobs;
    this.treePoint = null;  // the marker for the map.
    if (data) {
        // Data is a GeoJSON structure received from a query on existing PlotObs
        this.validated = !(data.validated === false);
        this.properties = data.properties;
        this._coords = data.geometry.coordinates;
    } else {
        this.validated = false;
        this.properties = {};
        this._coords = null;
    }
};

TreeObs.prototype.setProperty = function(name, value) {
    this.properties[name] = value;
    if (name == 'azimuth' || name == 'distance') {
        this._coords = null; // Invalidate calculated coords
    }
};

TreeObs.prototype.getPreviousTreeObs = function() {
    var prevPlotObs = this.plotobs.plot.previousObs(this.plotobs.year);
    if (prevPlotObs) {
        for (var i = 0; i < prevPlotObs.trees.length; i++) {
            if (prevPlotObs.trees[i].properties.tree == this.properties.tree) return prevPlotObs.trees[i];
        }
    }
    return null;
}

TreeObs.prototype.getCoords = function (reverse) {
    // Return lon, lat (as geoJSON), unless `reverse` is true
    if (!this._coords) {
        // Calculate new coords based on distance (m) and azimuth (grad)
        var plotCoords = this.plotobs.geometry.coordinates;
        var distance = parseFloat(this.properties.distance);
        var delta_lon = (distance / 10) * Math.sin(gradToRad(this.properties.azimuth)) / (111320 *Math.cos(degToRad(plotCoords[1])));
        var delta_lat = (distance / 10) * Math.cos(gradToRad(this.properties.azimuth)) / 110540;
        this._coords = [
            plotCoords[0] + delta_lon,
            plotCoords[1] + delta_lat
        ];
    }
    if (reverse) return this._coords.slice().reverse();
    else return this._coords;
};

TreeObs.prototype.setCoords = function (coords) {
    this._coords = coords.slice();
};

TreeObs.prototype.asGeoJSON = function () {
    return {
        type: "Feature",
        geometry: {type: "Point", coordinates: this.getCoords()},
        validated: this.validated,
        properties: this.properties
    };
};

TreeObs.prototype.showOnMap = function (map, parentLayerName) {
    var layerOpts = {},
        size = 20,
        treeIcon = staticImages.tree,
        coords = this.getCoords(true),
        self = this;
    if (isNaN(coords[0])) return; // No available coords yet for this point
    if (!map.vLayers[parentLayerName]) {
        map.vLayers[parentLayerName] = L.layerGroup();
        map.map.addLayer(map.vLayers[parentLayerName]);
    }
    if (!this.validated) {
        var prevTObs = this.getPreviousTreeObs();
        if (prevTObs && prevTObs.properties.dbh == 0) {
            // Special case where a living tree was wrongly marked with dbh=0
            treeIcon = staticImages.treeSpecial;
        } else {
            treeIcon = staticImages.treeTodo;
        }
    } else {
        size = Math.floor(13 + ((Math.abs(this.properties.dbh) - 11) / 4));
        let vitaStr = this.properties.vita[1];
        if (vitaStr.includes("(c)") || vitaStr.includes("(x)") || vitaStr.includes("(y)")) {
            treeIcon = staticImages.treeCut;
        } else if (vitaStr.includes("(m)") || vitaStr.includes("(z)")) {
            treeIcon = staticImages.treeDead;
        }
    }
    var treeNum = this.properties.nr === undefined ? '' : this.properties.nr;
    if (this.treePoint !== null && map.vLayers['ghostLayer']) {
        map.vLayers['ghostLayer'].removeLayer(this.treePoint);
    }
    this.treePoint = L.marker(coords, {
        icon: L.divIcon({
            className: 'icon-tree',
            iconSize: [size, size],
            html: '<img src="' + treeIcon + '" height="' + size + '" width="' + size + '"><div class="treenumb hidden">' + treeNum + '</div>'
        }),
        opacity: 0.9
    });
    this.treePoint.on({click: this.makeCurrent.bind(this)});
    map.vLayers[parentLayerName].addLayer(this.treePoint);
};

TreeObs.prototype.makeCurrent = function () {
    /* If editing, go to proper step.
     * Else, highlight point and line in tree list.
    */
    if (this.plotobs.fillStep < 100) {
        var step = PLOTOBS_FORM_STEPS + this.plotobs.trees.indexOf(this) + 1;
        goToStep(step);
    } else {
        var treeTable = document.getElementById('obs-infos-trees-table');
        if (treeTable.offsetParent !== null) {
            var current = treeTable.getElementsByClassName("current").item(0);
            if (current) current.classList.remove("current");
            document.getElementById('treeline-' + this.properties.nr).classList.add("current");
            map.highlightPoint(this.getCoords(true));
        }
    }
};

/*
 * PlotObs class
 */
var PlotObs = function (plot, data) {
    this.trees = [];
    this.plot = plot;
    if (data) {
        // When data is provided, it is a GeoJSON structure.
        // The first feature is the center coords with PlotObs properties
        this.id = data.features[0].id;
        this.plot_id = data.plot;
        this.year = data.features[0].properties.year;
        this.geometry = data.features[0].geometry;
        this._properties = data.features[0].properties;
        // Following features are trees
        for (var i = 1; i < data.features.length; i++) {
            this.trees.push(new TreeObs(this, data.features[i]));
        }
        this.trees.sort((a, b) => a.properties.azimuth - b.properties.azimuth);
        if (data.fillstep) this.fillStep = data.fillstep;
        else this.fillStep = 100;
    } else {
        this.plot_id = plot.id;
        this._properties = {type: "center"};
        this.geometry = plot.center();
        this.fillStep = 1;
    }
};

PlotObs.prototype.asGeoJSON = function () {
    // First feature is the Plot center with the PlotObs properties.
    // All following features are trees.
    var struct = {
        type: "FeatureCollection",
        plot: this.plot_id,
        fillstep: this.fillStep,
        features: [{
            type: "Feature",
            geometry: this.geometry,
            properties: this._properties
        }]
    };
    for (var i=0; i < this.trees.length; i++) {
        struct.features.push(this.trees[i].asGeoJSON());
    }
    return struct;
};

PlotObs.prototype.dbId = function () {
    return 'plotobs-' + this.plot_id + '-' + this.year;
};

PlotObs.prototype.properties = function () {
    var result = {};
    for (var prop in this._properties) {
        // Exclude non-interesting props or data already shown at plot level
        if (['type', 'radius', 'Aufnahmejahr', 'Gemeinde', 'Aufnahmepunkt (plot)'].indexOf(prop) >= 0) {
            continue;
        }
        result[prop] = this._properties[prop];
    }
    return result;
};

PlotObs.prototype.setProperty = function(name, value) {
    this._properties[name] = value;
    if (name == 'year') {
        this.year = value;
    }
    // When forest edge factor is lower than 0.6, no trees are observed.
    if (name == 'forest_edgef' && parseFloat(value) < 0.6) {
        this.trees = [];
        map.removePointsLayer('treeLayer');
        map.removePointsLayer('ghostLayer');
        // Remove allstepselect entries for trees
        var stepSelect = document.getElementById('allstepsselect');
        for (var i = stepSelect.length - 1; i >= 0; i--) {
            if (stepSelect.options[i].text.indexOf("Baum") >= 0) {
                stepSelect.removeChild(stepSelect.options[i]);
            }
        }
        document.getElementById('stepfinal').getElementsByClassName('newtree')[0].style.display = 'none';
    }
};

PlotObs.prototype.addTreeObs = function (previousTree) {
    var tree = new TreeObs(this);
    if (previousTree) {
        // Copy all properties from previous treeObs except dbh
        tree.setCoords(previousTree.getCoords());
        tree.properties = JSON.parse(JSON.stringify(previousTree.properties)); // Deep copy
        delete tree.properties.obs;
        tree.properties.prev_dbh = tree.properties.dbh;
        tree.properties.dbh = '';
    }
    this.trees.push(tree);
    return tree;
};

PlotObs.prototype.notCutTrees = function () {
    return this.trees.filter(item => !item.properties.vita[1].includes("(c)"));
};

PlotObs.prototype.getNextAvailableNumber = function () {
    var number = 1;
    for (var i = 0; i < this.trees.length; i++) {
        var nr = parseInt(this.trees[i].properties['nr']);
        if (nr >= number) {
            number = nr + 1;
        }
    }
    return number;
};

PlotObs.prototype.setUpStepSelect = function () {
    var stepSelect = document.getElementById('allstepsselect');
    // Reset step select to its initial state
    if (!stepSelect.dataset.initial) {
        stepSelect.dataset.initial = stepSelect.innerHTML;
    } else {
        stepSelect.innerHTML = stepSelect.dataset.initial;
    }
    for (var i=1; i < stepSelect.options.length; i++) {
        if (this.fillStep >= stepSelect.options[i].value)
            stepSelect.options[i].removeAttribute("disabled");
    }
    document.getElementById('stepfinal').getElementsByClassName('newtree')[0].style.display = 'block';
    // Insert a select option for each tree step
    var finalOpt = stepSelect.options[stepSelect.options.length - 1];
    for (var i=0; i < this.trees.length; i++) {
        var opt = document.createElement('option');
        opt.value = i + PLOTOBS_FORM_STEPS + 1;
        opt.innerHTML = "Schritt " + opt.value + ": Baum nr " + this.trees[i].properties.nr;
        if (this.fillStep < PLOTOBS_FORM_STEPS + 1) {
            opt.setAttribute("disabled", "disabled");
        }
        stepSelect.insertBefore(opt, finalOpt);
    }
};

PlotObs.prototype.showDetails = function() {
    this.showTrees(map);
    document.getElementById('obs-input').style.display = 'none';
    document.getElementById('error').style.display = 'none';
    document.getElementById('button-' + this.year).classList.add("current");

    // Show plotobs data
    var generalTable = document.getElementById('obs-infos-general-table');
    generalTable.innerHTML = "";
    var obsProperties = this.properties();
    for (var prop in obsProperties) {
        var tr = generalTable.insertRow();
        var td = tr.insertCell();
        var verbose_prop = verbose_names[prop];
        if (!verbose_prop) verbose_prop = prop;
        td.appendChild(document.createTextNode(verbose_prop));
        td = tr.insertCell();
        var value = obsProperties[prop];
        if (isArray(value)) {
            value = value[1]; // first is object pk, second is human-readable value
        }
        td.appendChild(document.createTextNode(value));
    }
    // Show tree data
    var treeTable = document.getElementById('obs-infos-trees-table');
    treeTable.innerHTML = "";
    for (var i=0; i<this.trees.length; i++) {
        var tree = this.trees[i];
        if (!tree.validated) continue;
        var tr = treeTable.insertRow();
        tr.setAttribute("id", "treeline-" + tree.properties.nr);
        var td = tr.insertCell();
        td.appendChild(document.createTextNode(tree.properties.nr + '.'));
        td = tr.insertCell();
        td.appendChild(document.createTextNode(tree.properties.species[1]));
        td = tr.insertCell();
        var azimuth = angleUnit == 'gon' ? tree.properties.azimuth : Math.round(tree.properties.azimuth * 0.9);
        td.appendChild(document.createTextNode(
            '⌀' + tree.properties.dbh + '/' + azimuth + '/' + tree.properties.distance
        ));
        td = tr.insertCell();
        td.appendChild(document.createTextNode(tree.properties.vita[1]));
        tr.addEventListener('click', tree.makeCurrent.bind(tree));
    }
    // Set height and visibility
    resizeObsDivs();
}

PlotObs.prototype.showTrees = function (map) {
    map.removePointsLayer('treeLayer');
    map.removePointsLayer('ghostLayer');
    if (this.fillStep < 100) {
        for (var i=0; i < this.trees.length; i++) {
            this.trees[i].showOnMap(map, 'ghostLayer');
        }
    } else {
        if (map.vLayers.treeLayer) map.vLayers.treeLayer.clearLayers();
        for (var i=0; i < this.trees.length; i++) {
            this.trees[i].showOnMap(map, 'treeLayer');
        }
    }
};

PlotObs.prototype.finished = function () {
    this.fillStep = 100;
    var self = this;
    if (isOnline()) {
        this.sendToServer().catch(err => {
            var msg = "Die aufnahmedaten konnten nicht auf dem Server gespeichert werden. Sie werden lokale gespeichern. (Fehler ist: " + err.statusText + ")";
            if (err.responseText) msg += "\nDetails: " + err.responseText;
            alert(msg);
            self.saveLocally();
        }).then(() => {
            // force icon refresh as its color may change
            self.plot.showOnMap(map);
        });
    } else {
        this.saveLocally();
    }
};

PlotObs.prototype.saveLocally = function () {
    var id = this.dbId();
    saveData(id, this.asGeoJSON());
    // Save the id for future sync
    db.get('new-plotobs').catch(err => {
        if (err.name === 'not_found') {
            return {
                _id: 'new-plotobs',
                newIDs: []
            };
        }
    }).then(doc => {
        if (doc.newIDs.indexOf(id) < 0) {
            doc.newIDs.push(id);
            unsyncCounter.increment();
            return db.put(doc);
        }
    });
    // force icon refresh as its color may change
    if (this.plot) this.plot.showOnMap(map);
};

PlotObs.prototype.remove = function () {
    // Remove an unfinished PlotObs (no UI for this function yet)
    // TODO: change color of year button and plot point
    if (this.fillStep >= 100) {
        alert("Unable to remove a completed observation.");
        return;
    }
    var docId = this.dbId();
    db.get(docId).then(doc => {
        unsyncCounter.decrement();
        // Remove the id from the newIDs list
        db.get('new-plotobs').then(doc => {
            doc.newIDs.splice(doc.newIDs.indexOf(docId), 1);
            db.put(doc);
        });
        return db.remove(doc);
    }).catch(err => {
        alert("An error occurred while trying to remove the current observation: " + err.statusText);
    }).then(() => {
        delete currentPlot.obs[thisYear];
        currentPlot._years.shift();
        var prevObs = currentPlot.previousObs(thisYear);
        if (prevObs) {
            document.getElementById("button-" + prevObs.year).click();
        }
    });
};

PlotObs.prototype.sendToServer = function () {
    // Send data to server
    var self = this;
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open("POST", sync_url);
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                if (this.responseURL.indexOf('/login/') >= 0) {
                    reject({status: 403, responseText: "Not logged in the platform."});
                } else {
                    resolve(self);
                }
            } else {
                reject({
                    status: this.status,
                    statusText: xhr.statusText,
                    responseText: this.responseText
                });
            }
        };
        xhr.onerror = function () {
            reject({
                status: this.status,
                statusText: xhr.statusText
            });
        };
        var postData = JSON.stringify(self.asGeoJSON());
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.send(postData);
    }).then(() => {
        unsyncCounter.decrement();
        // Remove the id from the newIDs list
        db.get('new-plotobs').then(doc => {
            doc.newIDs.splice(doc.newIDs.indexOf(self.dbId()), 1);
            db.put(doc);
        });
    }).catch(err => {
        // This will allow reediting the PlotObs.
        self.fillStep = 99;
        self.saveLocally();
        throw err;
    });
};

/*
 * Plot class
 */
var Plot = function (feature) {
    this.id = feature.id;
    this.geoJSON = feature;
    this.properties = feature.properties;
    this.obs = {};
    this.layer = null;
    var arr = Object.keys(this.geoJSON.properties.obsURLs);
    this._years = arr.map(obj => parseInt(obj));
};

Plot.prototype.center = function() {
    return this.geoJSON.geometry;
};

Plot.prototype.years = function() {
    // Return existing PlotObs years, sorted from more recent to older.
    this._years.sort();
    this._years.reverse();
    return this._years;
};

Plot.prototype.fillYearButtonsDiv = function () {
    var buttonDiv = document.getElementById('year-buttons'),
        years = this.years().slice(),
        hasNewYear = false,
        self = this;
    if (years.indexOf(thisYear) < 0) {
        // Add "this" year if not existing, in red
        years.unshift(thisYear);
        hasNewYear = true;
    }
    for (var i=0; i<years.length; i++) {
        var button = document.createElement("button"),
            year = years[i];
        button.setAttribute('id', 'button-' + year);
        button.className = "year";
        if (year == thisYear) {
            if (hasNewYear) button.classList.add("new");
            else if (self.obs[year].fillStep < 100) button.classList.add("editing");
        }
        button.innerHTML = year;
        button.addEventListener('click', function yearClick(ev) {
            if (this.className.indexOf('current') > -1) return;
            var oldCurrent = document.querySelector("button.current");
            if (oldCurrent) oldCurrent.classList.remove("current");
            this.classList.add("current");
            var obs = self.obs[parseInt(this.innerText)];
            if (obs === undefined || obs.fillStep < 100) {
                formSet.startInput(obs);
            } else {
                obs.showDetails();
            }
        });
        buttonDiv.appendChild(button);
    }
}

Plot.prototype.hasObs = function (year) {
    return this.years().indexOf(year) >= 0;
};

Plot.prototype.getObs = function (year) {
    var self = this;
    return new Promise(function(resolve, reject) {
        if (self.obs[year]) resolve(self.obs[year]);
        else if (self.properties.obsURLs[year] !== undefined) {
            loadJSON(self.properties.obsURLs[year]).then(data => {
                var po = new PlotObs(self, data);
                self.setObs(po, year);
                resolve(po);
            }).catch(err => {
                reject(new Error("Die Daten für diese Stichprobe sind leider nicht offline verfügbar."));
            });
        } else {
            // Check if locally saved
            db.get('plotobs-' + self.id + '-' + year).then(doc => {
                var po = new PlotObs(self, doc);
                self.setObs(po, year);
                resolve(po);
            }).catch(err => {
                reject(new Error("Keine Daten für Jahr " + year));
            });
        }
    });
};

Plot.prototype.setObs = function (po, year) {
    this.obs[year] = po;
    if (this._years.indexOf(year) < 0) this._years.push(year);
};

Plot.prototype.getLatestObs = function () {
    // Return the more recent finished PlotObs
    var proms = [];
    for (let i = 0; i < this._years.length; i++) {
        proms.push(this.getObs(this.years()[i]));
    }
    return Promise.all(proms).then(obss => {
        for (let i = 0; i < obss.length; i++) {
            if (obss[i].fillStep >= 100) return obss[i];
        }
    });
};

Plot.prototype.previousObs = function (year) {
    var previousIdx = this.years().indexOf(parseInt(year)) + 1;
    // We suppose the previous obs has been loaded (should be, but...)
    if (previousIdx < this.years().length) return this.obs[this.years()[previousIdx]];
    else return null;
};

Plot.prototype.createObs = function (year) {
    var po = new PlotObs(this, null);
    po.setProperty('year', year);
    this.obs[year] = po;
    this._years.unshift(year);

    var previousObs = this.previousObs(year);
    if (previousObs) {
        var trees = previousObs.notCutTrees();
        // Create a copy of each uncut tree from previous obs
        for (var i=0; i < trees.length; i++) {
            po.addTreeObs(trees[i]);
        }
    }
    return po;
};

Plot.prototype.showOnMap = function (map) {
    if (this.layer) {
        map.plotLayer.removeLayer(this.layer);
    }
    var icon = L.divIcon({className: 'icon-plot-blue', iconSize: [20, 20], iconAnchor: [10, 10]});
    // Show different icon color if obs for this year is already done (or partially done)
    if (this.hasObs(thisYear)) {
        if (this.obs[thisYear] && this.obs[thisYear].fillStep < 100) {
            icon = L.divIcon({className: 'icon-plot-orange', iconSize: [20, 20], iconAnchor: [10, 10]});
        } else {
            icon = L.divIcon({className: 'icon-plot-green', iconSize: [20, 20], iconAnchor: [10, 10]});
        }
    }
    var coords = this.center().coordinates.slice().reverse();
    this.layer = L.marker(coords, {icon: icon});
    var self = this;
    this.layer.on({click: (ev) => {
        map.focusOnPlot(coords, self.properties.radius);
        self.getLatestObs().then(obs => {
            self.showDetails();
            if (obs) obs.showDetails();
        }).catch(err => {
            // Display error
            document.getElementById('error').innerHTML = err;
            document.getElementById('error').style.display = 'block';
        });
    }});
    map.plotLayer.addLayer(this.layer);
};

Plot.prototype.showDetails = function () {
    document.getElementById('obs-input').style.display = 'none';
    // Show general plot data under the map
    document.getElementById('gemeinde-placeholder').textContent = this.properties.municipality_name;
    document.getElementById('koord-placeholder').innerHTML = '<span>' + this.properties.coords_2056[0] + '</span><span>' + this.properties.coords_2056[1] + '</span>';
    if (this.properties.coords_2056_exact) {
        document.getElementById('koord-placeholder').innerHTML += '<span>' + this.properties.coords_2056_exact[0] + '</span><span>' + this.properties.coords_2056_exact[1] + '</span>';
    }
    // Show plot data
    document.getElementById('plot-nr').textContent = this.properties.nr;
    if (this.properties.sealevel) {
        document.getElementById('plot-hohe').textContent = this.properties.sealevel + 'm';
    }
    document.getElementById('plot-slope').textContent = this.properties.slope;
    document.getElementById('plot-expo').textContent = this.properties.exposition;
    document.getElementById('plot-radius').textContent = this.properties.radius + 'm';
    document.getElementById('plot-data').style.display = 'block';
    if (this != currentPlot) {
        currentPlot = this;
        this.fillYearButtonsDiv();
    }
};

/*
 * Form instance to edit/add a tree obs.
 */
var TreeForm = function (tree, step, isFirstInv) {
    this.tree = tree;
    this.form = document.getElementById('steptree');
    this.vitaSelect = document.getElementById('id_vita');
    this.reset();

    var isNewTree = tree === null || tree.properties.tree == '';
    if (isNewTree) {
        // Only new trees can be canceled
        document.getElementById('canceltree').style.display = "block";
        // Only let empty, 'e' (if not first inventory), 'l', 'm' or 'z' as enabled vita choices
        for (var i = 0; i < this.vitaSelect.length; i++) {
            var opt = this.vitaSelect[i];
            if (i == 0 || (!isFirstInv && opt.text.includes('(e)')) || opt.text.includes('(l)')
                       || opt.text.includes('(m)') || opt.text.includes('(z)')) {
                opt.disabled = false;
            } else opt.disabled = true;
        }
    } else {
        document.getElementById('canceltree').style.display = "none";
        // Disable vita 'e' option
        for (var i = 0; i < this.vitaSelect.length; i++) {
            if (this.vitaSelect[i].text.includes('(e)'))
                this.vitaSelect[i].disabled = true;
            else this.vitaSelect[i].disabled = false;
        }
        // Inform widget of previous dbh value
        var prevTObs = tree.getPreviousTreeObs();
        if (prevTObs) {
            document.getElementById('id_dbh').dataset.previousValue = prevTObs.properties.dbh;
        }
        document.getElementById('id_dbh').focus();
    }

    if (tree === null) {
        // Entering a blank new tree form
        map.unHighlight();
        // Add a new option to the step select
        var obs = currentPlot.obs[thisYear];
        var stepSelect = document.getElementById('allstepsselect');
        var opt = document.createElement('option');
        opt.value = step;
        opt.innerHTML = "neuer Baum";
        stepSelect.insertBefore(opt, stepSelect.options[stepSelect.options.length - 1]);
        stepSelect.selectedIndex = stepSelect.options.length - 2;

        // Enable 'e' or 'l' vita select choice by default
        var defaultVita = isFirstInv ? "(l)" : "(e)";
        for (var i = 0; i < this.vitaSelect.length; i++) {
            if (this.vitaSelect[i].text.includes(defaultVita))
                this.vitaSelect.selectedIndex = i;
        }
        document.getElementById('id_nr').value = obs.getNextAvailableNumber();
    } else {
        // Populate form from tree properties
        setFormDefaultFromProperties(this.form, tree.properties);
        if (angleUnit == 'deg') {
            this.form.elements.id_azimuth.value = Math.round(parseInt(this.form.elements.id_azimuth.value) * 0.9);
        }
        if (tree.properties.tree != '' && !tree.validated && !tree.properties.vita[1].includes('(m)')) {
            // Force vita to 'l' (unless it was dead (m) before)
            for (var i = 0; i < this.vitaSelect.length; i++) {
                if (this.vitaSelect[i].text.indexOf("(l)") >= 0) {
                    this.vitaSelect.selectedIndex = i;
                    break;
                }
            }
        }

        map.highlightPoint(tree.getCoords(true));
    }
    this.hideHistoricTrees();
    TreeForm.checkStemHeight();
    TreeForm.setValuesFromVita();

    this.form.dataset.step = step;
};

TreeForm.prototype.reset = function () {
    formSet.resetForm(this.form);
    this.form.elements.id_tree.value = '';
    document.getElementById('azimuth-unit').innerHTML = angleUnit;
    document.getElementById('dbh_warning').style.display = 'none';
    document.getElementById('stem_height_warning').style.display = 'none';
    document.getElementById('id_distance').max = currentPlot.properties.radius * 10;
    var dbh = document.getElementById('id_dbh');
    dbh.removeAttribute("disabled");
    dbh.setAttribute("min", "12");
    dbh.dataset.previousValue = null;
    // Undo hideHistoricTrees
    var specSelect = document.getElementById('id_species');
    for (var j = 0; j < specSelect.options.length; j++) {
        specSelect.options[j].style.display = 'block';
    }
};

TreeForm.prototype.hideHistoricTrees = function () {
    // Hide all 'historic' species unless selected
    var specSelect = document.getElementById('id_species');
    var warn = false;
    for (var j = 0; j < specSelect.options.length; j++) {
        if (specSelect.options[j].dataset.historic == 'true') {
            if (j == specSelect.selectedIndex) {
                warn = true;
                document.getElementById('species_warning').style.display = 'block';
            } else {
                specSelect.options[j].style.display = 'none';
            }
        }
    }
    if (!warn) document.getElementById('species_warning').style.display = 'none';
};

TreeForm.checkStemHeight = function () {
    var stemH = document.getElementById('id_stem_height');
    if (stemH.value != '') {
        document.getElementById('stem_height_warning').style.display = 'none';
        return;
    }
    var vita = document.getElementById('id_vita');
    var vitaCode = vita.options[vita.selectedIndex].text.substr(-2, 1);
    var stem = document.getElementById('id_stem');
    var stemCode = stem.options[stem.selectedIndex].text.substr(-2, 1);

    if (vitaCode == 'z' || vitaCode == 'm' || stemCode != 'B') {
        document.getElementById('stem_height_warning').style.display = 'block';
    } else {
        document.getElementById('stem_height_warning').style.display = 'none';
    }
};

/*
 * Method called when the vita (Lebenslauf) select changes or is initialized
 * with a new tree.
*/
TreeForm.setValuesFromVita = function () {
    var vita = document.getElementById('id_vita');
    var vitaCode = vita.options[vita.selectedIndex].text.substr(-2, 1);
    // A cut tree has dbh forced to 0
    if (vitaCode == 'c' || vitaCode == 'x' || vitaCode == 'y') {
        document.getElementById('id_dbh').setAttribute("min", "0");
        document.getElementById('id_dbh').value = '0';
        document.getElementById('id_dbh').setAttribute("disabled", "disabled");
    } else {
        document.getElementById('id_dbh').removeAttribute("disabled");
        document.getElementById('id_dbh').setAttribute("min", "12");
    }
    // For cut, unfound or outside trees, select values are set to '' and disabled
    var selects = ['layer', 'rank', 'damage', 'damage_cause', 'crown_form',
                   'crown_length', 'stem', 'stem_forked'];
    var nullBooleans = ['stem_forked', 'woodpecker'];
    if (vitaCode == 'c' || vitaCode == 'x' || vitaCode == 'y') {
        for (var i = 0; i < selects.length; i++) {
            document.getElementById('id_' + selects[i]).value = '';
            document.getElementById('id_' + selects[i]).setAttribute("disabled", "disabled");
        }
        for (var i = 0; i < nullBooleans.length; i++) {
            document.getElementById('id_' + nullBooleans[i] + '_0').checked = true;
            document.getElementById('id_' + nullBooleans[i] + '_0').setAttribute("disabled", "disabled");
            document.getElementById('id_' + nullBooleans[i] + '_1').setAttribute("disabled", "disabled");
            document.getElementById('id_' + nullBooleans[i] + '_2').setAttribute("disabled", "disabled");
        }
    } else {
        for (var i = 0; i < selects.length; i++) {
            document.getElementById('id_' + selects[i]).removeAttribute("disabled");
        }
        for (var i = 0; i < nullBooleans.length; i++) {
            document.getElementById('id_' + nullBooleans[i] + '_0').removeAttribute("disabled");
            document.getElementById('id_' + nullBooleans[i] + '_1').removeAttribute("disabled");
            document.getElementById('id_' + nullBooleans[i] + '_2').removeAttribute("disabled");
        }
    }
    // For dead trees, only some selects are disabled
    if (vitaCode == 'm' || vitaCode == 'z') {
        var m_selects = ['layer', 'rank', 'crown_form', 'crown_length'];
        for (var i = 0; i < m_selects.length; i++) {
            document.getElementById('id_' + m_selects[i]).value = '';
            document.getElementById('id_' + m_selects[i]).setAttribute("disabled", "disabled");
        }
        // Set value from previous obs if available
        var dbh = document.getElementById('id_dbh');
        if (dbh.value == '') {
            dbh.value = dbh.dataset.previousValue;
        }
    }
};

// Class method, not instance related (run only once per app load)
TreeForm.initHandlers = function () {
    var dbhCheck = function(dbhInput) {
        if (dbhInput.dataset.previousValue && parseInt(dbhInput.value) <= dbhInput.dataset.previousValue) {
            document.getElementById('dbh_warning').style.display = 'block';
        } else {
            document.getElementById('dbh_warning').style.display = 'none';
        }
    };
    var showTreeOnMap = function(name, val) {
        var tree = formSet.treeForm.tree;
        if (tree) {
            tree.setProperty(name, val);
            tree.plotobs.showTrees(map);
            map.highlightPoint(tree.getCoords(true));
        }
    };

    document.getElementById('id_distance').addEventListener('input', (ev) => {
        showTreeOnMap('distance', ev.target.value);
    });
    document.getElementById('id_azimuth').addEventListener('input', (ev) => {
        var val = TreeForm.convertAzimuth(ev.target.value);
        showTreeOnMap('azimuth', val);
    });
    document.getElementById('id_dbh').addEventListener('input', (ev) => {
        dbhCheck(ev.target);
    });
    document.getElementById('id_perim').addEventListener('input', (ev) => {
        var diam = Math.round(ev.target.value / Math.PI);
        var dbhInput = document.getElementById('id_dbh');
        dbhInput.value = diam;
        dbhCheck(dbhInput);
    });
    document.getElementById('id_vita').addEventListener('change', (ev) => {
        TreeForm.checkStemHeight();
        TreeForm.setValuesFromVita();
    });
    document.getElementById('id_stem').addEventListener('change', (ev) => {
        TreeForm.checkStemHeight();
    });
    document.getElementById('id_stem_height').addEventListener('change', (ev) => {
        TreeForm.checkStemHeight();
    });
};

TreeForm.convertAzimuth = function(value) {
    if (angleUnit == 'gon') return value;
    return Math.round(value / 0.9);
};


/* Set of input forms */
var FormSet = function () {
    this.forms = document.querySelectorAll('form.input-form');
    this.treeForm = null;
};

FormSet.prototype.initHandlers = function () {
    // Form submit buttons
    var self = this;
    for (var i = 0; i < this.forms.length; i++) {
        this.forms[i].addEventListener('submit', (ev) => {
            ev.preventDefault();
            const form = ev.target;
            if (self.validateForm(form)) {
                if (form.dataset.nextform == "newtree") {
                    showStep(NEW_TREE_STEP);
                } else {
                    showStep(getNextStep());
                }
            }
        });
    }
    // When clicking the new tree or weiter buttons, set hint on the form about
    // which button was clicked.
    var ntButtons = document.querySelectorAll('button.newtree');
    for (var i = 0; i < ntButtons.length; i++) {
        ntButtons[i].addEventListener('click', (ev) => {
            ev.target.closest('form').dataset.nextform = "newtree";
        });
    }
    var wButtons = document.querySelectorAll('button.weiter');
    for (var i = 0; i < wButtons.length; i++) {
        wButtons[i].addEventListener('click', (ev) => {
            ev.target.closest('form').dataset.nextform = "nextstep";
        });
    }
    // Button to keep current GPS coordinates in form inputs
    document.getElementById('keepthis').addEventListener('click', (ev) => {
        ev.preventDefault();
        document.getElementById('id_exact_long').value = document.getElementById('currentLong').innerHTML;
        document.getElementById('id_exact_lat').value = document.getElementById('currentLat').innerHTML;
        checkLonLatDifference();
    });
};

FormSet.prototype.hideAll = function () {
    for (var i = 0; i < this.forms.length; i++) {
        this.forms[i].style.display = 'none';
    }
    document.getElementById('allsteps').style.display = 'none';
    map.unHighlight();
};

FormSet.prototype.startInput = function (obs) {
    // Show start screen for a new observation
    document.getElementById('obs-data').style.display = 'none';
    var divHeight = document.getElementById('infos-div').clientHeight -
                      totalHeight('inventory-title') -
                      totalHeight('year-buttons') - 25;
    document.getElementById('obs-input').setAttribute("style", "height: " + divHeight + "px");
    document.getElementById('obs-input').style.display = 'block';
    if (obs) {
        // We have a partially-filled PlotObs
        this.initForms(obs);
    } else {
        document.getElementById('step0').style.display = 'block';
    }
};

/*
 * Initialize plot obs input forms and show first or 'fillStep' form.
 */
FormSet.prototype.initForms = function (obs) {
    // Reset all input forms
    for (var i=0; i < this.forms.length; i++) {
        this.resetForm(this.forms[i]);
    }
    var is_new = (obs === null);
    if (is_new) {
        obs = currentPlot.createObs(thisYear);
    }
    obs.showTrees(map);
    obs.setUpStepSelect();
    // Set inventory team
    db.get('current-state').then(function (data) {
        document.getElementById('id_inv_team').value = data.inventory;
    });
    // Set initial values for non-tree form widgets
    var previousObs = currentPlot.previousObs(thisYear);
    // The first form has mixed plot and plotobs properties
    // currentPlot wins for some props like Entwicklungstufe, Mischungsgrad and Schlussgrad
    if (previousObs) {
        var inheritedProps = Object.assign(
            {}, previousObs.properties(), currentPlot.properties
        );
    } else {
        var inheritedProps = Object.assign({}, currentPlot.properties);
    }
    delete inheritedProps.remarks;
    for (var step = 1; step <= PLOTOBS_FORM_STEPS; step++) {
        var form = document.getElementById('step' + step);
        if (obs.fillStep <= step) {
            setFormDefaultFromProperties(form, inheritedProps);
        } else {
            setFormDefaultFromProperties(form, obs.properties());
        }
    }
    syncSlopeAndRadius(document.getElementById('id_slope').value);

    // Insert previous remarks above the remarks form input
    var form4Body = document.getElementById('previous-remarks');
    form4Body.innerHTML = '';
    while (previousObs !== null) {
        var remarks = previousObs.properties().remarks;
        if (remarks !== undefined && remarks.length > 0) {
            var row = form4Body.insertRow(0);
            var th = document.createElement('th');
            th.appendChild(document.createTextNode(previousObs.year + ':'));
            row.appendChild(th);
            var cell2 = row.insertCell(1);
            cell2.appendChild(document.createTextNode(remarks));
        }
        previousObs = currentPlot.previousObs(previousObs.year);
    }

    showStep(obs.fillStep);
};

FormSet.prototype.resetForm = function (form) {
    for (var i = 0; i < form.elements.length; i++) {
        if (['submit', 'button'].indexOf(form.elements[i].type) >= 0 ||
            form.elements[i].type == 'hidden') continue;
        if (form.elements[i].type == 'radio'){
            form.elements[i].checked = form.elements[i].value == '';
        } else if (form.elements[i].type == 'select-one') {
            form.elements[i].value = "";
            // Find any default value
            for (var j = 0; j < form.elements[i].options.length; j++) {
                if (j > 0 && form.elements[i].options[j].defaultSelected) {
                    form.elements[i].value = form.elements[i].options[j].value;
                    break;
                }
            }
        }
        else {
            form.elements[i].value = "";
        }
    }
};

FormSet.prototype.validateForm = function (form) {
    if (form === null) form = this.getActive();
    if (form.id === "stepfinal") return true; // Nothing to validate here

    var obs = currentPlot.obs[thisYear];
    var currentStep = parseInt(form.dataset.step);
    // Get form target, PlotObs or TreeObs
    if (currentStep <= PLOTOBS_FORM_STEPS)  {
        var target = obs;
    } else {
        var treeIdx = currentStep - PLOTOBS_FORM_STEPS - 1;
        if (treeIdx >= obs.trees.length) {
            target = obs.addTreeObs(null);
        } else {
            target = obs.trees[treeIdx];
        }
    }
    // Set form values to either PlotObs or TreeObs
    for (var i = 0; i < form.elements.length; i++) {
        if (!form.elements[i].name.length) continue;
        var value = form.elements[i].value;
        if (form.elements[i].name == 'azimuth') {
            value = TreeForm.convertAzimuth(value);
        }
        if (form.elements[i].tagName == "SELECT") {
            if (value != '') {
                value = [value, form.elements[i][form.elements[i].selectedIndex].textContent];
            } else {
                value = [value, value];
            }
        } else if (form.elements[i].type == 'radio') {
            if (!form.elements[i].checked) continue;
            value = [form.elements[i].value, form.elements[i].nextElementSibling.textContent];
        }
        target.setProperty(form.elements[i].name, value);
    }
    // Checking validity after saving values, so input are not lost even if validation fails
    var valid = form.reportValidity();
    if (!valid) return false;

    // Display the tree on the map with proper icon
    if (target !== obs) {
        target.validated = true;
        target.showOnMap(map, 'treeLayer');
    }
    if ((currentStep + 1) > obs.fillStep) {
        obs.fillStep = currentStep + 1;
    }
    obs.saveLocally();
    map.unHighlight();
    return true;
};

FormSet.prototype.getActive = function () {
    for (var i=0; i < this.forms.length; i++) {
        if (this.forms[i].style.display == 'block') return this.forms[i];
    }
    return null;
};

/*
 * Number widget to reflect the number of unsynchronised PlotObs.
 */
var UnsyncCounter = function () {
    this.count = 0;
    this.div = document.getElementById('unsync-counter');
    var self = this;
    db.get("new-plotobs").then(doc => {
        self.setCount(doc.newIDs.length);
    }).catch(err => {
        self.setCount(0);
    });
    // Allow force-resync of all local ksp data
    this.div.addEventListener('dblclick', (ev) => {
        if (confirm("Möchten sie wirklich alle lokalen Daten neu synchronisieren?")) {
            var promises = [];
            db.allDocs({include_docs: true, startkey: 'plotobs'}).then(result => {
                for (var i=0; i<result.rows.length; i++) {
                    if (result.rows[i].id.indexOf('-' + thisYear) >=0) {
                        var po = new PlotObs(null, result.rows[i].doc);
                        if (po.fillStep >= 100) promises.push(po.sendToServer());
                    }
                }
                Promise.all(promises).then(() => {
                    alert(promises.length + " Punkte wurden neu synchronisiert.");
                });
            });
        }
    });
};

UnsyncCounter.prototype.setCount = function (count) {
    if (count < 0) count = 0;
    this.count = count;
    this.div.innerHTML = this.count;
    if (this.count > 0) {
        this.div.classList.add("nonempty");
    } else {
        this.div.classList.remove("nonempty");
    }
};

UnsyncCounter.prototype.increment = function () { this.setCount(this.count + 1); }
UnsyncCounter.prototype.decrement = function () { this.setCount(this.count - 1); }

var map = new Map();
var formSet = new FormSet();

function resetInfos() {
    document.getElementById('plot-data').style.display = 'none';
    document.getElementById('year-buttons').innerHTML = "";
    document.getElementById('obs-data').style.display = 'none';
    document.getElementById('obs-input').style.display = 'none';
    formSet.hideAll();
    map.resetLayers();
    currentPlot = null;
}

function getNextStep() {
    // The next step is the next allstepsselect item
    // or the next unvalidated tree.
    var stepSelect = document.getElementById('allstepsselect');
    var nextOption = stepSelect.options[stepSelect.selectedIndex + 1]
    var step = nextOption.value;
    if (step == FINAL_STEP) {
        // Check all existing trees are validated
        var trees = currentPlot.obs[thisYear].trees;
        for (var j = 0; j < trees.length; j++) {
            if (!trees[j].validated) {
                step = PLOTOBS_FORM_STEPS + j + 1;
                break;
            }
        }
    }
    return step;
}

function goToStep(step) {
    // Validate currently-visible form. We don't really care if the form
    // pass validation or not. If not, the values are not saved and the tree
    // not marked as validated.
    if (step > PLOTOBS_FORM_STEPS && currentPlot.obs[thisYear].fillStep <= PLOTOBS_FORM_STEPS) {
        console.log(currentPlot.obs[thisYear].fillStep);
        // Too soon to display tree steps
        return;
    }
    formSet.validateForm(null);
    showStep(step);
}

function showStep(step) {
    var obs = currentPlot.obs[thisYear];
    formSet.hideAll();
    // Select and display the form matching step.
    if (step == FINAL_STEP) {
        var form = document.getElementById('stepfinal');
    } else if (step <= PLOTOBS_FORM_STEPS) {
        var form = document.getElementById('step' + step);
    } else {
        var form = document.getElementById('steptree');
    }
    form.style.display = 'block';
    var stepSelect = document.getElementById('allstepsselect');
    stepSelect.value = step.toString();
    if (step > 0) {
        document.getElementById('allsteps').style.display = 'block';
    }
    // Un-disable any select option before or matching this step
    for (var i=0; i < stepSelect.options.length; i++) {
        if (parseInt(stepSelect.options[i].value) < step.toString()) {
            stepSelect.options[i].removeAttribute("disabled");
        }
        else if (step > PLOTOBS_FORM_STEPS && stepSelect.options[i].text.indexOf('Baum') >= 0) {
            // At tree stages, enable all trees in one step
            stepSelect.options[i].removeAttribute("disabled");
        }
    }
    if (step == FINAL_STEP) {
        stepSelect.options[stepSelect.options.length - 1].removeAttribute("disabled");
        // Populate a summary of the trees
        var treeTable = document.getElementById('trees_summary');
        var newTbody = document.createElement('tbody');
        obs.trees.forEach((tree, index) => {
            let props = tree.properties;
            let klass = props.tree == '' ? 'new' : '';
            let dbh = props.dbh;
            if (props.prev_dbh) {
                let diff = props.dbh - props.prev_dbh;
                let sign = diff >= 0 ? '+' : '';
                dbh = props.dbh + ' (' + sign + diff + ')';
            }
            insertTableRow(
                newTbody, '#tree_row', klass,
                [props.nr, props.species[1], props.distance.toString().replace('.00', ''), props.azimuth, dbh, props.vita[1]]
            );
            // Show the delete icon for new trees.
            if (klass == 'new') {
                let img = newTbody.lastElementChild.querySelector("img");
                img.addEventListener('click', (ev) => {
                    if (confirm("Möchtest du diesen Baum wirklich löschen?")) {
                        newTbody.removeChild(ev.target.closest('tr'));
                        obs.trees.splice(index, 1);
                    }
                });
            }
        });
        var oldTbody = treeTable.children[1];
        oldTbody.parentNode.replaceChild(newTbody, oldTbody);
        Array.from(document.getElementsByClassName('treenumb')).forEach(
            numb => { numb.classList.remove('hidden'); }
        );
        // Handle exact coords warning
        const hasExactCoords = (
            document.getElementById('id_exact_long').value.length > 0 && document.getElementById('id_exact_lat').value.length > 0
        );
        document.getElementById('coords_missing_warning').style.display = hasExactCoords ? 'none' : 'block';
    }

    if (step > PLOTOBS_FORM_STEPS && step != FINAL_STEP) {
        // A tree form
        var treeIdx = step - PLOTOBS_FORM_STEPS - 1;
        if (treeIdx < obs.trees.length) {
            // An existing tree (copied from previous obs or recently added).
            formSet.treeForm = new TreeForm(obs.trees[treeIdx], step, false);
        } else {
            // New tree
            var newStep = PLOTOBS_FORM_STEPS + obs.trees.length + 1
            var previousObs = obs.plot.previousObs(thisYear);
            formSet.treeForm = new TreeForm(null, newStep, (previousObs === null));
        }
    }
    // Must come after values have been populated, as locks depend on values.
    resetLockInputs();
}

function setFormDefaultFromProperties(form, props) {
    for (var i = 0; i < form.elements.length; i++) {
        // Don't touch unnamed inputs or inputs from formset management forms
        if (!form.elements[i].name.length || form.elements[i].name.indexOf('_FORMS') >= 0) continue;
        if (form.elements[i].tagName == 'SELECT') {
            if (props[form.elements[i].name]) {
                form.elements[i].value = props[form.elements[i].name][0];
            } else {
                for (var j = 0; j < form.elements[i].options.length; j++) {
                    if (j > 0 && form.elements[i].options[j].defaultSelected) {
                        form.elements[i].value = form.elements[i].options[j].value;
                        break;
                    }
                }
            }
        } else {
            if (props[form.elements[i].name]) {
                if (form.elements[i].type == 'radio') {
                    form.elements[i].checked = form.elements[i].value == props[form.elements[i].name][0];
                } else {
                    form.elements[i].value = props[form.elements[i].name];
                }
            } else {
                if (form.elements[i].type == 'radio' && form.elements[i].defaultChecked === true) {
                    form.elements[i].checked = true;
                }
            }
        }
    }
}

function tabClick(ev) {
    var tabChildren = document.querySelectorAll('.tab-child');
    for (var j = 0; j < tabChildren.length; j++) {
        tabChildren[j].style.display = "none";
    }
    document.getElementById(this.dataset.target).style.display = "block";
    var tabDiv = document.getElementById('obs-tabs');
    tabDiv.querySelector("li.active").className = "";
    this.className = "active";
    // Show tree numbers on the map only when the active tab is "trees"
    var treenumbs = document.getElementsByClassName('treenumb');
    if (this.dataset.target == 'obs-infos-trees') {
        for(var i = 0; i < treenumbs.length; i++) { treenumbs[i].classList.remove('hidden'); }
    } else {
        map.unHighlight();
        for(var i = 0; i < treenumbs.length; i++) { treenumbs[i].classList.add('hidden'); }
    }
}

function saveData(docId, data) {
    // Save data in local database, replacing any existing occurrences
    db.get(docId).then(doc => {
        return db.remove(doc);
    }).then(result => {
        data._id = docId;
        return db.put(data);
    }).catch(err => {
        if (err.name === 'not_found') {
            data._id = docId;
            return db.put(data);
        } else throw err;
    });
}

/*
 * Load inventory data: inventory boundaries, plots, wms layers.
 * If forCache is true, also load every PlotObs URL to have all data in cache.
 */
function loadInventoryData(option, forCache) {
    return loadJSON(option.dataset.url).then((inventory) => {
        return new Promise(function(resolve, reject) {
            resolve(loadJSON(inventory.properties.plotsURL));
            // This can go on asynchronously
            if (!forCache)  {
                map.showInventory(inventory, true);
                Object.keys(inventory.properties.layerURLs).forEach((k, i) => {
                    map.loadLayer(k, inventory.properties.layerURLs[k]);
                });
            }
        });
    }).then(features => {
        // Fetch locally saved PlotObs, if any
        return new Promise(function(resolve, reject) {
            db.get('new-plotobs').then((doc) => { resolve([features, doc.newIDs]); }
            ).catch((err) => { resolve([features, []]); });
        });
    }).then(result => {
        var features = result[0],
            newIds = result[1],
            plotIdsWithNew = {}; // Array storing locally-saved PlotObs
        newIds.map(obj => {
            plotIdsWithNew[parseInt(obj.split('-')[1])] = parseInt(obj.split('-')[2]);
        });
        for (var i = 0; i < features.features.length; i ++) {
            var plot = new Plot(features.features[i]);
            if (plotIdsWithNew.hasOwnProperty(plot.id)) {
                plot.getObs(plotIdsWithNew[plot.id]).then(po => {
                    if (!forCache) po.plot.showOnMap(map);
                });
            } else if (!forCache) plot.showOnMap(map);
            if (forCache) {
                // Call every PlotObs URL to get them in the cache
                for (var j = 0, years = plot.years(); j < years.length; j++) {
                    plot.getObs(years[j]);
                }
            }
        }
    });
}

function unlockInput(ev) {
    if (this.src.indexOf('unlocked') !== -1) return;
    toggleImg(this);
    var prevElement = this.previousElementSibling;
    while(prevElement !== null) {
        if (prevElement.disabled) {
            prevElement.disabled = false;
            break;
        }
        prevElement = prevElement.previousElementSibling;
    }
}

/*
 * If target inputs have no value set, hide locks, set input disabled to false.
 * If target inputs have values, display lock, reset to lock image, set input
 * disabled to true.
 */
function resetLockInputs() {
    var locks = document.querySelectorAll('img.locked-input');
    for (var i = 0; i < locks.length; i++) {
        var input = locks[i].previousElementSibling;
        if (input.tagName == 'SPAN') input = input.previousElementSibling;
        if (input.value != '') {
            if (locks[i].src.indexOf('unlocked') >= 0) toggleImg(locks[i]);
            locks[i].style.display = 'inline';
            input.disabled = true;
        } else {
            locks[i].style.display = 'none';
            input.disabled = false;
        }
    }
}

function checkLonLatDifference() {
    // Check and warn if difference between theoretical and real coordinate is too high
    var theo_longVal = currentPlot.properties.coords_2056[0];
    var theo_latVal = currentPlot.properties.coords_2056[1];
    var longVal = document.getElementById('id_exact_long').value;
    var latVal = document.getElementById('id_exact_lat').value;
    document.getElementById('exact_long_warning').style.display = 'none';
    document.getElementById('exact_lat_warning').style.display = 'none';
    if (longVal != '' && Math.abs(longVal - theo_longVal) > 20) {
        document.getElementById('exact_long_warning').innerHTML = 'Der Unterschied zwischen theoretischer und realer Koordinate ist: ' + Math.floor(Math.abs(longVal - theo_longVal)) + 'm';
        document.getElementById('exact_long_warning').style.display = 'block';
    }
    if (latVal != '' && Math.abs(latVal - theo_latVal) > 20) {
        document.getElementById('exact_lat_warning').innerHTML = 'Der Unterschied zwischen theoretischer und realer Koordinate ist: ' + Math.floor(Math.abs(latVal - theo_latVal)) + 'm';
        document.getElementById('exact_lat_warning').style.display = 'block';
    }
}

function syncSlopeAndRadius(slopeVal) {
    document.getElementById('plot-slope').innerHTML = slopeVal;
    // Adapt radius to new slope
    var radius = 9.77;
    if (slopeVal > 20) {
        let rounded = parseInt(Math.round(slopeVal/5.0)*5);
        radius = slopeMapping[rounded];
    }
    document.getElementById('plot-radius').innerHTML = radius + 'm';
    // Adapt circle radius on the map
    map.vLayers.plotCircle.setRadius(radius);
    currentPlot.properties.radius = radius;
}

/*
 * After the user choose the inventory, restore the map state to the previous
 * position (if the inventory did not change) or to the inventory overview.
 */
function setInitialState(inventoryOption) {
    // Read state stored in the db (current municipality) and restore it
    db.get('current-state').then(data => {
        return data;
    }).catch(err => {
        if (err.name === 'not_found') {
            console.log("No current-state existing document, creating a new one.");
            var newState = {_id: 'current-state'};
            var newProm = db.put(newState).then(res => {
                return {_id: res.id, _rev: res.rev};
            }).catch(err => {
                console.log("Unable to save a new current-state object: " + err);
            });
            return newProm;
        } else throw err;
    }).then(currentState => {
        document.getElementById('inventory-title').innerHTML = inventoryOption.text;
        if (currentState.inventory && currentState.inventory == inventoryOption.value) {
            loadInventoryData(inventoryOption, false);
            // We are still in the same inventory, just restore the state
        } else if (inventoryOption.value) {
            currentState.inventory = inventoryOption.value;
            db.put(currentState).then(() => {
                // set a new inventory overview
                loadInventoryData(inventoryOption, false);
            }).catch(err => {
                console.log("Unable to save current-state with new inventory id: " + err);
            });
        } else {
            map.map.setView(CENTER_INITIAL, ZOOM_INITIAL);
        }
    });
}

function checkOfflineData() {
    // If unsynced content, propose to sync
    db.get('new-plotobs').then(doc => {
        if (doc.newIDs.length == 0) return;
        var promises = [];
        // Get all locally saved ids
        for (var i=0; i < doc.newIDs.length; i++) {
            var docId = doc.newIDs[i];
            var p = db.get(docId).then(podata => {
                return new PlotObs(null, podata);
            }).catch(err => {
                console.log("Unable to retrieve local PlotObs with id " + docId);
            });
            promises.push(p);
        }
        Promise.all(promises).then(pos => {
            pos = pos.filter(po => po !== undefined); // undefined in case of errors
            if (!pos.length) return;
            var finishedObs = pos.filter(po => po.fillStep >= 100);
            if (!finishedObs.length || !confirm("Möchten sie jetzt die neue offline Daten synchronisieren?")) return;
            var promises2 = [];
            for (var i=0; i < finishedObs.length; i++) {
                var p = finishedObs[i].sendToServer();
                promises2.push(p);
            }
            Promise.all(promises2).then(() => {
                alert("Die Daten sind jetzt auf dem Server gespeichert.");
            });
        });
    }).catch(err => {
        // A not_found indicates we have no unsynced content
        if (err.name !== 'not_found') {
            console.log(err);
        }
    });
}

function resizeObsDivs() {
    document.getElementById('obs-data').style.display = 'block';
    var tabChildHeight = document.getElementById('infos-div').clientHeight -
                         totalHeight('inventory-title') -
                         totalHeight('year-buttons') -
                         totalHeight('obs-tabs') - 25;
    var tabChildren = document.querySelectorAll('.tab-child');
    for (var j = 0; j < tabChildren.length; j++) {
        tabChildren[j].setAttribute("style", "height: " + tabChildHeight + "px");
    }
    document.getElementById('obs-infos-general').style.display = 'block';
}

document.addEventListener("DOMContentLoaded", (event) => {
    // Tab handling
    var tabs = document.querySelectorAll('ul.tabs li');
    for (var i = 0; i < tabs.length; i++) {
        tabs[i].addEventListener('click', tabClick);
    }
    unsyncCounter = new UnsyncCounter();
    // Zurück button
    document.getElementById('gemeinde-back').addEventListener('click', (ev) => {
        resetInfos();
        map.resetToOverview();
    });
    // Start/End Aufnahme buttons
    document.getElementById('input-start').addEventListener('click', (ev) => {
        document.getElementById('step0').style.display = 'none';
        formSet.initForms(null);
    });
    document.getElementById('input-end').addEventListener('click', (ev) => {
        // If the current form is filled, validate it
        var form = formSet.getActive();
        if (!isBlank(form)) {
            var form_valid = formSet.validateForm(form);
            if (!form_valid) return;
        }
        formSet.hideAll();
        currentPlot.obs[thisYear].finished();
        currentPlot.obs[thisYear].showDetails();
    });

    document.getElementById('canceltree').addEventListener('click', (ev) => {
        // Cancelling a new tree form.
        ev.preventDefault();
        if (formSet.treeForm.tree) {
            // Remove from obs.trees if it was added
            var treeArray = formSet.treeForm.tree.plotobs.trees
            var index = treeArray.indexOf(formSet.treeForm.tree);
            if (index > -1) {
                treeArray.splice(index, 1);
            }
        }
        // Remove allstepselect entry
        var stepSelect = document.getElementById('allstepsselect');
        var opt = stepSelect.options[stepSelect.selectedIndex];
        stepSelect.selectedIndex = stepSelect.selectedIndex - 1;
        stepSelect.removeChild(opt);
        showStep(getNextStep());
    });

    // Select widget giving access to steps
    document.getElementById('allstepsselect').addEventListener('change', (ev) => {
        goToStep(ev.target.value);
    });

    // Input unlock buttons
    var locks = document.querySelectorAll('img.locked-input');
    for (var i = 0; i < locks.length; i++) {
        locks[i].addEventListener('click', unlockInput);
    }
    // Slope value on the left synced with the slope input
    document.getElementById('id_slope').addEventListener('change', (ev) => {
        syncSlopeAndRadius(ev.target.value);
    });

    TreeForm.initHandlers();
    formSet.initHandlers();

    document.getElementById('sync-unsync').addEventListener('click', (ev) => {
        if (!confirm("Möchten sie jetzt auf der Karte sichtbaren Daten lokal speichern?")) return;
        var inventorySelect = document.getElementById('inventory-choice'),
            inventoryOption = inventorySelect[inventorySelect.selectedIndex];
        loadInventoryData(inventoryOption, true)
        map.storeOffline();
    });

    function goingOnline(ev) {
        toggleImg('network-status');
        document.getElementById('sync-unsync').style.display = "block";
        checkOfflineData();
    }

    function goingOffline(ev) {
        toggleImg('network-status');
        document.getElementById('sync-unsync').style.display = "none";
        //db.allDocs({startkey: 'gemeinde-', endkey: 'gemeinde-zzz'}).then (function (result) {
            // TODO: disable municipalities without offline content in the select list
        //});
    }

    if (!window.navigator.onLine) toggleImg('network-status');
    window.addEventListener('online',  goingOnline);
    window.addEventListener('offline', goingOffline);

    map.attachHandlers();
    // Base layer switcher
    document.getElementById('baselayer').addEventListener('click', (ev) => {
        toggleImg('baselayer');
        map.switchBaseLayers();
    });
    document.getElementById('tocurrent').addEventListener('click', (ev) => {
        map.toCurrentPosition();
    });
    document.getElementById('toinventory').addEventListener('click', (ev) => {
        map.map.fitBounds(map.inventoryLayer.getBounds());
    });

    // Show warning when forest edge factor is lower than 0.6
    document.getElementById('id_forest_edgef').addEventListener('input', (ev) => {
        if (parseFloat(ev.target.value) < 0.6) {
            document.getElementById('forest_edgef_warning').style.display = 'block';
        } else {
            document.getElementById('forest_edgef_warning').style.display = 'none';
        }
    });
    document.getElementById('id_exact_long').addEventListener('input', checkLonLatDifference);
    document.getElementById('id_exact_lat').addEventListener('input', checkLonLatDifference);

    // Inventory selection form at application load
    document.getElementById('inventory-form').addEventListener('submit', (ev) => {
        ev.preventDefault();
        const form = ev.target;
        if (form.elements[0].selectedIndex < 1) {
            alert("Wählen Sie bitte eine Aufnahme");
            return;
        }
        document.getElementById('inventory-choice-div').style.display = 'none';
        angleUnit = document.querySelector('input[name="angleunit"]:checked').value;
        document.cookie = "angleunit=" + angleUnit;
        setInitialState(form.elements[0][form.elements[0].selectedIndex]);
    });
    var versionDiv = document.getElementById('appversion');
    versionDiv.innerHTML = 'v.' + AppVersion;
    versionDiv.addEventListener('dblclick', (ev) => {
        window.location.reload(true);
    });
    window.addEventListener('resize', (ev) => {
        if (document.getElementById('obs-data').style.display == 'block') {
            resizeObsDivs();
        }
    }, true);
    checkOfflineData();
});

// Debug function to show cache keys
function show_cache_keys() {
    caches.open('kspmobile-v1').then(cache => {
        cache.keys().then(keys => {
             for (var i=0; i < keys.length; i++) {
                 console.log(keys[i].url);
             }
        });
    });
}

// Debug function to clear locally saved data
function clear_local_data() {
    db.get('new-plotobs').catch(err => {
        return;
    }).then(doc => {
        doc.newIDs = [];
        return db.put(doc);
    });
    unsyncCounter.setCount(0);
}
