import datetime
import time
from contextlib import contextmanager
from datetime import date
from unittest import skipUnless

from django.contrib.auth.models import User
from django.contrib.gis.geos import Point, MultiPolygon
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.core import mail
from django.test import TestCase, override_settings

import geckodriver_autoinstaller
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.chrome.webdriver import WebDriver as ChromeDriver
from selenium.webdriver.firefox.webdriver import FirefoxProfile, WebDriver as FirefoxDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait

from gemeinde.models import Gemeinde
from mobile.forms import PlotForm, POForm
from observation.models import *

try:
    import NetworkManager
    HAS_NETWORK_MANAGER = True
except ImportError:
    # python-networkmanager not available
    HAS_NETWORK_MANAGER = False

this_year = date.today().year


@contextmanager
def disable_connection(driver):
    """
    If we find a way to toggle Chrome Offline mode, we should replace this
    function by that capability.
    """
    if isinstance(driver, ChromeDriver):
        driver.set_network_conditions(
            offline=True, latency=5, download_throughput=500 * 1024, upload_throughput=500 * 1024
        )
        try:
            yield
        finally:
            driver.set_network_conditions(
                offline=False, latency=5, download_throughput=500 * 1024, upload_throughput=500 * 1024
            )
        time.sleep(0.5)
        return

    def get_active_device():
        for dev in NetworkManager.NetworkManager.GetDevices():
            if dev.State == NetworkManager.NM_DEVICE_STATE_ACTIVATED:
                return dev

    dev = get_active_device()
    conn = get_active_device().ActiveConnection.Connection
    dev.Disconnect()
    time.sleep(0.5)
    try:
        yield
    finally:
        NetworkManager.NetworkManager.ActivateConnection(conn, dev, "/")
    time.sleep(0.5)


class ProjectDataMixin:
    databases = ['default', 'afw']

    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(username='user', password='secret')
        plot_point = Point(2629800, 1257500, srid=2056)
        gemeinde = Gemeinde.objects.get(name='Böckten')
        cls.inventory = Inventory.objects.create(
            name="Test inventory",
            team="Super Team",
            municipality=gemeinde,
            period=2,
            default_density=20000,
            inv_from=datetime.date(year=date.today().year, month=1, day=1),
            inv_to=datetime.date(year=date.today().year, month=12, day=31),
        )
        plot = Plot.objects.create(
            nr=1, sealevel=263, slope=70, exposition='N',
            phytosoc=Phytosoc.objects.get(pk=74),
            the_geom=plot_point,
        )
        plotobs = PlotObs.objects.create(
            plot=plot, year=2013, inv_team=cls.inventory, area=1, abt=33, evaluation_unit='33',
            forest_clearing=False, subsector='VA', forest_edgef='1.0', gwl=5147.27,
            municipality=gemeinde,
            owner_type=OwnerType.objects.get(pk=4),
            sector=Sector.objects.get(pk=2),
            forest_form=ForestForm.objects.get(pk=4),
            acidity=Acidity.objects.get(pk=2),
            region=Region.objects.get(pk=1),
            regen_type=RegenType.objects.get(pk=1),
            relief=Relief.objects.get(pk=4),
            soil_compaction=SoilCompaction.objects.get(pk=1),
            stand_forest_mixture=ForestMixture.objects.get(code=4), # 90-100% LbH (Laubholz)
            stand_devel_stage=DevelStage.objects.get(code="1"), # Jungwuchs/Dickung (BHD<12cm)
            stand_structure=StandStructure.objects.get(pk=2),
            stand_crown_closure=CrownClosure.objects.get(code=5), # räumig bis aufgelöst
            gap=Gap.objects.get(pk=1),
        )
        tree1 = Tree.objects.create(
            plot=plot, nr=1, azimuth=29, distance=41, spec=TreeSpecies.objects.get(species="Buche"))
        tree2 = Tree.objects.create(
            plot=plot, nr=2, azimuth=229, distance=92, spec=TreeSpecies.objects.get(species="Spitzahorn"))
        tree3 = Tree.objects.create(
            plot=plot, nr=3, azimuth=0, distance=0, spec=TreeSpecies.objects.get(species="Tanne"))
        TreeObs.objects.create(
            tree=tree1, obs=plotobs, dbh=25, vita=Vita.objects.get(code='e'),
        )
        TreeObs.objects.create(
            tree=tree2, obs=plotobs, dbh=32, vita=Vita.objects.get(code='l'),
        )
        TreeObs.objects.create(
            tree=tree3, obs=plotobs, dbh=15, vita=Vita.objects.get(code='c'),
        )
        fm = ForestMixture.objects.get(code=3)  # 50-90% LbH (Laubholz)
        dev = DevelStage.objects.get(code=13)  # schwaches Stangenholz (BHD 12-20cm)
        cc = CrownClosure.objects.get(code=6)  # gruppiert
        WaldBestandKarte.objects.create(
            geom=plot_point.buffer(10),
            # Different from plot on purpose
            entwicklungstufe=dev, mischungsgrad=fm, schlussgrad=cc,
        )


class MobileSeleniumTests(ProjectDataMixin, StaticLiveServerTestCase):
    fixtures = ('gemeinde.json',) + OBSERVATION_FIXTURES
    _overridden_settings = {'DEBUG': True}  # To obtain any traceback in the terminal!

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        geckodriver_autoinstaller.install()
        #driver = 'chrome'
        driver = 'firefox'
        if driver == 'chrome':
            cls.selenium = ChromeDriver()
        elif driver == 'firefox':
            prf = FirefoxProfile()
            prf.set_preference("geo.prompt.testing", True)
            prf.set_preference("geo.prompt.testing.allow", True)
            cls.selenium = FirefoxDriver(prf)
        cls.selenium.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

    def setUp(self):
        super().setUp()
        self.setUpTestData()

    def login(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/mobile/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('user')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('secret')
        self.selenium.find_element_by_xpath('//input[@value="Anmelden"]').click()

    def go_to_inventory(self, name, degree=False):
        inventory_select = Select(self.selenium.find_element_by_id("inventory-choice"))
        time.sleep(2)  # setting up current geolocation may take time
        inventory_select.select_by_visible_text("Test inventory")
        if (degree):
            self.selenium.find_element_by_xpath('//input[@value="deg"]').click()
        self.selenium.find_element_by_id("inventory-ok").click()
        time.sleep(1)  # getting and displaying plots may take time

    def go_to_plotobs(self):
        for marker in self.selenium.find_elements_by_class_name('leaflet-marker-icon'):
            classes = marker.get_attribute('class').split()
            if 'icon-plot-blue' in classes:
                break
        else:
            self.fail("No marker with icon-plot-blue class")
        marker.click()

    def active_select_options(self, element_id):
        """Return the list of active options of a <select> element"""
        return [
            opt.text
            for opt in Select(self.selenium.find_element_by_id(element_id)).options
            if opt.is_enabled()
        ]

    def new_observation(self):
        # Click on current year button
        self.selenium.find_element_by_class_name('new').click()
        self.selenium.find_element_by_id("input-start").click()
        ####### STEP 1 ########
        self.assertEqual(
            Select(self.selenium.find_element_by_id("id_municipality")).first_selected_option.text,
            "4461 Böckten"
        )
        # Entwicklungstufe, Mischungsgrad and Schlussgrad should have the value of the Bestandeskarte,
        # not from previous observation.
        self.selenium.find_element_by_id("id_stand_devel_stage")
        self.assertEqual(
            Select(self.selenium.find_element_by_id("id_stand_devel_stage")).first_selected_option.text,
            'schwaches Stangenholz (BHD 12-20cm) (13)'
        )
        self.selenium.find_element_by_id("id_stand_forest_mixture")
        self.assertEqual(
            Select(self.selenium.find_element_by_id("id_stand_forest_mixture")).first_selected_option.text,
            '50-90% LbH (Laubholz) (3)'
        )
        self.selenium.find_element_by_id("id_stand_crown_closure")
        self.assertEqual(
            Select(self.selenium.find_element_by_id("id_stand_crown_closure")).first_selected_option.text,
            'gruppiert (6)'
        )
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step1"]//button[@type="submit"]'))
        ).click()
        ####### STEP 2 (Verjüngung) ########
        # 2=Ahorn
        Select(self.selenium.find_element_by_id("id_regenform-2-perc_an")
            ).select_by_visible_text("mittel (2)")
        Select(self.selenium.find_element_by_id("id_regenform-2-fegen")
            ).select_by_visible_text("sichtbar (2)")
        # 3=Eiche
        Select(self.selenium.find_element_by_id("id_regenform-2-perc_auf")
            ).select_by_visible_text("reichlich (3)")
        Select(self.selenium.find_element_by_id("id_regenform-2-verbiss")
            ).select_by_visible_text("stark (3)")

        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step2"]//button[@type="submit"]'))
        ).click()
        ####### STEP 3 ########
        # Freeze current coordinates from GPS
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element((By.ID, "currentLong"), '.')
        )
        self.selenium.find_element_by_id("keepthis").click()
        # Check radio values
        self.assertEqual(
            self.selenium.find_element_by_id("id_exposition_0").get_attribute('value'), '')
        self.assertFalse(self.selenium.find_element_by_id("id_exposition_0").is_selected())
        self.assertEqual(
            self.selenium.find_element_by_id("id_exposition_2").get_attribute('value'), 'N')
        self.assertTrue(self.selenium.find_element_by_id("id_exposition_2").is_selected())
        self.assertFalse(self.selenium.find_element_by_id("id_exposition_1").is_selected())

        self.assertTrue(self.selenium.find_element_by_id("id_relief_4").is_selected())

        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step3"]//button[@type="submit"]'))
        ).click()
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step4"]//button[@type="submit"]'))
        ).click()
        ####### Tree forms (2) #######
        self.assertFalse(self.selenium.find_element_by_id("id_species").is_enabled())
        self.assertFalse(self.selenium.find_element_by_id("id_nr").is_enabled())
        self.assertFalse(self.selenium.find_element_by_id("id_distance").is_enabled())
        self.assertFalse(self.selenium.find_element_by_id("id_azimuth").is_enabled())
        self.assertEqual(self.selenium.find_element_by_id("id_dbh").get_attribute('value'), "")
        self.assertTrue(self.selenium.find_element_by_id("id_stem_height").is_displayed())
        # A warning is displayed when the value is <= than previous obs.
        dbh_el = self.selenium.find_element_by_id("id_dbh")
        dbh_warning_el = self.selenium.find_element_by_id("dbh_warning")
        self.assertFalse(dbh_warning_el.is_displayed())
        dbh_el.send_keys("24")
        self.assertTrue(dbh_warning_el.is_displayed())
        dbh_el.clear()
        dbh_el.send_keys("25")
        self.assertTrue(dbh_warning_el.is_displayed())
        dbh_el.clear()
        dbh_el.send_keys("26")
        self.assertFalse(dbh_warning_el.is_displayed())
        # When choosing the "(c)" vita, dbh is set to 0 and disabled, and selects are set empty and disabled.
        Select(self.selenium.find_element_by_id("id_vita")
            ).select_by_visible_text("Probebaum wurde genutzt (c)")
        self.assertFalse(self.selenium.find_element_by_id("id_dbh").is_enabled())
        self.assertEqual(self.selenium.find_element_by_id("id_dbh").get_attribute('value'), "0")
        for sel_id in (
                'id_layer', 'id_rank', 'id_damage', 'id_damage_cause', 'id_crown_form',
                'id_crown_length', 'id_stem'):
            sel = self.selenium.find_element_by_id(sel_id)
            self.assertFalse(sel.is_enabled(), "%s should not be enabled" % sel_id)
            try:
                self.assertEqual(Select(sel).all_selected_options[0].get_attribute('value'), "")
            except IndexError:
                pass
        self.assertFalse(self.selenium.find_element_by_id("id_woodpecker_0").is_enabled())

        # When back to "(l)", dbh is editable again.
        Select(self.selenium.find_element_by_id("id_vita")
            ).select_by_visible_text("Probebaum lebt (l)")
        self.assertTrue(self.selenium.find_element_by_id("id_dbh").is_enabled())
        Select(self.selenium.find_element_by_id("id_vita")
            ).select_by_visible_text("Probebaum wurde genutzt (c)")
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="steptree"]//button[@type="submit"]'))
        ).click()

        # Second tree form
        # Go back to previous obs (e.g. to see some value) and come back
        self.selenium.find_element_by_id("button-2013").click()
        self.selenium.find_element_by_id("button-%d" % this_year).click()

        self.assertEqual(self.selenium.find_element_by_id("id_dbh").get_attribute('value'), "")
        self.selenium.find_element_by_id("id_dbh").send_keys("38")
        Select(self.selenium.find_element_by_id("id_vita")).select_by_visible_text("Probebaum lebt (l)")
        self.selenium.find_element_by_id("id_woodpecker_1").click()
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="steptree"]//button[@class="newtree"]'))
        ).click()

        ####### New tree form ########
        tree_form = self.selenium.find_element_by_id("steptree")
        self.assertTrue(tree_form.find_element_by_id("id_species").is_enabled())
        # Next number is automatically attributed
        self.assertEqual(tree_form.find_element_by_id("id_nr").get_attribute('value'), "3")
        Select(tree_form.find_element_by_id("id_species")).select_by_visible_text("Tanne")
        tree_form.find_element_by_id("id_distance").send_keys("24")
        tree_form.find_element_by_id("id_azimuth").send_keys("320")
        tree_form.find_element_by_id("id_dbh").send_keys("14")
        Select(tree_form.find_element_by_id("id_vita")).select_by_visible_text(
            "Probebaum ist eingewachsen (e)")
        tree_form.find_element_by_id("id_remarks").send_keys("Bemerkung.")
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="steptree"]//button[@type="submit"]'))
        ).click()

        # It's possible to go back to an already handled tree and see the entered values
        Select(self.selenium.find_element_by_id("allstepsselect")).select_by_visible_text(
            "Schritt 6: Baum nr 2")
        self.assertEqual(
            self.selenium.find_element_by_id("id_dbh").get_attribute('value'), "38")
        self.assertFalse(self.selenium.find_element_by_id("canceltree").is_displayed())
        self.assertNotIn(
            "Probebaum ist eingewachsen (e)", self.active_select_options("id_vita")
        )
        # Going back to a new tree also possible (delete button still visible)
        Select(self.selenium.find_element_by_id("allstepsselect")).select_by_visible_text(
            "neuer Baum")
        self.assertTrue(self.selenium.find_element_by_id("canceltree").is_displayed())
        vita_opts = self.active_select_options("id_vita")
        self.assertIn("Probebaum ist eingewachsen (e)", vita_opts)
        self.assertNotIn("Probebaum ist nicht mehr auffindbar (x)", vita_opts)

        Select(self.selenium.find_element_by_id("allstepsselect")).select_by_visible_text(
            "Endgültige Bestätigung")
        self.selenium.find_element_by_id("input-end").click()

    def test_new_observation_online(self):
        self.login()
        self.go_to_inventory("Test inventory")
        self.go_to_plotobs()
        self.assertEqual(
            self.selenium.find_element_by_xpath('//*[@id="obs-infos-general-table"]/tbody/tr[4]/td[2]').text,
            "4461 Böckten"
        )
        self.new_observation()
        self.assertEqual(
            self.selenium.execute_script("return currentPlot.obs[%s].year;" % this_year),
            this_year
        )
        # Test new PlotObs/Tree/TreeObs/RegenObs presence
        time.sleep(1)  # FIXME: use some other mean to detect saving is done
        plot = Plot.objects.get(nr=1)
        self.assertIsNotNone(plot.point_exact)
        self.assertEqual(plot.plotobs_set.count(), 2)
        po = plot.plotobs_set.get(year=this_year)
        self.assertEqual(po.municipality, Gemeinde.objects.get(name='Böckten'))
        self.assertEqual(po.regenobs_set.count(), 7) # One for each species in RegenObs.OBS_SPECIES
        self.assertEqual(po.treeobs_set.count(), 3)
        self.assertEqual(po.inv_team.name, "Test inventory")
        self.assertEqual(
            sorted(po.treeobs_set.values_list('tree__nr', flat=True)),
            [1, 2, 3]
        )
        tree1, tree2, tree3 = po.treeobs_set.order_by('tree__nr')
        self.assertEqual(tree1.survey_type, SurveyType.objects.get(code='K'))
        self.assertEqual(tree1.vita.code, "c")
        self.assertEqual(tree2.vita.code, "l")
        self.assertEqual(tree3.remarks, "Bemerkung.")
        self.assertEqual(tree3.vita.code, "e")

    def test_new_observation_online_naked_plot(self):
        PlotObs.objects.all().delete()
        self.login()
        self.go_to_inventory("Test inventory")
        self.go_to_plotobs()
        # Fill new minimal data
        self.selenium.find_element_by_class_name('new').click()
        self.selenium.find_element_by_id("input-start").click()
        self.assertEqual(self.selenium.find_element_by_id("plot-hohe").text, '263m')
        Select(self.selenium.find_element_by_id("id_municipality")
            ).select_by_visible_text("4461 Böckten")
        forest_edge = self.selenium.find_element_by_id("id_forest_edgef")
        forest_edge.send_keys("1")
        self.selenium.find_element_by_xpath('//*[@id="step1"]//button[@type="submit"]').click()
        self.selenium.find_element_by_xpath('//*[@id="step2"]//button[@type="submit"]').click()
        self.selenium.find_element_by_xpath('//*[@id="step3"]//button[@type="submit"]').click()
        self.selenium.find_element_by_xpath('//*[@id="step4"]//button[@type="submit"]').click()
        # Enter a new tree
        self.selenium.find_element_by_xpath('//*[@id="stepfinal"]//button[@class="newtree"]').click()
        tree_form = self.selenium.find_element_by_id("steptree")
        self.assertEqual(tree_form.find_element_by_id("id_nr").get_attribute('value'), "1")
        Select(tree_form.find_element_by_id("id_species")).select_by_visible_text("Tanne")
        tree_form.find_element_by_id("id_distance").send_keys("24")
        tree_form.find_element_by_id("id_azimuth").send_keys("320")
        tree_form.find_element_by_id("id_dbh").send_keys("14")
        self.selenium.find_element_by_xpath('//*[@id="steptree"]//button[@type="submit"]').click()
        self.assertTrue(self.selenium.find_element_by_id("coords_missing_warning").is_displayed())
        self.selenium.find_element_by_id("input-end").click()

        time.sleep(1)  # FIXME: use some other mean to detect saving is done
        plot = Plot.objects.get(nr=1)
        self.assertEqual(plot.plotobs_set.count(), 1)

    def test_new_observation_online_small_edge_factor(self):
        """
        With a forest edge factor lower than 0.6, no trees are observed.
        """
        self.login()
        self.go_to_inventory("Test inventory")
        self.go_to_plotobs()
        # Click on current year button
        self.selenium.find_element_by_class_name('new').click()
        self.selenium.find_element_by_id("input-start").click()
        fef_el = self.selenium.find_element_by_id("id_forest_edgef")
        fef_warning_el = self.selenium.find_element_by_id("forest_edgef_warning")
        self.assertFalse(fef_warning_el.is_displayed())
        fef_el.clear()
        fef_el.send_keys("0,5")
        self.assertTrue(fef_warning_el.is_displayed())
        self.selenium.find_element_by_xpath('//*[@id="step1"]//button[@type="submit"]').click()
        self.selenium.find_element_by_xpath('//*[@id="step2"]//button[@type="submit"]').click()
        self.selenium.find_element_by_xpath('//*[@id="step3"]//button[@type="submit"]').click()
        self.selenium.find_element_by_xpath('//*[@id="step4"]//button[@type="submit"]').click()
        self.assertEqual(
            Select(self.selenium.find_element_by_id("allstepsselect")).all_selected_options[0].text,
            'Endgültige Bestätigung'
        )
        self.selenium.find_element_by_id("input-end").click()

        time.sleep(1)  # FIXME: use some other mean to detect saving is done
        plot = Plot.objects.get(nr=1)
        self.assertEqual(plot.plotobs_set.count(), 2)
        po = plot.plotobs_set.get(year=this_year)
        self.assertEqual(po.treeobs_set.count(), 0)
        self.assertEqual(po.forest_edgef, 0.5)

    def test_new_observation_online_azimuth_degrees(self):
        """Untouched azimuth values should be kept even when angle unit is degree."""
        self.login()
        self.go_to_inventory("Test inventory", degree=True)
        self.go_to_plotobs()
        self.selenium.find_element_by_class_name('new').click()
        self.selenium.find_element_by_id("input-start").click()
        WebDriverWait(self.selenium, 4).until(
            EC.visibility_of_element_located((By.XPATH, '//*[@id="step1"]//button[@type="submit"]'))
        ).click()
        self.selenium.find_element_by_xpath('//*[@id="step2"]//button[@type="submit"]').click()
        self.selenium.find_element_by_xpath('//*[@id="step3"]//button[@type="submit"]').click()
        self.selenium.find_element_by_xpath('//*[@id="step4"]//button[@type="submit"]').click()
        self.assertEqual(self.selenium.find_element_by_id("id_azimuth").get_attribute('value'), "26")
        self.selenium.find_element_by_id("id_dbh").send_keys('28')
        self.selenium.find_element_by_xpath('//*[@id="steptree"]//button[@type="submit"]').click()
        self.selenium.find_element_by_id("id_dbh").send_keys('34')
        self.selenium.find_element_by_xpath('//*[@id="steptree"]//button[@type="submit"]').click()
        self.selenium.find_element_by_id("input-end").click()
        time.sleep(1)  # FIXME: use some other mean to detect saving is done
        po = PlotObs.objects.get(year=this_year)
        self.assertEqual(
            list(po.treeobs_set.values_list('tree__azimuth', flat=True)),
            [29, 229]
        )

    @skipUnless(HAS_NETWORK_MANAGER, 'python-networkmanager is needed to connect/disconnect the network')
    def test_new_observation_offline(self):
        # Offline tests are *really* flaky! Chrome seems to change behavior whether debug
        # panel is open or not. Firefox doesn't always trigger online/offline events when
        # network is programmatically changed.
        self.login()
        self.go_to_inventory("Test inventory")
        # Zoom from level 14 to 17 (so as only one level is synced (17 to 18))
        zoom_in = self.selenium.find_element_by_class_name("leaflet-control-zoom-in")
        while True:
            val = self.selenium.execute_script("return map.map.getZoom();")
            if val and val >= 18: # 17
                break
            zoom_in.click()
            time.sleep(0.1)
        self.selenium.find_element_by_id("sync-unsync").click()
        self.selenium.switch_to.alert.accept()
        # Wait for the sync to complete
        WebDriverWait(self.selenium, 20).until(EC.alert_is_present())
        self.assertIn("Daten wurden erfolgreich gespeichert", self.selenium.switch_to.alert.text)
        self.selenium.switch_to.alert.accept()

        with disable_connection(self.selenium):
            self.go_to_plotobs()
            self.assertEqual(
                self.selenium.find_element_by_xpath('//*[@id="obs-infos-general-table"]/tbody/tr[4]/td[2]').text,
                "4461 Böckten"
            )
            self.new_observation()
            self.assertEqual(self.selenium.find_element_by_id("unsync-counter").text, "1")

        # Resync data
        WebDriverWait(self.selenium, 4).until(EC.alert_is_present())
        self.selenium.switch_to.alert.accept()
        # Test results
        WebDriverWait(self.selenium, 4).until(EC.alert_is_present())
        self.selenium.switch_to.alert.accept()
        self.assertEqual(self.selenium.find_element_by_id("unsync-counter").text, "0")
        plot = Plot.objects.get(nr=1)
        self.assertEqual(plot.plotobs_set.count(), 2)
        po = plot.plotobs_set.get(year=date.today().year)
        self.assertEqual(po.municipality, Gemeinde.objects.get(name='Böckten'))
        self.assertEqual(po.treeobs_set.count(), 3)


@override_settings(ADMINS=[('John', 'john@example.com')])
class MobileTests(ProjectDataMixin, TestCase):
    fixtures = ('gemeinde.json',) + OBSERVATION_FIXTURES

    def setUp(self):
        super().setUp()
        self.sync_data = {
            'type': 'FeatureCollection',
            'plot': Plot.objects.get(nr=1).pk,
            'features': [{
                'type': 'Feature',
                'geometry': {'type': 'Point', 'coordinates': [7.6962502809437385, 47.53184686184221]},
                'properties': {
                    # Plot-related
                    'exposition': ['S', 'Süden'],
                    'slope': 54,
                    # Plotobs-related
                    'municipality': ['6', '4302 Augst'],
                    'inv_team': str(self.inventory.pk),
                    'year': this_year,
                    'remarks': '',
                    'stand_forest_mixture': ['', '---------'],
                    'soil_compaction': ['', '---------'],
                    'regen_type': ['', '---------'],
                    'stand_devel_stage': ['', '---------'],
                    'forest_form': ['', '---------'],
                    'forest_edgef': '1.0',
                    'type': 'center',
                    'stand_crown_closure': ['', '---------'],
                    'relief': ['', '---------'],
                }
            }, {
                'type': 'Feature',
                'geometry': {'type': 'Point', 'coordinates': [7.696274279412243, 47.53188017026505]},
                'properties': {
                    'nr': '1',
                    'species': ['1', 'Buche'],
                    'layer': ['', '---------'],
                    'vita': ['1', 'Baum ist seit der letzten Aufnahme genutzt wurden. (c)'],
                    'distance': '41.00',
                    'rank': ['', '---------'],
                    'azimuth': '29',
                    'crown_form': ['', '---------'],
                    'damage': ['', '---------'],
                    'tree': str(Tree.objects.get(nr=1).pk),
                    'stem': ['', '---------'],
                    'damage_cause': ['', '---------'],
                    'dbh': '0',
                    'crown_length': ['', '---------'],
                },
            }, {
                'type': 'Feature',
                'geometry': {'type': 'Point', 'coordinates': [7.696196430721728, 47.531772120990965]},
                'properties': {
                    'nr': '2',
                    'species': ['3', 'Spitzahorn'],
                    'layer': ['', '---------'],
                    'vita': ['3', 'Baum lebt. (l)'],
                    'distance': '92.00',
                    'rank': ['', '---------'],
                    'azimuth': '229',
                    'crown_form': ['', '---------'],
                    'damage': ['', '---------'],
                    'tree': str(Tree.objects.get(nr=2).pk),
                    'stem': ['', '---------'],
                    'damage_cause': ['', '---------'],
                    'dbh': '32',
                    'crown_length': ['', '---------'],
                    'stem_forked': ['3', 'Nein'],
                    'woodpecker': ['2', 'Ja'],
                },
            }, {
                'properties': {
                    'nr': '3',
                    'species': ['16', 'Tanne'],
                    'layer': ['', '---------'],
                    'vita': ['2', 'Baum ist seit der letzten Aufnahme neu eingewachsen. (e)'],
                    'distance': '24',
                    'rank': ['', '---------'],
                    'azimuth': '320',
                    'crown_form': ['', '---------'],
                    'damage': ['', '---------'],
                    'tree': '',
                    'stem': ['', '---------'],
                    'damage_cause': ['', '---------'],
                    'dbh': '14',
                    'crown_length': ['', '---------'],
                },
                'type': 'Feature',
                'geometry': {'type': 'Point', 'coordinates': [7.6962199123552235, 47.53185357109485]}
            }]
        }
        # RegenObs data (Verjüngung)
        self.sync_data['features'][0]['properties'].update({
            'regenform-INITIAL_FORMS': '0',
            'regenform-TOTAL_FORMS': '7',
        })
        for idx, spec_name in enumerate(RegenObs.OBS_SPECIES):
            ahorn_idx = RegenObs.OBS_SPECIES.index("Ahorn")
            eiche_idx = RegenObs.OBS_SPECIES.index("Eiche")
            self.sync_data['features'][0]['properties'].update({
                # RegenObs-related (non-default values for Ahorn and Eiche species)regenform
                'regenform-%d-id' % idx: '',
                'regenform-%d-spec' % idx: str(TreeSpecies.objects.get(species=spec_name).pk),
                'regenform-%d-perc_an' % idx: ['2', 'mittel (2)'] if idx == ahorn_idx else ['1', '0-1% (1)'],
                'regenform-%d-perc_auf' % idx: ['3', 'reichlich (3)'] if idx == eiche_idx else ['1', '0-1% (1)'],
                'regenform-%d-verbiss' % idx: ['3', 'stark (3)'] if idx == eiche_idx else ['1', 'unbedeutend (1)'],
                'regenform-%d-fegen' % idx: ['2', 'sichtbar (2)'] if idx == ahorn_idx else ['1', 'unbedeutend (1)'],
            })

    def test_forest_edgef_limits(self):
        po = PlotObs.objects.first()
        for wrong_val in ('0.4', '1.1'):
            with self.subTest(value=wrong_val):
                form = POForm(instance=po, data={
                    'municipality': po.municipality.pk,
                    'inv_team': self.inventory.pk,
                    'forest_edgef': wrong_val,
                })
                self.assertFalse(form.is_valid())
        for correct_val in ('0.5', '1.0'):
            with self.subTest(value=correct_val):
                form = POForm(instance=po, data={
                    'municipality': po.municipality.pk,
                    'inv_team': self.inventory.pk,
                    'forest_edgef': correct_val,
                })
                self.assertTrue(form.is_valid())

    def test_plotform_save(self):
        plot = Plot.objects.get(nr=1)
        form = PlotForm(instance=plot, data={
            'exact_long': "2552499.8",
            'exact_lat': "1216577.42",
            'exposition': "N",
            'slope': "70"}
        )
        self.assertTrue(form.is_valid())
        form.save()
        plot.refresh_from_db()
        self.assertEqual(plot.point_exact, Point(2552499.8, 1216577.42, srid=2056))

    def test_sync_view(self):
        # Tree species and azimuth changed:
        self.sync_data['features'][1]['properties']['species'] = ['3', 'Spitzahorn']
        self.sync_data['features'][1]['properties']['azimuth'] = '28'

        self.client.login(username='user', password='secret')
        response = self.client.post(
            reverse("plotobs-sync"), json.dumps(self.sync_data), content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
        # Plot-related fields are updated
        plot = Plot.objects.get(nr=1)
        self.assertEqual(plot.slope, 54)
        # Test new PlotObs
        self.assertEqual(plot.plotobs_set.count(), 2)
        po = plot.plotobs_set.get(year=this_year)
        self.assertEqual(po.municipality, Gemeinde.objects.get(name='Augst'))
        self.assertEqual(po.inv_team, self.inventory)
        self.assertEqual(po.density, self.inventory.default_density)
        # Test TreeObs results
        self.assertEqual(po.treeobs_set.count(), 3)
        treeobs1 = po.treeobs_set.get(tree__nr=1)
        self.assertEqual(treeobs1.vita.code, 'c')
        self.assertEqual(treeobs1.dbh, 0)
        self.assertEqual(treeobs1.tree.spec.species, 'Spitzahorn')
        self.assertEqual(treeobs1.tree.azimuth, 28)
        treeobs2 = po.treeobs_set.get(tree__nr=2)
        self.assertEqual(treeobs2.vita.code, 'l')
        self.assertIs(treeobs2.stem_forked, False)
        self.assertIs(treeobs2.woodpecker, True)

        # RegenObs fields
        self.assertEqual(po.regenobs_set.count(), len(RegenObs.OBS_SPECIES))
        self.assertEqual(po.regenobs_set.get(spec__species='Buche').perc_an.description, 'spärlich')  # default
        self.assertEqual(po.regenobs_set.get(spec__species='Ahorn').perc_an.description, 'mittel')
        self.assertEqual(po.regenobs_set.get(spec__species='Ahorn').fegen.description, 'sichtbar')
        self.assertEqual(po.regenobs_set.get(spec__species='Eiche').perc_auf.description, 'reichlich')
        self.assertEqual(po.regenobs_set.get(spec__species='Eiche').verbiss.description, 'stark')

        # Exposition can be blank
        plot.plotobs_set.all().delete()
        self.sync_data['features'][0]['properties']['exposition'] = ['', '---------']
        response = self.client.post(
            reverse("plotobs-sync"), json.dumps(self.sync_data), content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
        # ...or absent
        del self.sync_data['features'][0]['properties']['exposition']
        response = self.client.post(
            reverse("plotobs-sync"), json.dumps(self.sync_data), content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)

    def test_sync_missing_trees(self):
        """
        Sync data with missing tree informations are refused.
        All non-cut trees from previous observation should have values.
        """
        # Keeping just PlotObs data and one tree
        self.sync_data['features'] = self.sync_data['features'][:2]
        self.client.login(username='user', password='secret')
        response = self.client.post(
            reverse("plotobs-sync"), json.dumps(self.sync_data), content_type="application/json"
        )
        self.assertEqual(response.status_code, 403)
        # No new plotobs created
        plot = Plot.objects.get(nr=1)
        self.assertEqual(plot.plotobs_set.count(), 1)

    def test_sync_already_existing(self):
        """
        If data already exist for this plot and year, overwrite previous data.
        """
        self.client.login(username='user', password='secret')
        response = self.client.post(
            reverse("plotobs-sync"), json.dumps(self.sync_data), content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'')
        self.assertEqual(PlotObs.objects.filter(plot__nr=1).count(), 2)

        # Second time with same plot/year simply overwrite the previous data
        self.sync_data['features'][0]['properties']['slope'] = 44
        response = self.client.post(
            reverse("plotobs-sync"), json.dumps(self.sync_data), content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'')
        self.assertEqual(PlotObs.objects.filter(plot__nr=1).count(), 2)
        self.assertEqual(Plot.objects.get(nr=1).slope, 44)

    def _test_sync_with_plotobs_error(self):
        self.client.login(username='user', password='secret')
        response = self.client.post(
            reverse("plotobs-sync"), json.dumps(self.sync_data), content_type="application/json"
        )
        self.assertEqual(response.status_code, 403)
        plot = Plot.objects.get(nr=1)
        self.assertEqual(plot.plotobs_set.count(), 1)
        return response

    def test_sync_with_plotobs_error(self):
        """
        If the client is sending some unvalid value, do not crash, but give
        feedback and alert site admins.
        """
        self.sync_data['features'][0]['properties']['relief'] = 'H'  # Error: should be a list [pk, text]
        response = self._test_sync_with_plotobs_error()
        self.assertIn(
            "Errors: relief=H: invalid literal for int() with base 10: 'H'",
            response.content.decode()
        )
        # Mail sent to admins with original data
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn("'relief': 'H',", mail.outbox[0].body)

    def test_sync_with_plotobs_integrity_error(self):
        self.sync_data['features'][0]['properties']['forest_edgef'] = '1.1'
        response = self._test_sync_with_plotobs_error()

    def test_sync_with_treeobs_error(self):
        self.sync_data['features'][1]['properties']['dbh'] = 'abc'  # Error: should not be a string
        self.client.login(username='user', password='secret')
        response = self.client.post(
            reverse("plotobs-sync"), json.dumps(self.sync_data), content_type="application/json"
        )
        self.assertEqual(response.status_code, 403)
        self.assertIn(
            "Bitte eine ganze Zahl eingeben",
            response.content.decode()
        )
        # Mail sent to admins with original data
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn("Bitte eine ganze Zahl eingeben", mail.outbox[0].body)

        self.sync_data['features'][1]['properties']['dbh'] = '20'
        self.sync_data['features'][1]['properties']['species'] = ['', '']
        response = self.client.post(
            reverse("plotobs-sync"), json.dumps(self.sync_data), content_type="application/json"
        )
        self.assertEqual(response.status_code, 403)
        self.assertIn(
            "Dieses Feld ist zwingend erforderlich.",
            response.content.decode()
        )

        # Check no plotobs was created
        plot = Plot.objects.get(nr=1)
        self.assertEqual(plot.plotobs_set.count(), 1)
