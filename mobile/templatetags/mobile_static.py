from django import template
from django.templatetags.static import StaticNode as DjangoStaticNode, do_static

register = template.Library()


class StaticNode(DjangoStaticNode):
    @classmethod
    def handle_simple(cls, path):
        result = super().handle_simple(path)
        # Force the 'mobile' namespace (cached by serviceworker)
        return '/mobile' + result


@register.tag('static')
def do_static(parser, token):
    return StaticNode.handle_token(parser, token)
