from django import forms
from django.contrib.gis.geos import Point
from django.forms.widgets import RadioSelect, Select
from django.utils.functional import cached_property
from django.utils.html import format_html

from observation.models import (
    Plot, PlotObs, RegenObs, RegenValue, RegenVerbiss, RegenFegen, Stem,
    TreeObs, TreeSpecies,
)


class RadioSelectLabelAfter(RadioSelect):
    option_template_name = 'mobile/widgets/radio_option.html'


class RadioNullBoolean(forms.NullBooleanSelect):
    input_type = 'radio'
    template_name = 'django/forms/widgets/radio.html'
    option_template_name = 'mobile/widgets/radio_option.html'
    # Restore defaults for ChoiceWidget
    checked_attribute = {'checked': True}
    option_inherits_attrs = True
    add_id_index = True


class SpeciesSelect(Select):
    @cached_property
    def historic_ids(self):
        return list(TreeSpecies.objects.filter(is_historic=True).values_list('pk', flat=True))

    def create_option(self, *args, **kwargs):
        option = super().create_option(*args, **kwargs)
        if option['value'] in self.historic_ids:
            option['attrs']['data-historic'] = 'true'
        return option


class DevelStageSelect(Select):
    def create_option(self, *args, **kwargs):
        opt = super().create_option(*args, **kwargs)
        if 'historisch' in opt['label']:
            opt['attrs']['disabled'] = True
        return opt


class PlotForm(forms.ModelForm):
    exact_long = forms.FloatField(label="Genaue Länge", required=False)
    exact_lat = forms.FloatField(label="Genaue Breite", required=False)

    class Meta:
        model = Plot
        fields = ('exact_long', 'exact_lat', 'exposition', 'slope')
        widgets = {
            'exposition': RadioSelectLabelAfter,
        }

    def save(self, *args, **kwargs):
        plot = super().save(*args, **kwargs)
        lon, lat = self.cleaned_data.get('exact_long'), self.cleaned_data.get('exact_lat')
        if lon and lat:
            plot.point_exact = Point(lon, lat, srid=2056)
            plot.set_elevation_from_swisstopo(save=False)
            plot.save()
        return plot


class POForm(forms.ModelForm):
    forest_edgef = forms.DecimalField(
        label="Waldrandfaktor", min_value=0.5, max_value=1.0, max_digits=2, decimal_places=1
    )

    class Meta:
        model = PlotObs
        fields = (
            'municipality', 'relief', 'inv_team',
            'forest_edgef', 'stand_devel_stage', 'stand_forest_mixture', 'stand_crown_closure',
            'soil_compaction', 'forest_form', 'regen_type', 'remarks',
        )
        widgets = {
            'stand_devel_stage': DevelStageSelect,
            'relief': RadioSelectLabelAfter,
            'inv_team': forms.HiddenInput,
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['municipality'].can_be_locked = True
        self.fields['municipality'].queryset = self.fields['municipality'].queryset.order_by('name')

    def step1_fields(self):
        """Return a list of BoundField objects that are displayed at step 1."""
        return [
            field for field in self
            if not field.is_hidden and field.name not in ('relief', 'remarks')
        ]


class RegenForm(forms.ModelForm):
    class Meta:
        model = RegenObs
        exclude = ('perc',)
        widgets = {'obs': forms.HiddenInput, 'spec': forms.HiddenInput}

    def __init__(self, *args, spec=None, **kwargs):
        self.species = spec
        if not 'initial' in kwargs:
            kwargs['initial'] = {
                'spec': self.species.pk,
                'perc_an': RegenValue.objects.get(code=1).pk,
                'perc_auf': RegenValue.objects.get(code=1).pk,
                'verbiss': RegenVerbiss.objects.get(code=1).pk,
                'fegen': RegenFegen.objects.get(code=1).pk,
            }
        super().__init__(*args, **kwargs)

    def has_changed(self):
        # This form should always be saved, even if initial data didn't change
        return True


class RegenFormset(forms.BaseInlineFormSet):
    form = RegenForm
    model = RegenObs
    fk = RegenObs._meta.get_field('obs')
    min_num = 7
    max_num = 7
    absolute_max = 7
    validate_min = False
    validate_max = False
    extra = 0
    can_order = False
    can_delete = False

    def __init__(self, *args, **kwargs):
        kwargs['queryset'] = RegenObs.objects.none()
        kwargs['prefix'] = 'regenform'
        super().__init__(*args, **kwargs)
        self.species = [
            TreeSpecies.objects.get(species=sp) for sp in RegenObs.OBS_SPECIES
        ]

    def get_form_kwargs(self, index):
        return {'spec': self.species[index]}


TREE_FIELDS = ('species', 'nr', 'distance', 'azimuth')

class TreeForm(forms.ModelForm):
    species = forms.ModelChoiceField(
        queryset=TreeSpecies.objects.filter(is_tree=True).order_by('species'),
        widget=SpeciesSelect
    )
    nr = forms.IntegerField()
    distance = forms.DecimalField(label="Distanz", max_digits=6, decimal_places=2)
    azimuth = forms.IntegerField(min_value=0, max_value=400)
    dbh = forms.IntegerField(label="BHD", min_value=12, max_value=200)
    perim = forms.IntegerField(label="Umfang", max_value=900, required=False)

    class Meta:
        model = TreeObs
        fields = TREE_FIELDS + (
            'tree', 'vita', 'dbh', 'layer', 'rank', 'damage', 'damage_cause',
            'crown_length', 'crown_form', 'stem', 'stem_forked', 'stem_height',
            'woodpecker', 'ash_dieback', 'remarks',
        )
        widgets = {
            'tree': forms.HiddenInput,
            'stem_forked': RadioNullBoolean,
            'woodpecker': RadioNullBoolean,
        }

    def __init__(self, *args, **kwargs):
        if not 'initial' in kwargs:
            kwargs['initial'] = {
                'stem': Stem.objects.get(code='B').pk,
            }
        super().__init__(*args, **kwargs)
        # azimuth unit depends on user choice
        self.fields['distance'].unit = 'dm'
        self.fields['dbh'].unit = 'cm'

    def other_fields(self):
        for field in self.visible_fields():
            if field.name not in TREE_FIELDS + ('perim',):
                yield field

    def full_clean(self):
        try:
            # For cut/not considered trees, dbh shouldn't be validated
            if self.fields['vita'].clean(self.data['vita']).code in ('c', 'x', 'y'):
                self.fields['dbh'].validators = []
        except Exception:
            pass
        return super().full_clean()

    def save(self, **kwargs):
        # Save tree fields if changed
        tree_changed = False
        for fname in TREE_FIELDS:
            tree_field = 'spec' if fname == 'species' else fname
            if getattr(self.instance.tree, tree_field) != self.cleaned_data[fname]:
                setattr(self.instance.tree, tree_field, self.cleaned_data[fname])
                tree_changed = True
        if tree_changed:
            self.instance.tree.save()
        return super().save(**kwargs)
