import json
from datetime import date
from decimal import Decimal
from pprint import pformat

from django.contrib.gis.db.models.functions import Transform
from django.core.exceptions import ValidationError
from django.core.mail import mail_admins
from django.db import IntegrityError, transaction
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

#from afw.models import WaldBestandLaufend
from gemeinde.models import Gemeinde
from observation.models import (
    Inventory, Plot, PlotObs, SLOPE_MAPPING, SurveyType, Tree, TreeObs, TreeSpecies,
    WaldBestandKarte,
)
from .forms import PlotForm, POForm, RegenFormset, TreeForm


def main(request):
    context = {
        'gemeinden': Gemeinde.objects.all().order_by('name'),
        'inventories': Inventory.objects.filter(
            inv_from__lte=date.today(), inv_to__gte=date.today()
        ),
        'po_form': POForm(),
        'regen_form': RegenFormset(),
        'plot_form': PlotForm(),
        'tree_form': TreeForm(),
        'verbose_names':  dict([
            (field.name, field.verbose_name) for field in PlotObs._meta.fields
             if not field.name.startswith('i')
        ]),
        'slope_mapping': SLOPE_MAPPING,
    }
    return render(request, 'mobile/index.html', context)


def inventory_view(request, pk):
    """This view returns a GeoJSON with Inventory data."""
    inventory = get_object_or_404(Inventory, pk=pk)
    inventory.geom.transform(4326)
    geojson = {
        "type": "Feature",
        "id": inventory.pk,
        "properties": {
            "name": inventory.name,
            "plotsURL": reverse('inventory-plots', args=[inventory.pk]),
            "layerURLs": {
                'BestandesKarte': reverse('obs-bestand', args=[inventory.pk]),
                'Erschliessung': reverse('afw-erschliessung', args=[inventory.pk]),
            },
        },
        "geometry": eval(inventory.geom.json),
    }
    return JsonResponse(geojson)


def inventory_plots(request, pk):
    inventory = get_object_or_404(Inventory, pk=pk)
    gemeinden = Gemeinde.objects.filter(the_geom__intersects=inventory.geom)

    def get_gemeinde(pt):
        for gem in gemeinden:
            if gem.the_geom.contains(pt):
                return gem

    geojson = {"type": "FeatureCollection", "features": []}
    plot_qs = Plot.objects.filter(the_geom__within=inventory.geom
        ).annotate(plotgeom=Transform('the_geom', 4326))
    if inventory.excluded_plots:
        plot_qs = plot_qs.exclude(pk__in=inventory.excluded_plots)

    for plot in plot_qs:
        geojson["features"].append(plot.as_geojson(geom_field='plotgeom', for_mobile=True))
        for year in geojson["features"][-1]["properties"]["obsURLs"].keys():
            geojson["features"][-1]["properties"]["obsURLs"][year] += '?srid=4326'
        gemeinde = get_gemeinde(plot.the_geom)
        geojson["features"][-1]["properties"]["municipality_id"] = gemeinde.pk if gemeinde else ''
        geojson["features"][-1]["properties"]["municipality_name"] = str(gemeinde) if gemeinde else ''
        geojson["features"][-1]["properties"]["coords_2056"] = [
            int(round(c, 0)) for c in plot.the_geom.coords
        ]
        if plot.point_exact:
            geojson["features"][-1]["properties"].update({
                "coords_2056_exact": [
                    round(c, 1) for c in plot.point_exact.coords
                ],
                # This will populate the plot form step
                "exact_long": plot.point_exact.coords[0],
                "exact_lat": plot.point_exact.coords[1],
            })
        else:
            geojson["features"][-1]["properties"]["coords_2056_exact"] = None
        # Add infos from Bestandeskarte
        try:
            bestand = WaldBestandKarte.objects.select_related(
                'entwicklungstufe', 'mischungsgrad', 'schlussgrad'
            ).get(geom__contains=plot.the_geom)
        except (WaldBestandKarte.DoesNotExist, WaldBestandKarte.MultipleObjectsReturned):
            pass
        else:
            def to_list(val):
                return [val.pk, str(val)] if val is not None else []
            geojson["features"][-1]["properties"]["stand_devel_stage"] = to_list(
                bestand.entwicklungstufe)
            geojson["features"][-1]["properties"]["stand_forest_mixture"] = to_list(
                bestand.mischungsgrad)
            geojson["features"][-1]["properties"]["stand_crown_closure"] = to_list(
                bestand.schlussgrad)
    return JsonResponse(geojson)


class TreeDataError(IntegrityError):
    # subclass of IntegrityError to make the transaction rollback
    pass


class HttpResponseWithError(HttpResponseForbidden):
    def __init__(self, data, error):
        mail_admins(
            'Sync error on ksp.guaraci.ch',
            ('%s\n\n'
             'Sent data:\n%s') % (error, pformat(data))
        )
        super().__init__(error)


@csrf_exempt
@require_POST
def sync_view(request):
    """
    This view receives (in the request body) JSON-formatted data to sync PlotObs
    data entered in the mobile app.
    """
    data = json.loads(request.body.decode('utf-8'))
    plotobs_props = data['features'][0]['properties']
    plot = get_object_or_404(Plot, pk=int(data['plot']))
    inventory = get_object_or_404(Inventory, pk=int(plotobs_props['inv_team']))

    try:
        municipality_id = int(plotobs_props['municipality'][0])
    except (KeyError, IndexError, TypeError):
        return HttpResponseWithError(data, "Error: Data do not contains municipality id")
    municipality = get_object_or_404(Gemeinde, pk=municipality_id)

    try:
        year = int(plotobs_props.get('year'))
    except TypeError:
        return HttpResponseWithError(data, "Error: year expected, received '%s'" % plotobs_props.get('year'))
    if year != date.today().year:
        return HttpResponseWithError(data, "Unable to save a PlotObs for another year")

    try:
        plotobs = PlotObs.objects.get(plot=plot, year=plotobs_props['year'])
        # It's a new sync with existing data, overwrite old data with the new one
        plotobs.delete()
    except PlotObs.DoesNotExist:
        pass
    plotobs = PlotObs(
        plot=plot, year=plotobs_props['year'], municipality=municipality,
        density=inventory.default_density, gwl=0
    )

    # Some fields are related to the Plot
    plot_keys = [key for key in plotobs_props.keys() if key in PlotForm._meta.fields]
    plot_data = {}
    for key in plot_keys:
        value = plotobs_props[key]
        if isinstance(value, list):
            value = value[0]  # Currently exposition (ChoiceField)
        plot_data[key] = value
    plot_form = PlotForm(instance=plot, data=plot_data)
    if not plot_form.is_valid():
        return HttpResponseWithError(data, plot_form.errors.as_text())

    # Check PlotObs properties
    errs = []
    for key, value in plotobs_props.items():
        if key in {'year', 'municipality', 'type'} or key in plot_keys or key.startswith('regenform'):
            continue
        field = plotobs._meta.get_field(key)
        if field.many_to_one:
            # Select choices are [pk, human-readable]
            pk_value = value[0] if isinstance(value, list) else value
            if pk_value == '':
                continue
            try:
                value = field.related_model.objects.get(pk=int(pk_value))
            except ValueError as err:
                errs.append("%s=%s: %s" % (key, value, err))
                continue
        if value != '':
            try:
                setattr(plotobs, key, value)
            except ValueError as err:
                errs.append(err)
    try:
        plotobs.full_clean()
    except ValidationError as err:
        errs.append(err)
    if errs:
        return HttpResponseWithError(data, "Errors: %s" % ", ".join([str(err) for err in errs]))

    # RegenObs data
    regen_data = {
        k: (v[0] if isinstance(v, list) else v)
        for k, v in plotobs_props.items() if k.startswith('regenform')
    }
    regen_fset = RegenFormset(data=regen_data, instance=plotobs)
    if not regen_fset.is_valid():
        return HttpResponseWithError(data, "Errors in Verjüngen Daten")

    previous_obs = plot.plotobs_set.order_by('year').last()
    required_trees = {to.tree for to in previous_obs.not_cut_trees()} if previous_obs else set()
    try:
        with transaction.atomic():
            plot_form.save()
            plotobs.save()
            regen_fset.save()

            # Save new TreeObs
            for tree_feature in data['features'][1:]:
                try:
                    treeobs = save_treeobs(plotobs, tree_feature)
                    required_trees.discard(treeobs.tree)
                except ValidationError as err:
                    errs.append(err)
            # Checks
            if len(required_trees) > 0 and not errs and plotobs.forest_edgef >= 0.6:
                errs.append("Some previous trees are missing new data")
            if errs:
                # Raising this error will rollback the transaction
                raise TreeDataError("Errors in observation data")
    except TreeDataError:
        # Error already saved, just ignore the error
        pass
    except IntegrityError as err:
        errs.append(err)
    if errs:
        return HttpResponseWithError(data, "Errors: %s" % ", ".join([str(err) for err in errs]))

    return HttpResponse()


def save_treeobs(plotobs, tree_feature):
    if tree_feature['properties']['tree'] == '':
        # New tree
        spec_id = tree_feature['properties']['species'][0]
        if spec_id == '':
            raise ValidationError("The tree species is mandatory")
        spec = TreeSpecies.objects.get(pk=spec_id)
        tree = Tree.objects.create(
            plot=plotobs.plot, spec=spec,
            nr=tree_feature['properties']['nr'],
            azimuth=int(tree_feature['properties']['azimuth']),
            distance=Decimal(tree_feature['properties']['distance']),
        )
    else:
        tree = Tree.objects.get(pk=int(tree_feature['properties']['tree']))
    treeobs = TreeObs(obs=plotobs, tree=tree, survey_type=SurveyType.objects.get(code='K'))
    # Set TreeObs values from tree_feature['properties']
    errs = []

    data = {}
    for key, value in tree_feature['properties'].items():
        # Select choices come in the form [pk, human-readable]
        data[key] = value[0] if isinstance(value, list) else value
    data['tree'] = tree.pk
    form = TreeForm(instance=treeobs, data=data)
    if not form.is_valid():
        raise ValidationError(", ".join('%s: %s' % (k, v) for k, v in form.errors.items()))
    treeobs = form.save()
    return treeobs


def map_redirect(request):
    return HttpResponseRedirect('https://geowms.bl.ch/?' + request.environ['QUERY_STRING'])
