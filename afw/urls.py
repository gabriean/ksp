from django.urls import path

from . import views

urlpatterns = [
    path('waldbestand/<int:inventory_pk>/', views.waldbestand, name='afw-bestand'),
    path('erschliessung/<int:inventory_pk>/', views.erschliessung, name='afw-erschliessung'),
]
