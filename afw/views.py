from django.contrib.gis.db.models.functions import Transform
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

from observation.models import Inventory

from .models import ErschliessungLaufend, WaldBestandLaufend


def waldbestand(request, inventory_pk):
    inventory = get_object_or_404(Inventory, pk=inventory_pk)
    query = WaldBestandLaufend.objects.filter(geom__intersects=inventory.geom
        ).annotate(geom_wgs84=Transform('geom', 4326)
        ).select_related('entwicklungstufe', 'mischungsgrad', 'schlussgrad')
    # Transform to GeoJSON
    geojson = {"type": "FeatureCollection", "features": []}
    for bestand in query:
        geojson["features"].append({
            "type": "Feature",
            "id": bestand.pk,
            "properties": {
                "id": bestand.pk,
                "popupContent": "Entwicklungstufe: %s<br>Mischungsgrad: %s<br>Schlussgrad: %s" % (
                    bestand.entwicklungstufe, bestand.mischungsgrad, bestand.schlussgrad)
            },
            "geometry": eval(bestand.geom_wgs84.json),
        })
    return JsonResponse(geojson)


def erschliessung(request, inventory_pk):
    inventory = get_object_or_404(Inventory, pk=inventory_pk)
    query = ErschliessungLaufend.objects.filter(geom__intersects=inventory.geom
        ).annotate(geom_wgs84=Transform('geom', 4326))
    # Transform to GeoJSON
    geojson = {"type": "FeatureCollection", "features": []}
    for item in query:
        geojson["features"].append({
            "type": "Feature",
            "id": item.pk,
            "properties": {
                "id": item.pk,
            },
            "geometry": eval(item.geom_wgs84.json),
        })
    return JsonResponse(geojson)
