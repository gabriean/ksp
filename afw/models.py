from django.contrib.gis.db import models

from gemeinde.models import Gemeinde


class WaldausscheidungLaufend(models.Model):
    datum_import = models.DateTimeField()
    geom = models.PolygonField(srid=2056)

    class Meta:
        db_table = '"fp"."waldausscheidung_laufend"'
        managed = False


class Entwicklungsstufe(models.Model):
    """Equivalent of ksp2.lt_devel_stage"""
    lut_code = models.CharField(max_length=2, primary_key=True)
    name = models.CharField(max_length=50)
    code_bl = models.CharField(max_length=2, blank=True)
    normalwald = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = '"common"."lut_bk_entwicklungsstufe"'
        managed = False

    def __str__(self):
        return self.name

    def to_ksp_value(self):
        """Mapping between this table and ksp2.lt_devel_stage."""
        from observation.models import DevelStage
        try:
            return DevelStage.objects.get(code=self.lut_code)
        except DevelStage.DoesNotExist:
            return None


class Mischungsgrad(models.Model):
    """Equivalent of ksp2.lt_forest_mixture"""
    lut_code = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=70)

    class Meta:
        db_table = '"common"."lut_bk_mischungsgrad"'
        managed = False

    def __str__(self):
        return self.name

    def to_ksp_value(self):
        """Mapping between this table and ksp2.lt_forest_mixture."""
        from observation.models import ForestMixture
        try:
            return ForestMixture.objects.get(code=self.lut_code)
        except ForestMixture.DoesNotExist:
            return None


class Schlussgrad(models.Model):
    """Equivalent of ksp2.lt_crown_closure"""
    lut_code = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=50)
    code = models.SmallIntegerField()

    class Meta:
        db_table = '"common"."lut_bk_schlussgrad"'
        managed = False

    def __str__(self):
        return self.name

    def to_ksp_value(self):
        """Mapping between this table and ksp2.lt_crown_closure."""
        from observation.models import CrownClosure
        try:
            return CrownClosure.objects.get(code=self.lut_code)
        except CrownClosure.DoesNotExist:
            return None


class WaldBestandLaufend(models.Model):
    gemeinde_id_bfs = models.ForeignKey(Gemeinde, primary_key=True, to_field='bfs_nr', on_delete=models.DO_NOTHING),
    forstkreis = models.PositiveIntegerField(blank=True, null=True)  #FK to common.lut_forstkreis
    forstrevier = models.PositiveIntegerField(blank=True, null=True)  #FK to common.lut_forstrevier
    jagdrevier = models.PositiveIntegerField(blank=True, null=True)  #FK to common.lut_jagdrevier
    entwicklungstufe = models.ForeignKey(Entwicklungsstufe, blank=True, null=True,
        db_column='entw', on_delete=models.SET_NULL)
    mischungsgrad = models.ForeignKey(Mischungsgrad, blank=True, null=True,
        db_column='mg', on_delete=models.SET_NULL)
    schlussgrad = models.ForeignKey(Schlussgrad, blank=True, null=True,
        db_column='sg', on_delete=models.SET_NULL)
    fichte = models.SmallIntegerField(default=0, db_column='fi')
    tanne = models.SmallIntegerField(default=0, db_column='ta')
    buche = models.SmallIntegerField(default=0, db_column='bu')
    esche = models.SmallIntegerField(default=0, db_column='es')
    ahorn = models.SmallIntegerField(default=0, db_column='ah')
    eiche = models.SmallIntegerField(default=0, db_column='ei')
    bemerkungen = models.CharField(max_length=100, blank=True, db_column='bem')
    nh_rest = models.SmallIntegerField(default=0)
    lh_rest = models.SmallIntegerField(default=0)
    dateofchng = models.DateField(blank=True, null=True)
    wpid = models.FloatField(blank=True, null=True)
    jahr_eingriff = models.SmallIntegerField(blank=True, null=True)
    massnahme = models.CharField(max_length=14, blank=True)
    flaeche_eingriff = models.SmallIntegerField(blank=True, null=True)
    datum_import = models.DateTimeField(blank=True, null=True)
    geom = models.PolygonField(srid=2056)

    class Meta:
        db_table = '"fp"."waldbestand_laufend"'
        managed = False


class ErschliessungLaufend(models.Model):
    gemeinde_id_bfs = models.ForeignKey(Gemeinde, primary_key=True, to_field='bfs_nr', on_delete=models.DO_NOTHING),
    forstkreis = models.PositiveIntegerField(blank=True, null=True)  #FK to common.lut_forstkreis
    forstrevier = models.PositiveIntegerField(blank=True, null=True)  #FK to common.lut_forstrevier
    jagdrevier = models.PositiveIntegerField(blank=True, null=True)  #FK to common.lut_jagdrevier
    #FK to common.lut_erschliessung_typ
    typ = models.SmallIntegerField(blank=True, null=True)
    zustand = models.SmallIntegerField(blank=True, null=True)
    planung = models.SmallIntegerField(blank=True, null=True)
    status = models.SmallIntegerField(blank=True, null=True)
    #FK to common.lut_erschliessung_sichtbarkeit
    sichtbarkeit = models.SmallIntegerField(blank=True, null=True)
    #FK to common.lut_erschliessung_herkunft
    herkunft = models.SmallIntegerField(blank=True, null=True)
    bemerkungen = models.CharField(max_length=340, blank=True, db_column='bem')
    datum_import = models.DateTimeField(blank=True, null=True)
    geom = models.LineStringField(srid=2056)

    class Meta:
        db_table = '"fp"."erschliessung_laufend"'
        managed = False


class LutPflanzensoziologie(models.Model):
    lut_code = models.CharField(max_length=10, primary_key=True)
    ek_code = models.SmallIntegerField()
    farbwert = models.CharField(max_length=5)
    nais = models.CharField(max_length=5)
    g = models.IntegerField()
    g2 = models.SmallIntegerField()
    oek_grp = models.CharField(max_length=5)
    selten = models.IntegerField()
    s_bed = models.CharField(max_length=2)
    traubeneichenstandort = models.BooleanField(default=False)
    stieleichenstandort = models.BooleanField(default=False)

    class Meta:
        db_table = '"common"."lut_pflanzensoziologie"'
        managed = False


class PflanzensoziologieLaufend(models.Model):
    gemeinde_id_bfs = models.ForeignKey(Gemeinde, primary_key=True, to_field='bfs_nr', on_delete=models.DO_NOTHING),
    code = models.ForeignKey(LutPflanzensoziologie, to_field='lut_code', db_column='code', on_delete=models.DO_NOTHING)
    datum_import = models.DateTimeField(blank=True, null=True)
    geom = models.PolygonField(srid=2056)

    class Meta:
        db_table = '"fp"."pflanzensoziologie_laufend"'
        managed = False
