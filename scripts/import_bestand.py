"""
Script to import bestandeskarteBL_xxxx-xx-xx into observation.WaldBestandKarte
"""

from django.contrib.gis.geos import MultiPolygon, Polygon
from django.contrib.gis.gdal import DataSource
from django.contrib.gis.utils import LayerMapping
from observation.models import CrownClosure, DevelStage, ForestMixture, WaldBestandKarte

source = '../bestandeskarteBL_2019-09-19/bk_blbs_clip_waldflaeche190919.shp'
ds = DataSource(source)
layer = ds[0]

WaldBestandKarte.objects.all().delete()

for idx, feature in enumerate(layer):
    poly = feature.geom.geos
    polys = poly if isinstance(poly, MultiPolygon) else [poly] 
    for polygon in polys:
        mg_val = feature['MG'].value
        if mg_val == 9:
            # MG = 9: nicht interpretierbar
            mg_val = None
        try:
            WaldBestandKarte.objects.create(
                entwicklungstufe=DevelStage.objects.get(code=feature['ENTW']),
                mischungsgrad=ForestMixture.objects.get(code=mg_val) if mg_val else None,
                schlussgrad=CrownClosure.objects.get(code=feature['SG'].value),
                geom=polygon,
            )
        except Exception as exc:
            print("Import Error: %s" % exc)
            continue
