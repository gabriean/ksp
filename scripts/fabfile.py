import getpass
from fabric import task
from invoke import Context, Exit

MAIN_HOST = 'guaraci.vserver.softronics.ch:2222'

project = 'ksp'
default_db_owner = 'claude'


@task(hosts=[MAIN_HOST])
def deploy(conn, project=project):
    python_exec = "/var/www/virtualenvs/ksp3/bin/python"

    with conn.cd("/var/www/%s" % project):
        # activate maintenance mode
        conn.run('sed -i -e "s/UPGRADING = False/UPGRADING = True/" ksp/wsgi.py')
        conn.run('git stash && git pull && git stash pop')
        conn.run('%s manage.py migrate' % python_exec)
        conn.run('%s manage.py collectstatic --noinput' % python_exec)
        conn.run('sed -i -e "s/UPGRADING = True/UPGRADING = False/" ksp/wsgi.py')


@task(hosts=[MAIN_HOST])
def clone_to_testdb(conn):
    dbname = 'ksp2'
    tmp_path = '/tmp/{db}.dump'.format(db=dbname)
    conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
    conn.sudo('dropdb ksptest', user='postgres')
    conn.sudo('pg_dump -Fc {db} > {tmp}'.format(db=dbname, tmp=tmp_path), user='postgres')
    # recreate ksp_test
    conn.sudo('psql -c "CREATE DATABASE ksptest OWNER={owner};"'.format(owner='ksp_django'), user='postgres')
    conn.sudo('psql -c "CREATE EXTENSION IF NOT EXISTS postgis;"', user='postgres')
    # load dump
    conn.sudo('pg_restore -d ksptest {tmp}'.format(tmp=tmp_path), user='postgres')


@task(hosts=[MAIN_HOST])
def clone_remote_pgdb(conn, dbname):
    """ Dump a remote database and load it locally """
    local = Context()

    def exist_local_db(db):
        res = local.run('psql --list', hide='stdout')
        return db in res.stdout.split()

    def exist_username(user):
        res = local.run('psql -d postgres -c "select usename from pg_user;"', hide='stdout')
        return user in res.stdout.split()

    tmp_path = f'/tmp/{dbname}.dump'
    conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
    conn.sudo(f'pg_dump --no-owner --no-privileges -Fc {dbname} > {tmp_path}', user='postgres')
    conn.get(tmp_path, None)

    if exist_local_db(dbname):
        rep = input(f'A local database named "{dbname}" already exists. Overwrite? (y/n)')
        if rep == 'y':
            local.run(f'psql -d postgres -c "DROP DATABASE {dbname};"')
        else:
            raise Exit("Database not copied")

    if exist_username(dbname):
        owner = dbname
    else:
        owner = default_db_owner
    local.run(f'psql -d postgres -c "CREATE DATABASE {dbname} OWNER={owner};"')
    local.run(f'psql -d {dbname} -c "CREATE EXTENSION IF NOT EXISTS postgis;"')
    local.run(f'pg_restore --no-owner --no-privileges -d {dbname} {dbname}.dump')
