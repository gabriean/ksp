from django.db import models
from django.utils.translation import gettext_lazy as _


class Document(models.Model):
    CATEG_CHOICES = (
        ('anleit', "KSP Anleitungen"),
        ('wanleit', "Weitere Anleitungen"),
        ('wepana', "WEP Analyseteile"),
    )
    doc = models.FileField(upload_to='documents', verbose_name=_("File"))
    title = models.CharField(max_length=255, verbose_name=_("Title"))
    date = models.DateField(_("Date"))
    category = models.CharField(max_length=10, choices=CATEG_CHOICES, default='anleit')
    published = models.BooleanField(default=True)
    weight = models.SmallIntegerField(default=0)

    @property
    def fformat(self):
        return self.doc.path.rsplit('.')[-1]
