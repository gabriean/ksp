from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('document', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='category',
            field=models.CharField(choices=[('anleit', 'KSP Anleitungen'), ('wanleit', 'Weitere Anleitungen'), ('wepana', 'WEP Analyseteile')], default='anleit', max_length=10),
        ),
        migrations.AddField(
            model_name='document',
            name='published',
            field=models.BooleanField(default=True),
        ),
    ]
