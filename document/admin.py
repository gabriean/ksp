from django.contrib import admin

from .models import Document


class DocumentAdmin(admin.ModelAdmin):
    list_display = ('title', 'date', 'category', 'weight')


admin.site.register(Document, DocumentAdmin)
