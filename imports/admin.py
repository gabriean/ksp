from django.contrib import admin
from django.utils.text import Truncator

from .models import FileImport

class FileImportAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'warnings_trunc')

    def warnings_trunc(self, obj):
        return Truncator(obj.warnings).words(25)


admin.site.register(FileImport, FileImportAdmin)
