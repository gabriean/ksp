import os
import sys

from django.core.files import File
from django.core.management import BaseCommand, CommandError

from imports.imports import Importer, ImportErrors
from imports.models import FileImport


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        if len(args) != 1:
            raise CommandError("Please provide one and only one argument: the file to import")
        input_file = args[0]
        imp = FileImport(ifile=File(open(input_file)), name=os.path.basename(input_file))
        try:
            Importer().do_import(imp)
        except ImportErrors as err:
            sys.stderr.write("Errors happen during import:\n\n%s\n" % "\n".join(err.errors))
        else:
            imp.save()
            if imp.warnings:
                sys.stderr.write("Warnings have been produced during import:\n\n%s\n" % imp.warnings)
